clear all
close all
clc

% change MAXTIME to get Pts x,y and z between 8 to 9 sec
MAXTIME = 20;

N_sample = 30000;
types = [0, 1];



for i=1:N_sample
    KCap_opts_ty1 = 0.01 + (0.21-0.01)*rand(1,1);
    KS_opts_ty1 = 1e3 + (1e4-1e3)*rand(1,1);
    wavenumber_opts_ty1 = 0.1 + (10-0.1)*rand(1,1);
    KCap_opts_ty2 = 0.01 + (0.21-0.01)*rand(1,1);
    KS_opts_ty2 = 1e3 + (1e4-1e3)*rand(1,1);
    wavenumber_opts_ty2 = 0.1 + (10-0.1)*rand(1,1);
    
    Random_Parameters_Ty1(i,:) = [KCap_opts_ty1, KS_opts_ty1, wavenumber_opts_ty1];
    Random_Parameters_Ty2(i,:) = [KCap_opts_ty2, KS_opts_ty2, wavenumber_opts_ty2];
end

% fpath = '\Nathan_GelsData\1st_DM';
fpath = '/home/dorukaks/Desktop/trial';
% fpath = '/home/goroda/Data/CatalystGels/';
filename = strcat(fpath,'Random_Parameters_5th_R1_KCap_KS_Wv.mat');
save(filename, 'Random_Parameters_Ty1', 'Random_Parameters_Ty2');

% run = [3.87368421052632,8631.57894736842,0.0770526315789474,0];
% run = [2,7500,0.1,0];
% original parameters 1 
% run(1)=4.90034628e+00; %wavenumber
% run(2)=3.05536096e+03; %Ks
% run(3)=8.50884152e-02; %Kcap

% % recovered parameters 1
% run(1)=4.90034628e+00; %wavenumber
% run(2)=3.6920437e+03; %Ks
% run(3)=8.5565023e-02; %Kcap

% % original parameters 2 
% run(3)=1.16938773e-01; %wavenumber
% run(2)=3.38790626e+03; %Ks
% run(1)=5.22851550e+00; %Kcap
% 
% % recovered parameters 2
% run(3)=1.1617139e-01; %wavenumber
% run(2)=2.3257139e+03; %Ks
% run(1)=5.3452730e+00; %Kcap

% % original parameters 3 
% run(3)=8.50884152e-02; %wavenumber
% run(2)=3.05536096e+03; %Ks
% run(1)=4.90034628e+00; %Kcap

% recovered parameters 3
% run(3)=8.7287545e-02; %wavenumber
% run(2)=3.0127087e+03; %Ks
% run(1)=5.0926723e+00; %Kcap

% run=[5.61101437e+00,6.05191748e+03,1.04208235e-02 , 0]; %predicted1
% run=[5.51019284e+00, 6.05800767e+03,1.03027867e-02, 0]; %actual1

% run=[2.8037717e+00,8.1579849e+03,1.4177874e-01 , 0]; %predicted2
% run=[2.86865517e+00, 8.27395141e+03,1.46429489e-01, 0]; %actual2
% 
% run=[5.6726079e+00,5.6261597e+03,1.0182852e-02 , 0]; %predicted3
% run=[5.74130723e+00, 5.61859282e+03,1.0178032e-02, 0]; %actual3
% 
% run=[1.6687684e+00,5.9134888e+03,3.5569489e-02 , 0]; %predicted4 picccc
% run=[1.65447816e+00, 5.83039250e+03,3.66201177e-02, 0]; %actual4
% 
% run=[2.4911160e+00,8.2633574e+03,1.0592001e-02 , 0]; %predicted5
% run=[2.53166184e+00, 8.29373714e+03,1.07168352e-02, 0]; %actual5
% 
% run=[2.9139960e+00,8.3812344e+03,1.4409326e-01 , 1]; %predicted6
% run=[2.86865517e+00, 8.27395141e+03,1.46429489e-01, 1]; %actual6 pick
% 
% run=[2.6316502e+00,2.7159270e+03,1.9979593e-01 , 1]; %predicted7 tirt
% run=[2.65996429e+00, 2.71706183e+03,1.96222325e-01 , 1]; %actual7
% 
% run=[1.8782146e+00,5.8963906e+03,8.1000879e-02 , 1]; %predicted8
% run=[1.90999368e+00, 5.87534822e+03,8.24266330e-02 , 1]; %actual8 accept
% 
% run=[1.4066806e+00,5.5080254e+03,1.3443053e-02 , 0]; %predicted9
% run=[1.43117926e+00, 5.52034160e+03,1.36194909e-02 , 0]; %actual9
% 
% run=[1.7445683e+00,4.5618350e+03,9.1115721e-02 , 0]; %predicted10
% run=[1.71060882e+00, 4.55281861e+03,9.02740749e-02 , 0]; %actual10
% 
% run=[9.122226e+00,4.746893e+03,3.845965e-02 , 1]; %predicted11 tirtt
% run=[9.16578170e+00, 4.79585154e+03, 3.83772677e-02 , 1]; %actual11
% 
% run=[2.7089975e+00,8.8061191e+03,1.2106180e-01, 1]; %predicted12 accept
run=[2.72131236e+00, 8.82946929e+03, 1.18147867e-01 , 1]; %actual12
% 
% run=[2.8606284e+00,8.4396250e+03,1.2429897e-01, 0]; %predicted13 
% run=[2.89083158e+00, 8.36767523e+03, 1.25235529e-01 , 0]; %actual13
% 
% run=[2.451375e-01,4.636939e+03 ,1.267252e-01, 0]; %predicted14
% run=[2.46160262e-01, 4.71523962e+03 , 1.24849911e-01  , 0]; %actual14
% 
% run=[2.8285992e+00,6.8551865e+03 ,1.4497785e-01  0]; %predicted15
% run=[2.87699370e+00, 6.90038060e+03  , 1.46279725e-01 , 0]; %actual15
% 
% run=[1.1049535e+00,4.7824863e+03,5.3261284e-02  0]; %predicted16
% run=[1.11140995e+00, 4.84015539e+03   , 5.35515342e-02 , 0]; %actual16


% {
% run = [8.54468173787408,6668.95046557979,0.251361396201689,0];
elastic_sheet_mod_byNathan_20190408_Random(run(1), run(2), run(3), run(4), MAXTIME);
% }
figure(14)
savefig('actual12_20s1')



% The loop for type 2

% parfor i=1:N_sample
%         run = [Random_Parameters_Ty2(i,1), Random_Parameters_Ty2(i,2), Random_Parameters_Ty2(i,3), types(2)];
%         elastic_sheet_mod_byNathan_20190408_Random(run(1), run(2), run(3), run(4), MAXTIME);
% end
% 
% % The loop for type 1
% 
% parfor i=1:N_sample
%         run = [Random_Parameters_Ty1(i,1), Random_Parameters_Ty1(i,2), Random_Parameters_Ty1(i,3), types(1)];
%         elastic_sheet_mod_byNathan_20190408_Random(run(1), run(2), run(3), run(4), MAXTIME);
% end






%% from the code for 8th dataset

%{

Nwaves = 20;
NKS = 20;
NKCap = 20;


%%% For Type 1 iteration, create random numbers for following parameters
% KCap : [0.01 0.27] -> [0.01 0.21]
% KS : [1e3 1e4]
% wavenumber : [0.1 10]
% Type : 0


KCap_opts_ty1 = 0.01 + (0.21-0.01)*rand(1,NKCap);
KS_opts_ty1 = 1e3 + (1e4-1e3)*rand(1,NKS);
wavenumber_opts_ty1 = 0.1 + (10-0.1)*rand(1,Nwaves);

%%% For Type 2
% KCap : [0.01 0.21]
% KS : [1e3 1e4]
% wavenumber : [0.1 10]
% Type : 1

KCap_opts_ty2 = 0.01 + (0.21-0.01)*rand(1,NKCap);
KS_opts_ty2 = 1e3 + (1e4-1e3)*rand(1,NKS);
wavenumber_opts_ty2 = 0.1 + (10-0.1)*rand(1,Nwaves);


fpath = '/home/natekim/Documents/Umich/Research/Gel_Dataset/8th/';
filename = strcat(fpath,'Random_Parameters_KCap_KS_Wv_Ty.mat');
save(filename, 'KCap_opts_ty1', 'KS_opts_ty1', 'wavenumber_opts_ty1',...
    'KCap_opts_ty2', 'KS_opts_ty2', 'wavenumber_opts_ty2');


% The loop for type 2

for i=1:Nwaves
    parfor j=1:NKS
        for k=1:NKCap
            run = [wavenumber_opts_ty2(i), KS_opts_ty2(j), KCap_opts_ty2(k), types(2)];
            elastic_sheet_mod_byNathan_20190401_Random(run(1), run(2), run(3), run(4), MAXTIME);
        end
    end
end


% The loop for type 1

for i=1:Nwaves  %Nwaves
    parfor j=1:NKS  %NKS
        for k=1:NKCap   %NKCap
            run = [wavenumber_opts_ty1(i), KS_opts_ty1(j), KCap_opts_ty1(k), types(1)];
            elastic_sheet_mod_byNathan_20190401_Random(run(1), run(2), run(3), run(4), MAXTIME);
        end
    end
end

%}





%% For regular iterations

%{

Nwaves = 20;
NKS = 20;
NKCap = 20;

wavenumber_opts = linspace(0.1, 10, Nwaves);
KS_opts = linspace(1e3, 1e4, NKS);
KCap_opts = linspace(0.01, 1, NKCap);
% new span on KCap is created. KCap_opts(6) = 0.27
KCap_opts = linspace(0.01, KCap_opts(6), NKCap);
types = [0, 1];


% Run just one case
% run = [1, 10000, 0.2, 1];
% elastic_sheet_mod_byNathan_20190120(run(1), run(2), run(3), run(4), MAXTIME);


% The loop for type 2

for i=1:Nwaves
    parfor j=1:NKS
        for k=1:16
            run = [wavenumber_opts(i), KS_opts(j), KCap_opts(k), types(2)];
            elastic_sheet_mod_byNathan_20190205_Random(run(1), run(2), run(3), run(4), MAXTIME);
        end
    end
end

% The loop for type 1

for i=1:Nwaves  %Nwaves
    parfor j=1:NKS  %NKS
        for k=1:NKCap   %NKCap
            run = [wavenumber_opts(i), KS_opts(j), KCap_opts(k), types(1)];
            elastic_sheet_mod_byNathan_20190205_Random(run(1), run(2), run(3), run(4), MAXTIME);
        end
    end
end


%
% Iteration for the other boundary.
% 

% nominal_run = [0.5, 1e4, 0.2, 0];
% The nearest case from nominal_run = [wavenumber_opts(2), KS_opts(20),
% KCap_opts(5)];

% elastic_sheet_mod(nominal_run(1), nominal_run(2), nominal_run(3), nominal_run(4), MAXTIME);

% Valid cases according to Type
% Type 1 : 0.21842 (KCap_opts(5)) -> 0.27 (KCap_opts(6))
% Type 2 : 0.0621 (KCap_opts(2)) ->  0.21 (KCap_opts(5))
%  -> However, I set up KCap_opts(6) = 0.0648 as maximum to compare
%  (->Disregarded. My boundary is 0.21 KCap_opts)

%}