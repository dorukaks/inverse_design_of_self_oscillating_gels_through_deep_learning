function out = elastic_sheet_mod_byNathan_20190408_Random( wavenumber,KS,KCap,  type, maxtime, plottime)



%Nathan defined a directory where data is saved.
% fpath = '/home/natekim/Documents/Umich/Research/Gel_Dataset/5th_R1_Type1/';
fpath = '/home/dorukaks/Desktop/trial';
% fpath = '/home/goroda/Data/CatalystGels/';
% fpath = 'C:\Users\DK\Documents\@ Umich\Research\catalyst_gels\Analysis\3rd_Data\';


%%%%%

if (~exist('maxtime', 'var'))
    maxtime = 20;
end
if (~exist('plottime', 'var'))
    plottime = 0.05;
end

%wavenumber = 0.5; %[0.1 10]
%KS = 1e4; %1/sqrt(thickness) %potential parameter [1e3, 1e4]
% results are approximately linear % might need to decrease dt
%KCap = 0.2; % bounds = [0.01, 1];, nominal = 0.2
% type = 0 or 1

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Parameters (2019.1.20) - These parameters are given from main code.
% KS = 1e4; %1/sqrt(thickness) %potential parameter [1e3, 1e4]
% KB = 1;0;1;1e-1;
% 
% % results are approximately linear % might need to decrease dt
% KCap = 0.2; % bounds = [0.01, 1];, nominal = 0.2 
% % Define type for calculation
% Type = 0;    % 0 or 1
% %KCap = 0.2;0.1;
% 
% % Move wavenumber statement above Moviefilename statement
% wavenumber = 1; %[0.1 10]
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




n = 33;32;16;32;8;16;64;32;16;64;8;

d = 1/n;

MakeMesh = 1;
Pts = [];

% top half of the hexagon
for j1 = 0:n
    Pts = [Pts, [-0.5-j1*d/2:d:0.5+j1*d/2;...
        sqrt(3)/2*(1-j1*d)*ones(1,n+1+j1);...
        zeros(1,n+1+j1)]];
end

% bottom half of the hexagon
for j1 = 1:n;
    Pts = [Pts, [-1+j1*d/2:d:1-j1*d/2;...
        sqrt(3)/2*(-j1*d)*ones(1,2*n+1-j1); ...
        zeros(1,2*n+1-j1)]];
end

%Pts = Pts(:,[1,2,n+2,n+3]);

% plot(Pts(1,:), Pts(2,:), 'r.');
NumPts = size(Pts, 2);if (~exist('opt', 'var'))
    opt = true;
end
% axis equal;

if MakeMesh
    
    Edges = [];
    
    for j2 = 1:size(Pts, 2);
        Dist = sqrt((Pts(1,j2) - Pts(1,j2+1:end)).^2 + ...
            (Pts(2,j2) - Pts(2,j2+1:end)).^2);
        AddEdges = find(abs(Dist - d) < 1e-4);
        Edges = [Edges, [j2*ones(size(AddEdges)); ...
            AddEdges + j2]];
    end
    
    NumEdges = size(Edges, 2);
    
    
    
    %%%%%Pts(3,:) = 0.1*d*randn(4,1);%0.1*d*[1 0 0 1];
    
    %Pts(3,:) = 0.1*(Pts(1,:).^2 + Pts(2,:).^2);
    
    Vel = zeros(size(Pts));
    %Pts(3,:) = Pts(3,:) + 0.001*d*randn(1,size(Pts,2));
    
    %
    % %for k1 = 1:size(Edges, 2);
    %  %  plot(Pts(1,Edges(:,k1)), Pts(2,Edges(:,k1)), 'bx-');
    % %end
    %
    %figure; hold on; view(3);
    
    
    BendEdges = [];
    Faces = [];
    for k1 = 1:size(Edges, 2);
        %plot3(Pts(1,Edges(:,k1)), Pts(2,Edges(:,k1)),Pts(3,Edges(:,k1)), 'kx-');
        Edge1 = find(Edges(1,:) == Edges(1, k1) | Edges(2,:) == Edges(1, k1));
        Edge2 = find(Edges(1,:) == Edges(2, k1) | Edges(2,:) == Edges(2, k1));
        OtherPoints = setdiff(intersect(Edges(:,Edge1),Edges(:,Edge2)), Edges(:, k1));
        %plot(Pts(1,OtherPoints), Pts(2,OtherPoints), 'bx-');
        if length(OtherPoints) == 2;
            BendEdges = [BendEdges, [Edges(:, k1); OtherPoints(1); OtherPoints(2)]];
            Faces = [Faces; Edges(:, k1)', OtherPoints(1); Edges(:, k1)', OtherPoints(2)];
            %elseif length(OtherPoints) == 1;
            %    Faces = [Faces; Edges(:, k1)', OtherPoints(1)];
        end
    end
    Faces = sort(Faces, 2);
    Faces = unique(Faces,'rows');
    
    
    
    NumBendEdges = size(BendEdges, 2);
    
    Diff12StretchMat = sparse([1:NumEdges;1:NumEdges],Edges(1:2,:),...
        [ones(1,NumEdges);-ones(1,NumEdges)],NumEdges,NumPts);
    
    ValstoFStretchMat = sparse(Edges,[1:NumEdges;1:NumEdges],...
        [ones(1,NumEdges);-ones(1,NumEdges)],NumPts,NumEdges);
    
    Diff12BendMat = sparse([1:NumBendEdges;1:NumBendEdges],BendEdges(1:2,:),...
        [ones(1,NumBendEdges);-ones(1,NumBendEdges)],NumBendEdges,NumPts);
    Diff42BendMat = sparse([1:NumBendEdges;1:NumBendEdges],BendEdges([4 2],:),...
        [ones(1,NumBendEdges);-ones(1,NumBendEdges)],NumBendEdges,NumPts);
    Diff32BendMat = sparse([1:NumBendEdges;1:NumBendEdges],BendEdges([3 2],:),...
        [ones(1,NumBendEdges);-ones(1,NumBendEdges)],NumBendEdges,NumPts);
    
    ValstoFBendMat = sparse(reshape(BendEdges',4*NumBendEdges,1)',[1:4*NumBendEdges],...
        ones(1,4*NumBendEdges),NumPts,4*NumBendEdges);
end

% [i1,j1,s1] = find(ValstoFStretchMat*...
%     spdiags(-KS*Stretch./DistEdges, 0, NumEdges, NumEdges)*Diff12StretchMat);
% s1Vect = ones(3,1)*s1';
% FStretchMat = sparse([1:3*NumPts], [1:3*NumPts], s1Vect(:));
% %xDiff = (Diff12StretchMat*Pts(1,:)')';
% %    xStretchVals = -KS*Stretch.*xDiff./DistEdges;
% %    FxStretch = ValstoFStretchMat*xStretchVals';

Pts = Pts(:);
% Pts = Pts + 0.1*d*(2*rand(size(Pts))-1);
Pts(3:3:end,1) = 0.002*(Pts(1:3:end,1).^4 + Pts(2:3:end,1).^4);
%0.2*Pts(1:3:end,1).*Pts(2:3:end,1);

%(Pts(1:3:end,1).^2 + Pts(2:3:end,1).^2).*...
%    sin(2*atan2(Pts(2:3:end,1),Pts(1:3:end,1)));

%0.002*(Pts(1:3:end,1).^4 + Pts(2:3:end,1).^4);

%Pts(1:3:end) = Pts(1:3:end) + 0.1*d*(2*rand(size(Pts(1:3:end)))-1);
%Pts(2:3:end) = Pts(2:3:end) + 0.1*d*(2*rand(size(Pts(2:3:end)))-1);
%Pts(3:3:end) = Pts(2:3:end) + 0.1*d*(2*rand(size(Pts(2:3:end)))-1);


m = 0;1;
if m ~= 0
    dt = 6e-3;5e-3;
    mu = 2*sqrt(KS);
else
    dt = 1e-2;4e-2;5e-5;5e-3;4e-2;2e-4;2e-4;2e-3;2e-4;2e-4;2e-4;5e-5;5e-4;2e-4;2e-4;3e-4;
    mu = 0.01;
    %mu = 0.01;1;1;
end
PlotCt = round(plottime/dt);%8e1;1e1;3e2;2e1;1e3;1e8;
StopCt = round(maxtime/dt);%8e2;5e1;3e3;
maxsteps = 1*StopCt;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% x1,y1,zHeight,FStretchnorm,FBendnorm are not used (No Figure,
%%%%%%%%%% Dataset) - From Here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% x1 = [];
% y1 = [];
% zHeight = [];
FStretchnorm = [];
FBendnorm = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% x1,y1,zHeight,FStretchnorm,FBendnorm are not used (No Figure,
%%%%%%%%%% Dataset) - To Here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%deq = d*(1 + KCap*(0.5*(Pts(3*Edges(1,:)-2,1) + Pts(3*Edges(2,:)-2,1))).^2 + ...
%    KCap*(0.5*(Pts(3*Edges(1,:)-1,1) + Pts(3*Edges(2,:)-1,1))).^2);
%deq = d;
[i1,j1,s1] = find(-KS*ValstoFStretchMat*Diff12StretchMat);
i1Vect = 3*ones(3,1)*i1'+[-2;-1;0]*ones(size(i1'));
j1Vect = 3*ones(3,1)*j1'+[-2;-1;0]*ones(size(j1'));
s1Vect = ones(3,1)*s1';
FStretchMat = sparse(i1Vect(:), j1Vect(:), s1Vect(:), 3*NumPts, 3*NumPts);

i1b = repmat(BendEdges(:),4,1);
SwitchSame = BendEdges([2 1 4 3],:);
SwitchOpp1 = BendEdges([3 4 1 2],:);
SwitchOpp2 = BendEdges([4 3 2 1],:);

j1b = [BendEdges(:); SwitchSame(:); SwitchOpp1(:);SwitchOpp2(:)];
s1b = [-ones(8*NumBendEdges,1);ones(8*NumBendEdges,1)]/(sqrt(3)*d/2)^2;
i1bVect = 3*ones(3,1)*i1b'+[-2;-1;0]*ones(size(i1b'));
j1bVect = 3*ones(3,1)*j1b'+[-2;-1;0]*ones(size(j1b'));
s1bVect = ones(3,1)*s1b';
FBendMat1 = sparse(i1bVect(:), j1bVect(:), s1bVect(:), 3*NumPts, 3*NumPts);
% Nathan revised a filename in order to be short and include a directory and 
% other features in filename (it eventually makes different files.)

% filename = strcat('/home/goroda/Data/CatalystGels/', ...
%     'Hx.Wv',num2str(wavenumber),'.Ks', num2str(KS),'.K', num2str(KCap), '.mu',...
%     num2str(mu),'.dt', num2str(dt),'.n',num2str(n),'.ty',num2str(type), '.mat');
% filename is revised to change long directory information to fpath. 
filename = strcat(fpath, ...
    'Hx.KCap',num2str(KCap),'.Ks',num2str(KS),'.Wv',...
    num2str(wavenumber),'.Ty', num2str(type),'.mat');

Moviefilename = strcat('Hx.Wv',num2str(wavenumber),'.Ks', num2str(KS),'.K', num2str(KCap), '.mu', ...
    num2str(mu), '.dt',num2str(dt), '.n',num2str(n),'.gif');

ImCt = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Add Pts, F_Bend, F_Stretch (x,y,z each) %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NoofTimestpes = 10; % I need data on 10 discrete time steps.
Pts_14to15sec_dt0_1 =zeros(NumPts*3,NoofTimestpes);
Pts_7to8sec_dt0_1 =zeros(NumPts*3,NoofTimestpes);
Pts_8to9sec_dt0_1 =zeros(NumPts*3,NoofTimestpes);
% FxBend_6to7sec_dt0_1 = zeros(NumPts,NoofTimestpes);
% FyBend_6to7sec_dt0_1 = zeros(NumPts,NoofTimestpes);
% FzBend_6to7sec_dt0_1 = zeros(NumPts,NoofTimestpes);
% FxStretch_6to7sec_dt0_1 = zeros(NumPts,NoofTimestpes);
% FyStretch_6to7sec_dt0_1 = zeros(NumPts,NoofTimestpes);
% FzStretch_6to7sec_dt0_1 = zeros(NumPts,NoofTimestpes);
TimeforData1 = ones(1,10)*700 + linspace(10,100,10);
TimeforData2 = ones(1,10)*800 + linspace(10,100,10);
TimeforData3 = ones(1,10)*1400 + linspace(10,100,10);       %I need 1410,1420, ... 1500 time step data.
% TimeforData = [TimeforData1, TimeforData2, TimeforData3]; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



for k1 = 1:maxsteps;
    
    % Radial Waves
    %deq is spacing on the mesh
    %x y location of midpoint
    % radial
    if type == 0
        deq = d*(1 + KCap*sin(2*pi*( ...
            (0.5*sqrt((Pts(3*Edges(1,:)-2,1) + Pts(3*Edges(2,:)-2,1)).^2 + ...
            (Pts(3*Edges(1,:)-1,1) + Pts(3*Edges(2,:)-1,1)).^2)) ...
            * wavenumber-k1*dt)));
        deqPts = d*(1 + KCap*sin(2*pi*(wavenumber*sqrt(Pts(1:3:end).^2+Pts(2:3:end).^2)-k1*dt)));
    elseif type == 1
        % Bottom two go together (Planar Waves)
        deq = d*(1 + KCap*sin(2*pi*(wavenumber*Pts(3*Edges(2,:)-1,1)-k1*dt)));
        deqPts = d*(1 + KCap*sin(2*pi*(wavenumber*Pts(2:3:end)-k1*dt)));
    else
        error("Type not recognized");
    end
        
    %deq = d*(1 + KCap*sin(2*pi*(0.5*sqrt((Pts(3*Edges(1,:)-2,1) + Pts(3*Edges(2,:)-2,1)).^2 + ...
    %(Pts(3*Edges(1,:)-1,1) + Pts(3*Edges(2,:)-1,1)).^2))));
    
    
    
    
    %xDiff = (Diff12StretchMat*Pts(1,:)')';
    %yDiff = (Diff12StretchMat*Pts(2,:)')';
    %zDiff = (Diff12StretchMat*Pts(3,:)')';
    xDiff = (Diff12StretchMat*Pts(1:3:end,1));
    yDiff = (Diff12StretchMat*Pts(2:3:end,1));
    zDiff = (Diff12StretchMat*Pts(3:3:end,1));
    
    DistEdges = sqrt(xDiff.^2 + yDiff.^2 + zDiff.^2);
    Stretch = DistEdges - deq;
    xStretchVals = -KS*Stretch.*xDiff./DistEdges;
    yStretchVals = -KS*Stretch.*yDiff./DistEdges;
    zStretchVals = -KS*Stretch.*zDiff./DistEdges;
    
    xStretchValsE = -KS*-deq.*xDiff./DistEdges;
    yStretchValsE = -KS*-deq.*yDiff./DistEdges;
    zStretchValsE = -KS*-deq.*zDiff./DistEdges;
    
    
    % %     xStretchMat1 = sparse(Edges(1,:), [1:NumEdges], xStretchVals, NumPts, NumEdges);
    % %     yStretchMat1 = sparse(Edges(1,:), [1:NumEdges], yStretchVals, NumPts, NumEdges);
    % %     zStretchMat1 = sparse(Edges(1,:), [1:NumEdges], zStretchVals, NumPts, NumEdges);
    % %     xStretchMat2 = sparse(Edges(2,:), [1:NumEdges], -xStretchVals, NumPts, NumEdges);
    % %     yStretchMat2 = sparse(Edges(2,:), [1:NumEdges], -yStretchVals, NumPts, NumEdges);
    % %     zStretchMat2 = sparse(Edges(2,:), [1:NumEdges], -zStretchVals, NumPts, NumEdges);
    % %
    % %     FxStretch = sum(xStretchMat1, 2) + sum(xStretchMat2, 2);
    % %     FyStretch = sum(yStretchMat1, 2) + sum(yStretchMat2, 2);
    % %     FzStretch = sum(zStretchMat1, 2) + sum(zStretchMat2, 2);
    %
    %     FxStretch = ValstoFStretchMat*xStretchVals';
    %     FyStretch = ValstoFStretchMat*yStretchVals';
    %     FzStretch = ValstoFStretchMat*zStretchVals';
    
    FxStretch = ValstoFStretchMat*xStretchVals;
    FyStretch = ValstoFStretchMat*yStretchVals;
    FzStretch = ValstoFStretchMat*zStretchVals;
    
    FxStretchE = ValstoFStretchMat*xStretchValsE;
    FyStretchE = ValstoFStretchMat*yStretchValsE;
    FzStretchE = ValstoFStretchMat*zStretchValsE;
    
    %[i1,j1,s1] = find(ValstoFStretchMat*...
    %        spdiags(-KS*Stretch./DistEdges, 0, NumEdges, NumEdges)*Diff12StretchMat);
    %     i1Vect = 3*ones(3,1)*i1'+[-2;-1;0]*ones(size(i1'));
    % j1Vect = 3*ones(3,1)*j1'+[-2;-1;0]*ones(size(j1'));
    %     s1Vect = ones(3,1)*s1';
    %     FStretchMat = sparse(i1Vect(:), j1Vect(:), s1Vect(:), 3*NumPts, 3*NumPts);
    
    %%%%FBend
    
    Diff12x = (Diff12BendMat*Pts(1:3:end,1));
    Diff12y = (Diff12BendMat*Pts(2:3:end,1));
    Diff12z = (Diff12BendMat*Pts(3:3:end,1));
    Length12Sqr = Diff12x.^2 + Diff12y.^2 + Diff12z.^2;
    
    Diff42x = (Diff42BendMat*Pts(1:3:end,1));
    Diff42y = (Diff42BendMat*Pts(2:3:end,1));
    Diff42z = (Diff42BendMat*Pts(3:3:end,1));
    
    r42Crossr12x = Diff42y.*Diff12z - Diff42z.*Diff12y;
    r42Crossr12y = Diff42z.*Diff12x - Diff42x.*Diff12z;
    r42Crossr12z = Diff42x.*Diff12y - Diff42y.*Diff12x;
    Normr42Crossr12Sqr = r42Crossr12x.^2 + r42Crossr12y.^2 + ...
        r42Crossr12z.^2;
    
    Diff32x = (Diff32BendMat*Pts(1:3:end,1));
    Diff32y = (Diff32BendMat*Pts(2:3:end,1));
    Diff32z = (Diff32BendMat*Pts(3:3:end,1));
    
    r32Crossr12x = Diff32y.*Diff12z - Diff32z.*Diff12y;
    r32Crossr12y = Diff32z.*Diff12x - Diff32x.*Diff12z;
    r32Crossr12z = Diff32x.*Diff12y - Diff32y.*Diff12x;
    Normr32Crossr12Sqr = r32Crossr12x.^2 + r32Crossr12y.^2 + ...
        r32Crossr12z.^2;
    CosTheta = (r42Crossr12x.*r32Crossr12x + ...
        r42Crossr12y.*r32Crossr12y + r42Crossr12z.*r32Crossr12z)./...
        sqrt(Normr42Crossr12Sqr)./sqrt(Normr32Crossr12Sqr);
    SinTheta = sqrt(1-min(max(CosTheta.^2,0),1));
    
    Sign4 = sign(r42Crossr12x.*-Diff32x + r42Crossr12y.*-Diff32y + ...
        r42Crossr12z.*-Diff32z);
    FBend4x = r42Crossr12x./Normr42Crossr12Sqr.*sqrt(Length12Sqr).*SinTheta.*Sign4;
    FBend4y = r42Crossr12y./Normr42Crossr12Sqr.*sqrt(Length12Sqr).*SinTheta.*Sign4;
    FBend4z = r42Crossr12z./Normr42Crossr12Sqr.*sqrt(Length12Sqr).*SinTheta.*Sign4;
    FBend4 = [FBend4x,FBend4y,FBend4z];
    %FBend4 = FBend4/100;
    
    Sign3 = sign(r32Crossr12x.*-Diff42x + r32Crossr12y.*-Diff42y + ...
        r32Crossr12z.*-Diff42z);
    FBend3x = r32Crossr12x./Normr32Crossr12Sqr.*sqrt(Length12Sqr).*SinTheta.*Sign3;
    FBend3y = r32Crossr12y./Normr32Crossr12Sqr.*sqrt(Length12Sqr).*SinTheta.*Sign3;
    FBend3z = r32Crossr12z./Normr32Crossr12Sqr.*sqrt(Length12Sqr).*SinTheta.*Sign3;
    FBend3 = [FBend3x,FBend3y,FBend3z];
    %FBend3 = FBend3/100;
    
    %     quiver3(Pts(1,BendEdges(3,:)), Pts(2,BendEdges(3,:)),Pts(3,BendEdges(3,:)),...
    %         FBend3(1,:)/1e2, FBend3(2,:)/1e2,FBend3(3,:)/1e2);
    %     quiver3(Pts(1,BendEdges(4,:)), Pts(2,BendEdges(4,:)),Pts(3,BendEdges(4,:)),...
    %         FBend4(1,:)/1e2, FBend4(2,:)/1e2,FBend4(3,:)/1e2);
    
    Length32Sqr = Diff32x.^2 + Diff32y.^2 + Diff32z.^2;
    R32ProjR12Length = sqrt(Length32Sqr - Normr32Crossr12Sqr./Length12Sqr).*...
        sign(Diff32x.*Diff12x+Diff32y.*Diff12y+Diff32z.*Diff12z);
    Length42Sqr = Diff42x.^2 + Diff42y.^2 + Diff42z.^2;
    R42ProjR12Length = sqrt(Length42Sqr - Normr42Crossr12Sqr./Length12Sqr).*...
        sign(Diff42x.*Diff12x+Diff42y.*Diff12y+Diff42z.*Diff12z);
    FBend1 = (-FBend3.*repmat(R32ProjR12Length,1,3) - ...
        FBend4.*repmat(R42ProjR12Length,1,3))./...
        repmat(sqrt(Length12Sqr),1,3);
    
    FBend2 = -FBend1-FBend3-FBend4;
    %%%%%%%%%%%%%%
    FBendVals = [FBend1; FBend2; FBend3; FBend4];
    
    FxBend = (ValstoFBendMat*FBendVals(:,1));
    FyBend = (ValstoFBendMat*FBendVals(:,2));
    FzBend = (ValstoFBendMat*FBendVals(:,3));
 
    
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%%%%% x1, y1, zHeight, FStretchnorm, FBendnorm calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    
    x1(k1) = Pts(1,1);
    y1(k1) = Pts(2,1);
    %zHeight(k1) = max(Pts(3,:))-min(Pts(3,:));
    zHeight(k1) = max(Pts(3:3:end,1))-min(Pts(3:3:end,1));
    FStretchnorm(k1) = norm(full([(FxStretch+FxStretchE), (FyStretch+FyStretchE),...
        (FzStretch+FzStretchE)]));
    FBendnorm(k1) = norm(full([FxBend, FyBend, FzBend]));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

    
    
    
    %     quiver3(Pts(1,BendEdges(1,:)), Pts(2,BendEdges(1,:)),Pts(3,BendEdges(1,:)),...
    %         FBend1(1,:)/1e2, FBend1(2,:)/1e2,FBend1(3,:)/1e2);
    %     quiver3(Pts(1,BendEdges(2,:)), Pts(2,BendEdges(2,:)),Pts(3,BendEdges(2,:)),...
    %         FBend2(1,:)/1e2, FBend2(2,:)/1e2,FBend2(3,:)/1e2);
    %     axis equal;
    

    
    
    
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%% Plotting statements are deactivated. (From Here) %%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    
    if mod(k1, PlotCt) == 0;
        ImCt = ImCt + 1;
        figure(14+(m~=0));clf;%hold on;
        
%         change figure size to full screen
%         set(gcf, 'Position',get(0,'Screensize'));

        % plot3(Pts(1,Edges), Pts(2,Edges), Pts(3,Edges),'k-');
        %plot3(Pts(3*Edges-2,1), Pts(3*Edges-1,1), Pts(3*Edges,1),'k-');
        
        subplot(3,2,[1,3]);
        trimesh(Faces, Pts(1:3:end,1), Pts(2:3:end,1), Pts(3:3:end,1));
        title('Color = z coordinate', 'interpreter', 'latex');
        xlabel('x');
        ylabel('y');
        zlabel('z');
        axis equal;
        view(3);%view([90 0]);
        
        subplot(3,2,[2,4]);
    
    
        trimesh(Faces, Pts(1:3:end,1), Pts(2:3:end,1), Pts(3:3:end,1),deqPts);
        xlabel('x');
        ylabel('y');
        zlabel('z');
        axis equal;
        view(3);
        title('Color = rest strain ($\alpha$), $0.2\sin(2\pi(r-t))$', 'interpreter', 'latex');
        
        subplot(3,2,5);
        plot(dt*[1:size(zHeight,2)], zHeight, 'k-');
        ylabel('\Delta z');
        xlabel('Time');
        title('Height ($\Delta z$)', 'interpreter', 'latex');
        subplot(3,2,6);
        hold on;
        plot(dt*[1:k1], log10(FStretchnorm), 'r-');
        plot(dt*[1:k1], log10(FBendnorm), 'b-');
        xlabel('Time');
        % NATHAN deactivated ylim function because a plot is beyond this limit.  
%         ylim([-3 2]);
        
        % NATHAN change the location of a legend above the plot   
%         legend({'$\log_{10}\|F_{stretch}\|$', '$\log_{10}\|F_{bend}\|$'},...
%             'interpreter', 'latex', 'FontSize', 12, 'position', [0.6 0.3 0.3 0.1]);
        legend({'$\log_{10}\|F_{stretch}\|$', '$\log_{10}\|F_{bend}\|$'},...
            'interpreter', 'latex', 'FontSize', 10, 'position', [0.7 0.35 0.25 0.08]);

        drawnow;
        savefig('actual12_20s')
        
                [A,map] = rgb2ind(frame2im(getframe(14)),256);
                if ImCt == 1
                    imwrite(A,map,Moviefilename,'gif','LoopCount',Inf,'DelayTime',0.2);
                else
                    imwrite(A,map,Moviefilename,'gif','WriteMode','append','DelayTime',0.2);
                end
        
        
        
                cmap1 = parula(50);
%                 plot3(Pts(3*Edges-2,1), Pts(3*Edges-1,1), Pts(3*Edges,1),'.', ...
%                     'Color', cmap1(ceil(Pts(3:3:end,1)/max(abs(Pts(3:3:end)),:))));
        
        
        
%                plot3(Pts(1,:), Pts(2,:), Pts(3,:),'r.');
%                quiver3(Pts(1:3:end,:)', Pts(2:3:end,:)', Pts(3:3:end,:)',FxStretch, FyStretch, FzStretch, 'r-');
%                quiver3(Pts(1:3:end,1), Pts(2:3:end,1), Pts(3:3:end,1),FxBend, FyBend, FzBend, 'b-');
        
    end
     %NATHAN add below function to save a figure       
        plotname1 = strcat('Hx.Wv',num2str(wavenumber),'.Ks', num2str(KS),'.K', num2str(KCap), '.mu',...
        num2str(mu),'.dt', num2str(dt),'.n',num2str(n),'.ty',num2str(type),'.1st', '.jpg');
        saveas(gcf,fullfile(fpath,plotname1),'jpeg')    
% 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%% Plotting statements are deactivated. (To Here) %%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        
        
        

    

    if m == 0
        %Pts(1:3:end,1) = Pts(1:3:end,1) + (1/mu)*dt*(FxStretch);
        %Pts(2:3:end,1) = Pts(2:3:end,1) + (1/mu)*dt*(FyStretch);
        %Pts(3:3:end,1) = Pts(3:3:end,1) + (1/mu)*dt*(FzStretch);
        
        %Pts = (speye(3*NumPts)-(1/mu)*dt*FStretchMat)\Pts;
        FSE = [FxStretchE,FyStretchE,FzStretchE]';
        FBE = [FxBend,FyBend,FzBend]';
        %Pts = (speye(3*NumPts)-(1/mu)*dt*FStretchMat)\(Pts+dt/mu*(FSE(:)+FBE(:)));
        %%Pts = Pts + dt/mu*(FSE(:)+FBE(:)+FStretchMat*Pts);
        if k1 == 1;
            PtsP1 = (speye(3*NumPts)-(1/mu)*dt*FStretchMat-(1/mu)*dt*FBendMat1)\...
                (Pts+dt/mu*(FSE(:)+FBE(:)-FBendMat1*Pts));
        else
            PtsP1 = ((3/2)*speye(3*NumPts)-(1/mu)*dt*FStretchMat-(1/mu)*dt*FBendMat1)\...
                (2*Pts-1/2*PtsM1+dt/mu*(2*FSE(:)-FSEM1(:)+2*FBE(:)-FBEM1(:)-FBendMat1*(2*Pts-PtsM1)));
        end
        PtsM1 = Pts;
        Pts = PtsP1;
        
        %%% Nathan add below if statement to save Pts, FBend and FStretch %%
        if find(TimeforData3 == k1)
            Pts_14to15sec_dt0_1(:,((k1/10)-140)) = Pts;
%             FxBend_6to7sec_dt0_1(:,((k1/10)-70)) = FxBend;
%             FyBend_6to7sec_dt0_1(:,((k1/10)-70)) = FyBend;
%             FzBend_6to7sec_dt0_1(:,((k1/10)-70)) = FzBend;
%             FxStretch_6to7sec_dt0_1(:,((k1/10)-70)) = FxStretch;
%             FyStretch_6to7sec_dt0_1(:,((k1/10)-70)) = FyStretch;
%             FzStretch_6to7sec_dt0_1(:,((k1/10)-70)) = FzStretch;
        end
        if find(TimeforData1 == k1)
            Pts_7to8sec_dt0_1(:,((k1/10)-70)) = Pts;
        end
        if find(TimeforData2 == k1)
            Pts_8to9sec_dt0_1(:,((k1/10)-80)) = Pts;
        end        
        
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        FSEM1 = FSE;
        FBEM1 = FBE;
        
%         a=1;  % a variable is not necessary for entire calculation...
    else
        Pts(1,:) = Pts(1,:) + dt*Vel(1,:);
        Pts(2,:) = Pts(2,:) + dt*Vel(2,:);
        Pts(3,:) = Pts(3,:) + dt*Vel(3,:);
        % Nathan add below if statement to save Pts, FBend and FStretch
        if find(TimeforData3 == k1)
            Pts_14to15sec_dt0_1(:,((k1/10)-140)) = Pts;
%             FxBend_6to7sec_dt0_1(:,((k1/10)-70)) = FxBend;
%             FyBend_6to7sec_dt0_1(:,((k1/10)-70)) = FyBend;
%             FzBend_6to7sec_dt0_1(:,((k1/10)-70)) = FzBend;
%             FxStretch_6to7sec_dt0_1(:,((k1/10)-70)) = FxStretch;
%             FyStretch_6to7sec_dt0_1(:,((k1/10)-70)) = FyStretch;
%             FzStretch_6to7sec_dt0_1(:,((k1/10)-70)) = FzStretch;
        end
        if find(TimeforData1 == k1)
            Pts_7to8sec_dt0_1(:,((k1/10)-70)) = Pts;
        end
        if find(TimeforData2 == k1)
            Pts_8to9sec_dt0_1(:,((k1/10)-80)) = Pts;
        end                
        
        
        
        Vel(1,:) = Vel(1,:)*(1-mu/m*dt) + (1/m)*dt*(FxStretch' + FxBend');
        Vel(2,:) = Vel(2,:)*(1-mu/m*dt) + (1/m)*dt*(FyStretch' + FyBend');
        Vel(3,:) = Vel(3,:)*(1-mu/m*dt) + (1/m)*dt*(FzStretch' + FzBend');
    end

    % Nathan moved below statements (if ..) to save all Pts
    if mod(k1, StopCt) == 0;
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%%%%% Plotting statements are deactivated. -From Here    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
                
        %return;
        figure(18+(m~=0));clf;
        subplot(1,2,1);hold on;
        %plot(x1, 'rx');
        plot(zHeight, 'gs');
        %plot(y1, 'bo');
        subplot(1,2,2);hold on;
        plot(log10(FStretchnorm), 'ro');
        plot(log10(FBendnorm), 'bo');
        drawnow;
        
        figure(1)
        subplot(1,7,[1:4]);
        trimesh(Faces, Pts(1:3:end,1), Pts(2:3:end,1), Pts(3:3:end,1));
%         title('', 'latex');
%         title('Color = z coordinate', 'interpreter', 'latex');
        xlabel('x','interpreter','latex');
        ylabel('y','interpreter','latex');
        zlabel('z','interpreter','latex');
        axis equal;
        view(3);%view([90 0]);
        
        subplot(1,7,[5,7]);
%         trimesh(Faces, Pts(1:3:end,1), Pts(2:3:end,1), Pts(3:3:end,1),deqPts);
%         xlabel('x');
%         ylabel('y');
%         zlabel('z');
%         axis equal;
%         view(3);
%         title('Color = rest strain ($\alpha$), $0.2\sin(2\pi(r-t))$', 'interpreter', 'latex');
        
%         subplot(3,2,5);
%         plot(dt*[1:size(zHeight,2)], zHeight, 'k-');
%         ylabel('\Delta z');
%         xlabel('Time');
%         title('Height ($\Delta z$)', 'interpreter', 'latex');
%         subplot(3,2,6);
        hold on;
        plot(dt*[1:k1], log10(FStretchnorm), 'r-');
        plot(dt*[1:k1], log10(FBendnorm), 'b-');
%         title('', 'latex');
        xlabel('Time','interpreter','latex');
        % NATHAN deactivated ylim function because a plot is beyond this limit.  
%         ylim([-3 2]);
        
        % NATHAN change the location of a legend above the plot   
%         legend({'$\log_{10}\|F_{stretch}\|$', '$\log_{10}\|F_{bend}\|$'},...
%             'interpreter', 'latex', 'FontSize', 12, 'position', [0.6 0.3 0.3 0.1]);
        legend({'$\log_{10}\|F_{stretch}\|$', '$\log_{10}\|F_{bend}\|$'},...
            'interpreter', 'latex', 'FontSize', 10, 'position', [0.7 0.35 0.25 0.08]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%%%%% Plotting statements are deactivated. -To Here    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

        % new dataset is necessary. 
        save(filename, 'FBendnorm', 'FStretchnorm', 'Pts_14to15sec_dt0_1', 'Pts_7to8sec_dt0_1', 'Pts_8to9sec_dt0_1');
%         save(filename, 'FxBend_6to7sec_dt0_1',...
%             'FyBend_6to7sec_dt0_1', 'FzBend_6to7sec_dt0_1',...
%             'FxStretch_6to7sec_dt0_1', 'FyStretch_6to7sec_dt0_1',...
%             'FzStretch_6to7sec_dt0_1', 'Pts_6to7sec_dt0_1');

%         % Nathan changed Pts to Pts_all to save entire points
%         save(filename, 'FStretchnorm', 'FBendnorm', 'zHeight', 'x1', 'y1', 'Pts_all', 'KB');
        
    end    
     
    
    %return;
end




% plot(x1, 'rx'); % x position of one point (Not necessary)

%NATHAN add below function to save a figure - Not Save the 2nd figure (Revised)
% plotname2 = strcat('Hx.Wv',num2str(wavenumber),'.Ks', num2str(KS),'.K', num2str(KCap), '.mu',...
%     num2str(mu),'.dt', num2str(dt),'.n',num2str(n),'.ty',num2str(type),'.2nd', '.jpg');
% saveas(gcf,fullfile(fpath,plotname2),'jpeg') 





% xDiff = (Pts(1,Edges(1,:)) - Pts(1,Edges(2,:)));
%     yDiff = (Pts(2,Edges(1,:)) - Pts(2,Edges(2,:)));
%     zDiff = (Pts(3,Edges(1,:)) - Pts(3,Edges(2,:)));

%      Diff12x = (Pts(1,BendEdges(1,:)) - Pts(1,BendEdges(2,:)));
%     Diff12y = (Pts(2,BendEdges(1,:)) - Pts(2,BendEdges(2,:)));
%     Diff12z = (Pts(3,BendEdges(1,:)) - Pts(3,BendEdges(2,:)));
%
%     Diff42x = (Pts(1,BendEdges(4,:)) - Pts(1,BendEdges(2,:)));
%     Diff42y = (Pts(2,BendEdges(4,:)) - Pts(2,BendEdges(2,:)));
%     Diff42z = (Pts(3,BendEdges(4,:)) - Pts(3,BendEdges(2,:)));
%
%     Diff32x = (Pts(1,BendEdges(3,:)) - Pts(1,BendEdges(2,:)));
%     Diff32y = (Pts(2,BendEdges(3,:)) - Pts(2,BendEdges(2,:)));
%     Diff32z = (Pts(3,BendEdges(3,:)) - Pts(3,BendEdges(2,:)));


%     R12CrossF1Mat = sparse([1:4:4*NumBendEdges,2:4:4*NumBendEdges], ...
%         [2:3:3*NumBendEdges,1:3:3*NumBendEdges], ...
%         [-Diff12z, Diff12z], 4*NumBendEdges, 3*NumBendEdges) + ...
%         sparse([1:4:4*NumBendEdges,3:4:4*NumBendEdges], ...
%         [3:3:3*NumBendEdges,1:3:3*NumBendEdges], [Diff12y,-Diff12y], ...
%         4*NumBendEdges, 3*NumBendEdges) + ...
%         sparse([2:4:4*NumBendEdges,3:4:4*NumBendEdges], ...
%         [3:3:3*NumBendEdges,2:3:3*NumBendEdges], [-Diff12x,Diff12x], ...
%         4*NumBendEdges, 3*NumBendEdges)+ ...
%         sparse([4:4:4*NumBendEdges,4:4:4*NumBendEdges,4:4:4*NumBendEdges],...
%         [1:3:3*NumBendEdges,2:3:3*NumBendEdges,3:3:3*NumBendEdges],...
%         [Diff12x,Diff12y,Diff12z],4*NumBendEdges, 3*NumBendEdges);
%     R32CrossF3Mat = sparse([1:4:4*NumBendEdges,2:4:4*NumBendEdges], ...
%         [2:3:3*NumBendEdges,1:3:3*NumBendEdges], ...
%         [-Diff32z, Diff32z], 4*NumBendEdges, 3*NumBendEdges) + ...
%         sparse([1:4:4*NumBendEdges,3:4:4*NumBendEdges], ...
%         [3:3:3*NumBendEdges,1:3:3*NumBendEdges], [Diff32y,-Diff32y], ...
%         4*NumBendEdges, 3*NumBendEdges) + ...
%         sparse([2:4:4*NumBendEdges,3:4:4*NumBendEdges], ...
%         [3:3:3*NumBendEdges,2:3:3*NumBendEdges], [-Diff32x,Diff32x], ...
%         4*NumBendEdges, 3*NumBendEdges);
%     R42CrossF4Mat = sparse([1:4:4*NumBendEdges,2:4:4*NumBendEdges], ...
%         [2:3:3*NumBendEdges,1:3:3*NumBendEdges], ...
%         [-Diff42z, Diff42z], 4*NumBendEdges, 3*NumBendEdges) + ...
%         sparse([1:4:4*NumBendEdges,3:4:4*NumBendEdges], ...
%         [3:3:3*NumBendEdges,1:3:3*NumBendEdges], [Diff42y,-Diff42y], ...
%         4*NumBendEdges, 3*NumBendEdges) + ...
%         sparse([2:4:4*NumBendEdges,3:4:4*NumBendEdges], ...
%         [3:3:3*NumBendEdges,2:3:3*NumBendEdges], [-Diff42x,Diff42x], ...
%         4*NumBendEdges, 3*NumBendEdges);
%
%     RhsVect = -R32CrossF3Mat*FBend3(:)-R42CrossF4Mat*FBend4(:);
%     F1Vect = R12CrossF1Mat\RhsVect;
%     FBend1a = [];
%     FBend1a(1,:) = F1Vect(1:3:end)';
%     FBend1a(2,:) = F1Vect(2:3:end)';
%     FBend1a(3,:) = F1Vect(3:3:end)';


% xBendMat1 = sparse(BendEdges(1,:), [1:NumBendEdges], FBend1(1,:), NumPts, NumBendEdges);
%     yBendMat1 = sparse(BendEdges(1,:), [1:NumBendEdges], FBend1(2,:), NumPts, NumBendEdges);
%     zBendMat1 = sparse(BendEdges(1,:), [1:NumBendEdges], FBend1(3,:), NumPts, NumBendEdges);
%     xBendMat2 = sparse(BendEdges(2,:), [1:NumBendEdges], FBend2(1,:), NumPts, NumBendEdges);
%     yBendMat2 = sparse(BendEdges(2,:), [1:NumBendEdges], FBend2(2,:), NumPts, NumBendEdges);
%     zBendMat2 = sparse(BendEdges(2,:), [1:NumBendEdges], FBend2(3,:), NumPts, NumBendEdges);
%     xBendMat3 = sparse(BendEdges(3,:), [1:NumBendEdges], FBend3(1,:), NumPts, NumBendEdges);
%     yBendMat3 = sparse(BendEdges(3,:), [1:NumBendEdges], FBend3(2,:), NumPts, NumBendEdges);
%     zBendMat3 = sparse(BendEdges(3,:), [1:NumBendEdges], FBend3(3,:), NumPts, NumBendEdges);
%     xBendMat4 = sparse(BendEdges(4,:), [1:NumBendEdges], FBend4(1,:), NumPts, NumBendEdges);
%     yBendMat4 = sparse(BendEdges(4,:), [1:NumBendEdges], FBend4(2,:), NumPts, NumBendEdges);
%     zBendMat4 = sparse(BendEdges(4,:), [1:NumBendEdges], FBend4(3,:), NumPts, NumBendEdges);
%
%     FxBend = sum(xBendMat1 + xBendMat2 + xBendMat3 + xBendMat4, 2);
%     FyBend = sum(yBendMat1 + yBendMat2 + yBendMat3 + yBendMat4, 2);
%     FzBend = sum(zBendMat1 + zBendMat2 + zBendMat3 + zBendMat4, 2);










%    r42Dotr21 = Diff21x.*Diff42x + Diff21y.*Diff42y;
%     Alt4x = Diff42x - (r42Dotr21./Length21Sqr).*Diff21x;
%     Alt4y = Diff42y - (r42Dotr21./Length21Sqr).*Diff21y;
%     NormAlt4 = sqrt(Alt4x.^2 + Alt4y.^2);
%     s4x = Alt4x./NormAlt4;
%     s4y = Alt4y./NormAlt4;
%
% Diff32x = (Pts(1,BendEdges(3,:)) - Pts(1,BendEdges(2,:)));
%     Diff32y = (Pts(2,BendEdges(3,:)) - Pts(2,BendEdges(2,:)));
%     r32Dotr21 = Diff21x.*Diff32x + Diff21y.*Diff32y;
%     Alt3x = Diff32x - (r32Dotr21./Length21Sqr).*Diff21x;
%     Alt3y = Diff32y - (r32Dotr21./Length21Sqr).*Diff21y;
%     NormAlt3 = sqrt(Alt3x.^2 + Alt3y.^2);
%
end