# README #

Welcome to the repository for source files of the paper "Inverse Design of Self-Oscillating Gels Through Deep Learning".
This repository has the source codes for training the neural networks mentioned in the publication. The inverse design architecture mainly relies on PyTorch machine learning library (more dependencies can be found in the respective files). 


### Contents ###

* Classification
* Regression
* Simulation Files

### Classification ###
Various python scripts using the PyTorch base are stored unter this folder for different purposes. The files without "pytorch" in the file name uses the scikit learn library for network creation and training. The networks mentioned in the publication are created using the files with "pytorch" in the file name.

* Files with "[x]instance" in the name uses x snapshots with uniform time intervals between them.
* "ClassificationPostprocess" files are used to create the figures in the publication.

### Regression ###
Various python scripts using the PyTorch base are stored unter this folder for different purposes. All files here use PyTorch library for network creation and training.  The networks mentioned in the publication are created using the file "CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py". Unlike classification, the library for regressor networks is stored on a separate file "Regresor.py". Other types of configurations (such as RNN or ensemble learning) can also be found under this folder.

### Simulation Files ###
The files for creating the simulation database can be found here. The simulation files are written in MATLAB.

### Related Repositories ###

* [Result Files](https://bitbucket.org/dorukaks/result_files/src/main/) - Statistics for the trained networks are provided here. The stats are packed/saved using pickle library.
* [Trained Networks](https://bitbucket.org/dorukaks/trained_networks/src/main/) - The networks trained using the presented architecture can be found here. The networks and the auxillary preprocessors (such as PCA and scalers) are packed/saved using PyTorch module.

* For further questions regarding this work, please contact to owner of this repository  (Doruk Aksoy)
* [Computational Autonomy Group](http://www.alexgorodetsky.com/index.html)



Copyright © 2020-2021 Doruk Aksoy

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
