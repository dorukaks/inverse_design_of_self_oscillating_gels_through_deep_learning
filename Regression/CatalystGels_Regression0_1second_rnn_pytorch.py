"""
Regression RNN trial 
Changelog:
v0 Created on Fri Jun 26 2020 15.40

Author: Doruk
"""

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import time
import pickle
import joblib
import torch
import torch.nn as nn
import torch.nn.functional as F
import Regresor as reg
from os import remove
#import matplotlib.cm as cm
#import pandas as pd

# from sklearn.pipeline import Pipeline
# from mpl_toolkits.mplot3d import Axes3D
# from matplotlib.font_manager import FontProperties
#form sklearn.decomposition import FastICA, KernelPCA, NMF
# from sklearn.cluster import KMeans
# from sklearn.metrics import silhouette_score, silhouette_samples

# from sklearn.neural_network import MLPRegressor, MLPClassifier
# from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA, IncrementalPCA,FastICA
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, r2_score, explained_variance_score, mean_squared_error

n_pca=np.array([40,50,60,70,80])
n_par=3
dms=["3rd","4th"]
test_dms=["5th","6th"]

rsq_score=np.zeros((len(dms),len(test_dms),len(n_pca),10))
ms_error=np.zeros((len(dms),len(test_dms),len(n_pca),10))
mean_percent_error=np.zeros((len(dms),len(test_dms),len(n_pca),10,n_par))
relative_mse=np.zeros((len(dms),len(test_dms),len(n_pca),10,n_par))
pca_scores=np.zeros((len(dms),len(n_pca)))
train_time=np.zeros((len(dms),len(n_pca)))
training_loss=[]

instances=10
split=True
double_mlp=True
drop=False
add_z=True
inv_tr=False

preprocess="pca"
pri_1="z"
pri_2="y"
sec_1="x"
typeind=1

# sonrasi icin bu bolum, su an kullanilmayacak
u=3000
v=1000
w=50
y=300
hidden_layer = [y,y,y]

start_time=time.time()

for train in range(len(dms)):
    if dms[train]=="3rd":
        train_sets=['71','72','73','74','75','76','77','78','79','80']
        test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90']]
        dm='3rd'
    elif dms[train]=="4th":
        train_sets=['61','62','63','64','65','66','67','68','69','70']
        test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90']]
        dm='4th'
    print('Iteration for Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
        +train_sets[-1][0]+'.'+train_sets[-1][1]+'s has started.')

    for iii in range(len(train_sets)):
        #burasi x datasini cekip kaydedecek, aynisinin tekrarini bu bitince y icin yap
        # print(f"x Load Loop #{iii+1}")
        matnamex=f"mat_contents_x{iii+1}"
        exec(matnamex+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_"+pri_1+train_sets[iii]+f"_type{typeind}')['CA_Gelsdata']")
        exec(matnamex+"="+matnamex+"[:6400]")   
        arrnamex=f"train_x{iii+1}"
        exec(arrnamex+"=np.zeros(("+matnamex+".shape[0],"+matnamex+"[0][0].shape[1]))")
        exec("for idx in range(len("+matnamex+")):"+arrnamex+"[idx]="+matnamex+"[idx][0]")
        exec("del "+matnamex)
        if iii != 0:
            exec("train_x1=np.append(train_x1,"+arrnamex+",axis=1)")
            exec("del "+arrnamex)
    # train_x1=train_x1.reshape(-1,3367)
    # save_name='unprocessed_x_train_10s.pckl'
    # save=open(save_name,'wb')
    # pickle.dump(train_x1,save)
    # save.close 
    # del train_x1
        
    for iii in range(len(train_sets)):
        #burasi y datasini cekip kaydedecek,
        # print(f"y Load Loop #{iii+1}")
        matnamey=f"mat_contents_y{iii+1}"
        exec(matnamey+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_"+pri_2+train_sets[iii]+f"_type{typeind}')['CA_Gelsdata']")
        exec(matnamey+"="+matnamey+"[:6400]")   
        arrnamey=f"train_y{iii+1}"
        exec(arrnamey+"=np.zeros(("+matnamey+".shape[0],"+matnamey+"[0][0].shape[1]))")
        exec("for idx in range(len("+matnamey+")):"+arrnamey+"[idx]="+matnamey+"[idx][0]")
        exec("del "+matnamey)
        if iii != 0:
            exec("train_y1=np.append(train_y1,"+arrnamey+",axis=1)")
            exec("del "+arrnamey)
    # train_y1=train_y1.reshape(-1,3367)
    # save_name='unprocessed_y_train_10s.pckl'
    # save=open(save_name,'wb')
    # pickle.dump(train_y1,save)
    # save.close
    # del train_y1

    params=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+f'DM_Parameters_type{typeind}')['CA_Gelsdata']
    params=params[:6400,:n_par].reshape(-1,n_par)
    # exec("paramscaler_"+dm+"=StandardScaler()")
    # exec("paramscaler_"+dm+".fit(params)")
    # exec("params=paramscaler_"+dm+".transform(params)")
    paramscaler=StandardScaler()
    paramscaler.fit(params)
    params=paramscaler.transform(params)
    train_all=np.append(np.append(train_x1,train_y1,axis=1),params,axis=1)
    del train_x1,train_y1,params
    np.random.shuffle(train_all)
    params=train_all[:,-n_par:]
    train_x,train_y=np.split(train_all[:,:-n_par],2,axis=1)
    del train_all
    train_x=train_x.reshape(-1,3367)
    save_name='unprocessed_x_train_10s.pckl'
    save=open(save_name,'wb')
    pickle.dump(train_x,save)
    save.close 
    del train_x
    train_y=train_y.reshape(-1,3367)
    save_name='unprocessed_y_train_10s.pckl'
    save=open(save_name,'wb')
    pickle.dump(train_y,save)
    save.close 
    del train_y

    # save_name=f"param_scaler_set{iii+1}.pckl"
    # save=open(save_name,'wb')
    # pickle.dump(paramscaler,save)
    # save.close
    # del paramscaler

    for idx in range(len(n_pca)):
   
        print(f'Iteration for #PC:{n_pca[idx]} has started...')     
        #burada x in pc transformationu yapiliyor
        save_name='unprocessed_x_train_10s.pckl'
        f=open(save_name,'rb')
        x_train=pickle.load(f)
        f.close
        pca_x=IncrementalPCA(n_components=n_pca[idx],batch_size=1000)
        x_train=pca_x.fit_transform(x_train)
        # save_name=f'x_pca{n_pca[idx]}_train{train+1}.pckl'
        # save=open(save_name,'wb')
        # pickle.dump(pca_x,save)
        # del pca_x
        
        scaler_x=StandardScaler()
        x_train=scaler_x.fit_transform(x_train)
        # save_name=f'x_scaler{n_pca[idx]}_train{train+1}.pckl'
        # save=open(save_name,'wb')
        # pickle.dump(scaler_x,save)
        # del scaler_x
        print("x done")

        save_name='unprocessed_y_train_10s.pckl'
        f=open(save_name,'rb')
        y_train=pickle.load(f)
        f.close
        pca_y=IncrementalPCA(n_components=n_pca[idx],batch_size=1000)
        y_train=pca_y.fit_transform(y_train)
        # save_name=f'y_pca{n_pca[idx]}_train{train+1}.pckl'
        # save=open(save_name,'wb')
        # pickle.dump(pca_y,save)

        scaler_y=StandardScaler()
        y_train=scaler_y.fit_transform(y_train)
        # save_name=f'y_scaler{n_pca[idx]}_train{train+1}.pckl'
        # save=open(save_name,'wb')
        # pickle.dump(pca_y,save)
        # pickle.dump([scaler_y,pca_y],save)
        # del pca_y
        # del scaler_y
        print("y done")

        train_all=np.append(x_train,y_train,axis=1)
        del x_train,y_train
        data_train=train_all.reshape(6400,10,-1)

        batch_size=250
        network=reg.LSTM(input_size=int(2*n_pca[idx]),hidden_layer_size=1000,output_size=3,num_layers=5)
        # print(network)
        network.train()
        criterion = nn.MSELoss().cuda()
        optimizer = torch.optim.Adam(network.parameters(),lr=1e-3)
        epochs=10
        network=network.cuda()

        batch_number=np.ceil(len(params)/batch_size).astype(int)
        # print(f"{batch_number} batches with {batch_size} elements")
        losses=[]
        print("LSTM training started...")
        network.train()
        inter_time=time.time()
        for i in range(epochs):
            current_batch=0
            batch_loss=[]
            while current_batch<batch_number:
                optimizer.zero_grad()
                network.hidden_cell = (torch.zeros(network.num_lstm_layers, 10, network.hidden_layer_size).cuda(),
                                     torch.zeros(network.num_lstm_layers, 10, network.hidden_layer_size).cuda())
                if current_batch<len(params)//batch_size:
                    param_batch=torch.from_numpy(params[current_batch*batch_size:(current_batch+1)*batch_size]).type(torch.cuda.FloatTensor)
                    loss=criterion(network.forward(data_train[current_batch*batch_size:(current_batch+1)*batch_size,:])\
                        ,param_batch)
                else:
                    param_batch=torch.from_numpy(params[current_batch*batch_size:]).type(torch.cuda.FloatTensor)
                    loss=criterion(network.forward(data_train[current_batch*batch_size:,:])\
                        ,param_batch)
                loss.backward(retain_graph=True)
                optimizer.step()
                batch_loss.append(loss.item())
                current_batch+=1
            losses.append(np.mean(batch_loss))
            if (i+1)%20==0: print("Epoch: ",i+1,"/",epochs," Current Training Loss: ",losses[i])
            if i+1>5 and abs(losses[-1])<1e-5:
                print("Terminiated at Epoch:",i+1,"/",epochs," Current Training Loss: ",losses[i])
                break

        train_time[train,idx]=time.time()-inter_time
        print(f'Neural network trained in {train_time[train,idx]}s.')
        network.eval()

        # print("predict oncesi")
        label_predict=np.zeros((params.shape[0],n_par))
        # print(label_predict.shape,params.shape)
        

        gpu_batch=100
        current_batch=0
        gpu_epoch=np.ceil(label_predict.shape[0]/gpu_batch)
        while current_batch<gpu_epoch:
            # if current_batch%10 or current_batch==gpu_epoch-1:
            #     print("gpu epoch: ",current_batch,"/",gpu_epoch)
            if current_batch<label_predict.shape[0]//gpu_batch:
                batch_predict=network.forward(data_train[current_batch*gpu_batch:(current_batch+1)*gpu_batch,:])
                label_predict[current_batch*gpu_batch:(current_batch+1)*gpu_batch,:]=batch_predict.detach().cpu().numpy()
            else:
                batch_predict=network.forward(data_train[current_batch*gpu_batch:,:])
                label_predict[current_batch*gpu_batch:,:]=batch_predict.detach().cpu().numpy()
            current_batch+=1
            del batch_predict
        
        # save_name=f"param_scaler_set{iii+1}.pckl"    
        # f=open(save_name,'rb')
        # paramscaler=pickle.load(f)
        # f.close
        label_predict=paramscaler.inverse_transform(label_predict)
        params=paramscaler.inverse_transform(params)

        mpe_1=np.mean((abs(label_predict-params)/params)*100,axis=0)
        print(f"LSTM network rsquared value       : {r2_score(params,label_predict)}")
        print(f'LSTM network Mean percentage error: {np.round(mpe_1,decimals=2)}%')
        params=paramscaler.transform(params)

        test_params=sio.loadmat\
                (f'/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Parameters_type{typeind}')['CA_Gelsdata']
        test_params=test_params[:,:n_par].reshape(-1,n_par)

        for test in range(len(test_sets)):
            for iii in range(len(test_sets[test])):
                matnamex="secondary_mat_contents_x"
                exec(matnamex+"=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_"+pri_1+test_sets[test][iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                arrnamex=f"test_x{iii+1}"
                exec(arrnamex+"=np.zeros(("+matnamex+".shape[0],"+matnamex+"[0][0].shape[1]))")
                exec("for i in range(len("+matnamex+")):"+arrnamex+"[i]="+matnamex+"[i][0]")
                exec("del "+matnamex)
                if iii != 0:
                    exec("test_x1=np.append(test_x1,"+arrnamex+",axis=1)")
                    exec("del "+arrnamex)
            test_x=test_x1.reshape(-1,3367)
            # save_name='unprocessed_x_train_10s.pckl'
            # save=open(save_name,'wb')
            # pickle.dump(test_x1,save)
            # save.close 
            del test_x1    
                # test_x=np.zeros((secondary_mat_contents_x.shape[0],secondary_mat_contents_x[0][0].shape[1]))
                # for ii in range(len(secondary_mat_contents_x)):test_x[ii]=secondary_mat_contents_x[ii][0]
                # del secondary_mat_contents_x

                

                # save_name=f'x_pca{n_pca[idx]}_train{train+1}.pckl' 
                # f=open(save_name,'rb')
                # pca_x=pickle.load(f)
                # f.close
                # save_name=f'x_scaler{n_pca[idx]}_train{train+1}.pckl'
                # f=open(save_name,'rb')
                # scaler_x=pickle.load(f)
                # f.close
            test_x=pca_x.transform(test_x)
            test_x=scaler_x.transform(test_x)
            print("test x done")


            for iii in range(len(test_sets[test])):    
                matnamey="secondary_mat_contents_y"
                exec(matnamey+"=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_"+pri_2+test_sets[test][iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                # test_y=np.zeros((secondary_mat_contents_y.shape[0],secondary_mat_contents_y[0][0].shape[1]))
                # for ii in range(len(secondary_mat_contents_y)):test_y[ii]=secondary_mat_contents_y[ii][0]
                # del secondary_mat_contents_y
                arrnamey=f"test_y{iii+1}"
                exec(arrnamey+"=np.zeros(("+matnamey+".shape[0],"+matnamey+"[0][0].shape[1]))")
                exec("for i in range(len("+matnamey+")):"+arrnamey+"[i]="+matnamey+"[i][0]")
                exec("del "+matnamey)
                if iii != 0:
                    exec("test_y1=np.append(test_y1,"+arrnamey+",axis=1)")
                    exec("del "+arrnamey)
            test_y=test_y1.reshape(-1,3367)
            del test_y1

                # save_name=f'y_pca{n_pca[idx]}_train{train+1}.pckl' 
                # f=open(save_name,'rb')
                # pca_y=pickle.load(f)
                # f.close
            test_y=pca_y.transform(test_y)

                # save_name=f'y_scaler{n_pca[idx]}_train{train+1}.pckl'
                # f=open(save_name,'rb')
                # scaler_y,_=pickle.load(f)
                # f.close
            test_y=scaler_y.transform(test_y)
            print("test y done")
                
            data_test=np.append(test_x,test_y,axis=1)
            data_test=data_test.reshape(-1,10,int(2*n_pca[idx]))

                # test_all_labeled=np.append(data_test,test_params,axis=1)
            del test_x,test_y
            param_predicted=network.forward(data_test).detach()
            param_predicted=param_predicted.cpu()
            param_predicted=param_predicted.numpy()
            del data_test

            # save_name=f"param_scaler_set{iii+1}.pckl"
            # f=open(save_name,'rb')
            # params=pickle.load(f)
            # f.close
            param_predicted=paramscaler.inverse_transform(param_predicted)
            rsq_score[train,test,idx,iii]=r2_score(test_params,param_predicted)
            ms_error[train,test,idx,iii]=mean_squared_error(test_params,param_predicted)
            mean_percent_error[train,test,idx,iii,:]=np.mean(abs(test_params-param_predicted)/test_params*100,axis=0)
            relative_mse[train,test,idx,iii,:]= np.mean((test_params-param_predicted)**2,axis=0)/np.mean(test_params**2,axis=0)
            print('Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
                +train_sets[-1][0]+'.'+train_sets[-1][1]+'s, Test t='+test_sets[test][iii][0]\
                    +'.'+test_sets[test][iii][1]+'s completed.')
            print('* Accuracy of the neural network: ',rsq_score[train,test,idx,iii])
            print('* Mean percentage error: ',np.round(mean_percent_error[train,test,idx,iii,:],decimals=2),'%')
            print('* Relative MSE: ',np.round(relative_mse[train,test,idx,iii,:],decimals=4))
            del data_train
            torch.cuda.empty_cache()

    del network, params,scaler_x,scaler_y,pca_x,pca_y        
print(f"execution of the whole code finised in {time.time()-start_time}s")
                










