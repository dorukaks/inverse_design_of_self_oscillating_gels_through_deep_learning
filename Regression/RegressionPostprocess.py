import pickle
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc, rcParams
import joblib
import torch
import torch.nn as nn
import torch.nn.functional as F
import scipy.io as sio
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import Regresor as reg
from math import floor,ceil

rc('text',usetex=True)
# matplotlib.use("pgf")
rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})


# deneme=np.zeros((3,5))
# deneme2=np.ones((2,6))
# train_sets=[]
# test_sets=[]
# n_pca=[]
# hidden_layer=[]
# train_time=[]
# accuracy_pca=[]
# training_loss=[]
# training_loss_sizes=[]
metrics=3
n_par=3
loc='desktop'
if loc=='desktop':
    path='dorukaks'
elif loc=='xps':
    path='doruk'
else:
    ValueError('Where are you running this code??')

#loading the necessary file for figure reproduction
pars=['Amplitude','Stiffness','Wavenumber']
hu_number=300
layershape=''
os.chdir("/home/"+path+"/Desktop/softgels/Bitbucket/catalystgelslearning/RegResults")
# parno=1
# parno-=1
case='gensearch'
for parno in np.arange(0,1):
    print(f"For "+pars[parno])
    if case=='layeropt':
        caseno=5
        layershape=''
    elif case=='unitopt':
        hu_number=[30,300,1000]
        caseno=len(hu_number)
    elif case=='gensearch':
        layers=4
        layershape=''
        for iii in range(layers):
            layershape=layershape+f'_{hu_number}'
        casetype=['pcsearch','NoPCA']
        caseno=len(casetype)
    for idx in range(caseno):
        if case=='layeropt':
            print(f'{idx+1} Layers {hu_number} Units')
            layershape=layershape+f'_{hu_number}'
            f=open(f'PCA_pytorch_pcasearch_10instance_split_{idx+1}layers'+layershape+'DoubleMLP.pckl','rb')

        elif case=='unitopt':
            print(f'1 Layers {hu_number[idx]} Units')
            f=open(f'PCA_pytorch_pcasearch_10instance_split_1layers_{hu_number[idx]}DoubleMLP.pckl','rb')

        elif case=='gensearch':
            caseidentifier=casetype[idx]
            print(f'{layers} Layers {hu_number} Units '+casetype[idx])
            f=open(f'PCA_pytorch_pcasearch_10instance_split_{layers}layers'+layershape+'DoubleMLP'+casetype[idx]+'.pckl','rb')
        
        
        train_sets,test_sets,n_pca,hidden_layer,n_par,train_time,ms_error,mpe_quantiles,\
            rsq_score,mean_percent_error,std_mpe,max_mpe,median_mpe,relative_mse,training_loss,instances=pickle.load(f)
        f.close
        print('Investigated principle components: ',n_pca)  
        # print(accuracy_pca.shape)
        # print(train_time.shape)
        anan=np.mean(median_mpe,axis=3)[0,0,:,:]
        baban=np.mean(max_mpe,axis=3)[0,0,:,:]
        orospu=np.mean(std_mpe,axis=3)[0,0,:,:]
        sikik=np.mean(mpe_quantiles,axis=3)[0,0,:,0,:]
        got=np.mean(mpe_quantiles,axis=3)[0,0,:,1,:]
        
        # print(np.mean(mpe_quantiles,axis=3)[0,0,:,:,:])
        print('MQPE 0.25:',np.round(sikik[:,parno],decimals=2),'%')
        print("MMPE     :",np.round(anan[:,parno],decimals=2))
        print('MQPE 0.75:',np.round(got[:,parno],decimals=2),'%')
        print("MaxPE    :",np.round(baban[:,parno],decimals=1))
        # print("StMPE    :",np.round(orospu[:,parno],decimals=4))
        print("TT       :",np.round(train_time[0],decimals=2))

        # print("median:",np.round(np.sum(anan,axis=1),decimals=4))
        # print("std   :",np.round(np.sum(orospu,axis=1),decimals=4))
        # print("max   :",np.round(np.sum(baban,axis=1),decimals=4))
        # print("traint:",np.round(train_time[0],decimals=2))
    # print()
        mean_loss=np.zeros((5,50))
        training_loss=np.array(training_loss)
        # print(training_loss.shape)
        # for idx in range(training_loss.shape[0]):
        #     if len(training_loss[idx])!=50:
        #         break
        # for idx in range(training_loss.shape[0]):
        #     if len(hidden_layer)==1:
        #         mean_loss[idx%5,:]=mean_loss[idx%5,:]+training_loss[idx,:] #for 1 layer
        #     elif len(training_loss[idx])!=50:
        #         training_loss[idx]=np.append(training_loss[idx],training_loss[idx][-1]*np.ones(50-len(training_loss[idx])))
        #         mean_loss[idx%5,:]=mean_loss[idx%5,:]+np.array(training_loss[idx])
        #     else:
        #         mean_loss[idx%5,:]=mean_loss[idx%5,:]+np.array(training_loss[idx])
        mean_loss=mean_loss/(training_loss.shape[0]/mean_loss.shape[0])
        # print(len(hidden_layer)," layer(s), ",hidden_layer[0]," units")
        # if instances==1:
        #     print('* Average Mean percentage error : \n', np.round(np.mean(np.mean(mean_percent_error,axis=2),axis=0),decimals=4))
        #     print('* Average Relative MSE          : \n',np.round(np.mean(np.mean(relative_mse,axis=2),axis=0),decimals=4))
        #     print('* Average R^2 score             : \n', np.round(np.mean(np.mean(rsq_score,axis=2),axis=0),decimals=4))
        #     print('* Average Training times        : \n',np.round(np.mean(train_time,axis=0),decimals=3))
        # else:
        #     print('* Average Mean percentage error : \n',np.round(np.mean(np.mean(np.mean(mean_percent_error,axis=0),axis=0),axis=1),decimals=4),'%')
        #     print('* Average Relative MSE          : \n',np.round(np.mean(np.mean(np.mean(relative_mse,axis=0),axis=0),axis=1),decimals=4))
        #     print('* Average R^2 score             : \n',np.round(np.mean(np.mean(np.mean(rsq_score,axis=0),axis=0),axis=1),decimals=4))
        #     print('* Average Training times        : \n',np.round(np.mean(train_time,axis=0),decimals=3))


        #loading the network model

        os.chdir("/home/"+path+"/Desktop/softgels/Bitbucket/catalystgelslearning")
        model=torch.load('MLPRegressor_pytorch_pcasearch_fbfs_10instance_split_train61s_pc60_4layers_MLPRegresor_yz_10fbfs_pcas_pca_200200_-_44_300300_DoubleMLPType2.pth.pth')

        # print(model)
        N_p=model['N_p']
        double_mlp=model['double_mlp']
        hidden_layer1,hidden_layer2=model['hidden_layer']
        network,second_network=model['network']
        try:
            split=model['split']
        except KeyError:
            split=False

        if split:
            fb_preprocess,fs_preprocess=model['force_preprocess']
            x_preprocessor,y_preprocessor=model['coord_preprocessor']
        else:
            preprocessor=model['pca']
        fbscaler,fsscaler=model['force_scaler']
        scaler=model['total_scaler']
        paramscaler=model['paramscaler']
        instances,force_instance=model['instances']
        network.eval()
        second_network.eval()
        # print(network)
        # print(second_network)

        time=71
        tresh=2
        mpe=np.array([[30,30,30]])
        while (mpe[0][0]>=tresh or mpe[0][1]>=tresh or mpe[0][2]>=tresh)  and time<91:
            print(f"time={time}s")
            typeinst=1
            secondary_mat_contents_x =\
                sio.loadmat(f'/home/'+path+f'/Desktop/data/5th_DM/CA_5thDM_R1_Part1_Pts_y{time}_type{typeinst}')\
                    ['CA_Gelsdata']
            secondary_mat_contents_y =\
                sio.loadmat(f'/home/'+path+f'/Desktop/data/5th_DM/CA_5thDM_R1_Part1_Pts_z{time}_type{typeinst}')\
                    ['CA_Gelsdata']
            secondary_fbfs =\
                sio.loadmat(f'/home/'+path+f'/Desktop/data/5th_DM/CA_5thDM_R1_Part1_FbFs_type{typeinst}')\
                    ['CA_Gelsdata']
            # secondary_fs =\
            #     sio.loadmat('/home/'+path+'/Desktop/data/5th_DM/CA_5thDM_R1_Part1_Pts_y71_type1')\
            #         ['CA_Gelsdata']
            secondary_params=\
                sio.loadmat(f'/home/'+path+f'/Desktop/data/5th_DM/CA_5thDM_R1_Part1_Parameters_type{typeinst}')\
                    ['CA_Gelsdata']
            # # Type = 0
            # # select=8648#beyhan
            # # select=6969#doruk
            # select=6986#ozun
            select=0
            # mpe=np.array([[30,30,30]])
            while  (mpe[0][0]>=tresh or mpe[0][1]>=tresh or mpe[0][2]>=tresh) and select<15000:
                # if (select==2 or select==6 or select ==24 or select ==60 or select==26) and time==71:
                if select==9915 or select==541 or select==7028: 
                    select+=1
                    if select==61:inverse
                        select+=1
                test_x=secondary_mat_contents_x[select][0]
                test_y=secondary_mat_contents_y[select][0]
                test_label=secondary_params[select,:n_par].reshape(-1,n_par)
                if test_label[0][2]>4 or test_label[0][0]<5e-02:
                    select+=1
                    continue
                test_fb=secondary_fbfs[select,0][0,time*10-force_instance:time*10].reshape(-1,force_instance)
                test_fs=secondary_fbfs[select,1][0,time*10-force_instance:time*10].reshape(-1,force_instance)
                test_fb=fbscaler.transform(test_fb)
                test_fs=fsscaler.transform(test_fs)
                test_fb=fb_preprocess.transform(test_fb)
                test_fs=fs_preprocess.transform(test_fs)
                test_x=x_preprocessor.transform(test_x)
                test_y=y_preprocessor.transform(test_y)
                test_data=np.append(test_x,test_y,axis=1)
                test_data=np.append(test_data,np.append(test_fb,test_fs,axis=1),axis=1)
                test_data=scaler.transform(test_data)
            
                test_predicted=network.predict(test_data,len(hidden_layer1)+2)
                test_predicted=test_predicted.cpu()
                test_predicted=test_predicted.numpy()
                if double_mlp:
                    test_data=np.append(test_data,test_predicted,axis=1)
                    test_predicted=second_network.predict(test_data,len(hidden_layer2)+2)
                test_predicted=paramscaler.inverse_transform(test_predicted)
                # test_label=paramscaler.inverse_transform(test_label)
                mpe=abs(test_label-test_predicted)/test_label*100
                select+=1
                if (select)%1000==0:
                    print(select)
            time+=1
        print(mpe)
        print(select-1)
        print(test_predicted)
        print(test_label)

        # print(secondary_mat_contents_x)
        # print(secondary_mat_contents_y.shape)
        # print(secondary_params[select,:n_par].reshape(-1,n_par))

        #Extract the data and create labels etc.#
        # secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
        # secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
        # secondary_test_data_x=np.zeros((secondary_mat_contents_x.shape[0],secondary_mat_contents_x[0][0].shape[1]))
        # secondary_test_data_y=np.zeros((secondary_mat_contents_y.shape[0],secondary_mat_contents_y[0][0].shape[1]))
        # #Extract the x points from the cells to a big array
        # for ii in range(len(secondary_mat_contents_x)):
        #     secondary_test_data_x[ii]=secondary_mat_contents_x[ii][0]
        #     secondary_test_data_y[ii]=secondary_mat_contents_y[ii][0]

        secondary_test_data_x=secondary_mat_contents_x[0]
        secondary_test_data_y=secondary_mat_contents_y[0]
            # if ii<len(secondary_mat_contents_1):
        # for ii in range(len(secondary_mat_contents_1)):
        #         secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]
        xsize=secondary_test_data_x[0].shape[1]
        secondary_test_data_all=np.append(secondary_test_data_x,secondary_test_data_y,axis=1)
        # secondary_test_label_all=np.append(secondary_test_label_x,secondary_test_label_y,axis=1)
        secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label,axis=1)

        #Shuffle the labeled data to get a "neutral" dataset#
        np.random.shuffle(secondary_test_shuffled)
        #Separate the labels and data part of the shuffled dataset#
        secondary_test_label=secondary_test_shuffled[:,-n_par:]
        secondary_test_data=secondary_test_shuffled[:,0:-n_par]
        secondary_test_label=secondary_test_label.astype(float)
        secondary_test_data=secondary_test_data.astype(float)

        # try:
        #     pca
        # except NameError:
        #     if 'ipcax' in locals():
        #         pca='ipca'
        #     else:
        #         pca='none'

        # if pca=='none':
        #     secondary_test_data_after_pca=secondary_test_data
        #     del secondary_test_data
        # else:
        #     if split:
        #         print("split")
        #         print(secondary_test_data[:,:xsize].shape)
        #         print(secondary_test_data[:,xsize:].shape)
        #         secondary_test_data_after_pca=ipcax.transform(secondary_test_data[:,:xsize])

        #         deneme=ipcay.transform(secondary_test_data[:,xsize:])

        #         secondary_test_data_after_pca=np.append(secondary_test_data_after_pca,deneme,axis=1)

        #         secondary_test_data_after_pca=secondary_test_data_after_pca[:,:2*N_p]
        #     else:
        #         secondary_test_data_after_pca = pca.transform(secondary_test_data)
        #         secondary_test_data_after_pca=secondary_test_data_after_pca[:,:N_p]
        # # secondary_test/_data_after_pca=scaler.transform(secondary_test_data_after_pca)
        # # # secondary_test_data_after_pca=torch.from_numpy(secondary_test_data_after_pca).type(torch.cuda.FloatTensor)
        # # secondary_label_predicted=network.predict(secondary_test_data_after_pca,len(hidden_layer)+2)
        # # secondary_label_predicted=secondary_label_predicted.cpu()
        # # secondary_label_predicted=secondary_label_predicted.numpy()

        # # print('actual parameters   :',paramscaler.inverse_transform(secondary_test_label))
        # # print('recovered parameters:',paramscaler.inverse_transform(secondary_label_predicted))
        # # print('error margin        :',abs(paramscaler.inverse_transform(secondary_test_label)-paramscaler.inverse_transform(secondary_label_predicted))/paramscaler.inverse_transform(secondary_test_label)*100)
        # # secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
        # # print("accuracy :",accuracy_score(secondary_test_label,secondary_label_predicted))


        # # plt.figure()
        # # plt.title('Training Loss vs Number of Epochs')
        # # for idx in range(len(training_loss)):
        # #     plt.plot(training_loss[idx][:],label="set "+str((idx//5)+1)+", NPca "+str(n_pca[(idx)%5]))
        # # plt.xlabel('Epoch')
        # # plt.ylabel('Training Loss')
        # # plt.legend()

        # # plt.figure()
        # # plt.title('Mean Training Loss vs Number of Epochs')
        # # for idx in range(mean_loss.shape[0]):
        # #     plt.plot(np.linspace(1,20,num=20),mean_loss[idx,:],label=str(n_pca[idx])+" PCs")
        # # plt.xlabel('Epoch')
        # # plt.ylabel('Training Loss')
        # # plt.xticks(np.linspace(2,20,num=10))
        # # plt.legend()

        # # plt.figure()
        # # plt.title('Mean Training Time vs Number of PCs')
        # # # for idx in range(train_time.shape[1]):
        # # plt.plot(np.array(n_pca),np.mean(train_time,axis=0),label="Training Time")
        # # plt.xlabel('Number of Principal Components')
        # # plt.ylabel('Training Time [s]')
        # # plt.xticks(n_pca)
        # # plt.legend()

        # # plt.figure()
        # # plt.title('Training Loss vs Number of Epochs')
        # # for idx in range(len(training_loss)):
        # #     plt.plot(training_loss[idx][:],label="set "+str((idx//5)+1)+", NPca "+str(n_pca[(idx)%5]))
        # # plt.xlabel('Epoch')
        # # plt.ylabel('Training Loss')
        # # plt.legend()
        # dms=train_sets
        # test_dms=test_sets
        line_colors=['r-o','b-v','g-^','c-s','m-p','k-*']

        rc('axes',labelsize=20)
        rc('axes',titlesize=25)
        rc('legend',fontsize=20)
        rc('ytick',labelsize=25)
        rc('xtick',labelsize=25)
        if instances==1:
            fig,axs=plt.subplots(1,len(train_sets),sharey=True)
            fig.suptitle('Mean Square Error for different number of PCs')
            for train in range(len(train_sets)):
                for pcs in range(len(n_pca)):
                        axs[train].plot(ms_error[train,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))      
                axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
                axs[train].set_xticklabels([])

            handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
            fig.legend(handles,labels,loc='lower right')
            # plt.xticks([])

            fig,axs=plt.subplots(1,len(train_sets),sharey=True)
            fig.suptitle('$R^2$ Score for different number of PCs')
            for train in range(len(train_sets)):
                for pcs in range(len(n_pca)):
                        axs[train].plot(rsq_score[train,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))    
                axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
                axs[train].set_xticklabels([])

            handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
            fig.legend(handles,labels,loc='lower right')
            # plt.xticks([])
            for idx in range(n_par):
                fig,axs=plt.subplots(1,len(train_sets),sharey=True)
                fig.suptitle('Mean Percentage error for parameter '+str(idx+1)+' with different number of PCs')
                for train in range(len(train_sets)):
                    for pcs in range(len(n_pca)):
                            axs[train].plot(mean_percent_error[train,pcs,:,idx],line_colors[pcs],label='PC='+str(n_pca[pcs]))
                    axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
                    axs[train].set_xticklabels([])

                handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
                fig.legend(handles,labels,loc='lower right')
            # plt.xticks([])
            plt.show()
        elif instances==10:

            # fig,axs=plt.subplots(len(dms),len(test_dms),sharex=True,sharey=True)
            # fig.suptitle('Mean Square Error for Different Number of PCs')

            # for train in range(len(dms)):
            #     for test in range(len(test_dms)):
            #         for pcs in range(len(n_pca)):
            #             axs[train][test].plot(ms_error[train,test,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))
            #         axs[train][test].set_xticklabels([])
            # axs[0][0].set_ylabel('7-8s Str')
            # axs[1][0].set_ylabel('6-7s Str')
            # # axs[2][2].set_xlabel('5th DM')
            # axs[1][0].set_xlabel('7-8s Rand')
            # axs[1][1].set_xlabel('8-9s Rand')
            # axs[1][2].set_xlabel('14-15s Rand')
            # # axs[2][0].set_ylabel('5th DM')
            # handles,labels=axs[1][1].get_legend_handles_labels()
            # fig.legend(handles,labels,loc='lower right')

            # fig,axs=plt.subplots(len(dms),len(test_dms),sharex=True,sharey=True)
            # fig.suptitle('$R^2$ Score for Different Number of PCs')
            # for train in range(len(dms)):
            #     for test in range(len(test_dms)):
            #         for pcs in range(len(n_pca)):
            #             axs[train][test].plot(rsq_score[train,test,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))
            #         axs[train][test].set_xticklabels([])
            # axs[0][0].set_ylabel('7-8s Str')
            # axs[1][0].set_ylabel('6-7s Str')
            # # axs[2][2].set_xlabel('5th DM')
            # axs[1][0].set_xlabel('7-8s Rand')
            # axs[1][1].set_xlabel('8-9s Rand')
            # axs[1][2].set_xlabel('14-15s Rand')
            # # axs[2][0].set_ylabel('5th DM')
            # handles,labels=axs[1][1].get_legend_handles_labels()
            # fig.legend(handles,labels,loc='lower right')

            # plt.xticks([])
            # print(mean_percent_error.shape)
            # print(len(train_sets))
            # print(len(test_sets))

            # fig,axs=plt.subplots(1,n_par,sharex=True,sharey=True)
            # # fig.suptitle('Mean Percentage error for different number of PCs')
            # for idx in range(n_par):
            #     for train in range(1):
            #         for test in range(1):
            #             for pcs in range(len(n_pca)):
            #                 axs[idx].plot(mean_percent_error[train,test,pcs,:,idx],line_colors[pcs],label='PC='+str(n_pca[pcs]))
            #             axs[idx].set_xticks([0,9,19])
            #             axs[idx].set_xticklabels(['$7.1s$','$8.0s$','$9.0s$'])
            #             axs[idx].xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(1))
            #             axs[idx].tick_params(which='major',width=2,length=4)
            #             axs[idx].tick_params(which='minor',width=1,length=2)
            # # axs[0][0].set_ylabel('7-8s Str')
            # axs[0].set_ylabel('Mean PE')
            # # axs[2][2].set_xlabel('5th DM')False
            # # axs[0].set_xlabel('7-8s Rand')
            # # axs[1].set_xlabel('8-9s Rand')
            # axs[0].set_title(pars[0])
            # axs[1].set_title(pars[1])
            # axs[2].set_title(pars[2])
            # # axs[2].set_xlabel('14-15s Rand')
            # # axs[2][0].set_ylabel('5th DM')False
            # handles,labels=axs[1].get_legend_handles_labels()
            # # fig.legend(handles,labels,loc='lower right')
            # fig.legend(handles,labels,bbox_to_anchor=(0.26, -0.03, .5, .102),ncol=6,mode="expand")
            
            
            train=0
            test=0
            rc('axes',labelsize=35)
            rc('axes',titlesize=35)
            rc('legend',fontsize=25)
            rc('ytick',labelsize=25)
            rc('xtick',labelsize=25)
            try :
                caseidentifier
            except NameError:
                caseidentifier=''
            if caseidentifier!='NoPCA':
                fig,axs=plt.subplots(metrics,n_par,sharex=True,sharey='row',figsize=(18,10))
            else:
                line_colors=['y--']
            
                
            # fig.suptitle('Regression Metrics ')
            for par in range(n_par):
                for test in range(1):
                    for pcs in range(len(n_pca)):
                        sikik=np.mean(mpe_quantiles,axis=3)[0,0,:,0,:]
                        if caseidentifier!='NoPCA':
                            axs[1][par].plot(median_mpe[train,test,pcs,:,par],line_colors[pcs],label='PC='+str(n_pca[pcs]))
                        else:
                            axs[1][par].plot(median_mpe[train,test,pcs,:,par],line_colors[pcs],label='No PC')
                        axs[0][par].plot(mpe_quantiles[train,test,pcs,:,0,par],line_colors[pcs],label='_nolegend_')
                        axs[2][par].plot(mpe_quantiles[train,test,pcs,:,1,par],line_colors[pcs],label='_nolegend_')
                        # axs[3][par].plot(max_mpe[train,test,pcs,:,par],line_colors[pcs],label='_nolegend_')
                        for axis in range(metrics):
                            axs[axis][par].set_xticks([0,9,19])
                            axs[axis][par].set_xticklabels(['$7.1s$','$8.0s$','$9.0s$'])
                            axs[axis][par].xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(1))
                            axs[axis][par].tick_params(which='major',width=2,length=4)
                            axs[axis][par].tick_params(which='minor',width=1,length=2)
            
            axs[1][0].set_ylim([floor(np.min(median_mpe)),ceil(np.max(median_mpe))])
            axs[0][0].set_ylim([floor(np.min(mpe_quantiles[:,:,:,:,0,:])),ceil(np.max(mpe_quantiles[:,:,:,:,0,:]))])
            axs[2][0].set_ylim([floor(np.min(mpe_quantiles[:,:,:,:,1,:]))-1,ceil(np.max(mpe_quantiles[:,:,:,:,1,:]))])
            # axs[3][par].set_ylim([floor(min(max_mpe)),ceil(max(max_mpe))])
            plt.setp(axs[1][0],yticks=np.linspace(floor(np.min(median_mpe)),ceil(np.max(median_mpe)),5,dtype=int))
            plt.setp(axs[0][0],yticks=np.linspace(floor(np.min(mpe_quantiles[:,:,:,:,0,:])),ceil(np.max(mpe_quantiles[:,:,:,:,0,:])),5))
            plt.setp(axs[2][0],yticks=np.linspace(floor(np.min(mpe_quantiles[:,:,:,:,1,:]))-1,ceil(np.max(mpe_quantiles[:,:,:,:,1,:])),6,dtype=int))
            
            plt.setp(axs[1][1],yticks=np.linspace(floor(np.min(median_mpe)),ceil(np.max(median_mpe)),5,dtype=int))
            plt.setp(axs[0][1],yticks=np.linspace(floor(np.min(mpe_quantiles[:,:,:,:,0,:])),ceil(np.max(mpe_quantiles[:,:,:,:,0,:])),5))
            plt.setp(axs[2][1],yticks=np.linspace(floor(np.min(mpe_quantiles[:,:,:,:,1,:]))-1,ceil(np.max(mpe_quantiles[:,:,:,:,1,:])),6,dtype=int))
            
            plt.setp(axs[1][2],yticks=np.linspace(floor(np.min(median_mpe)),ceil(np.max(median_mpe)),5,dtype=int))
            plt.setp(axs[0][2],yticks=np.linspace(floor(np.min(mpe_quantiles[:,:,:,:,0,:])),ceil(np.max(mpe_quantiles[:,:,:,:,0,:])),5))
            plt.setp(axs[2][2],yticks=np.linspace(floor(np.min(mpe_quantiles[:,:,:,:,1,:]))-1,ceil(np.max(mpe_quantiles[:,:,:,:,1,:])),6,dtype=int))
            axs[1][0].set_ylabel('Median')
            axs[0][0].set_ylabel('0.25 Quant')
            axs[2][0].set_ylabel('0.75 Quant')
            # axs[3][0].set_ylabel('Max')
            # axs[0].set_ylabel('6-7s Str')
            # axs[2][2].set_xlabel('5th DM')
            axs[0][0].set_title(pars[0])
            axs[0][1].set_title(pars[1])
            axs[0][2].set_title(pars[2])
            # axs[1].set_xlabel('8-9s Rand')
            # axs[2].set_xlabel('14-15s Rand')
            # axs[2][0].set_ylabel('5th DM')
            handles,labels=axs[1][0].get_legend_handles_labels()
            # fig.legend(handles,labels,loc='lower right')
            if caseidentifier=='NoPCA':
                fig.legend(handles,labels,bbox_to_anchor=(0.16, -0.015, .7, .102),ncol=7,mode="expand",handletextpad=0.1)
            # else:                
                # fig.legend(handles,labels,bbox_to_anchor=(0.16, -0.015, .7, .102),ncol=6,mode="expand",handletextpad=0.1)
            fig.align_ylabels()
            
            # for idx in range(n_par):
            #     fig,axs=plt.subplots(1,len(test_sets),sharex=True,sharey=True)
            #     fig.suptitle('Mean Percentage error for different number of PCs, Parameter '+str(idx+1))
            #     for train in range(len(train_sets)-1):
            #         for test in range(len(test_sets)):
            #             for pcs in range(len(n_pca)):
            #                 axs[test].plot(mean_percent_error[train,test,pcs,:,idx],line_colors[pcs],label='PC='+str(n_pca[pcs]))
            #                 if test==0:
            #                     axs.set_xticks(np.array([0,19]))
            #                     axs.set_xticklabels(['$7.1s$','$9.0s$'])
            #                 else:
            #                     axs.set_xticks(np.array([0,19]))
            #                     axs.set_xticklabels(['$8.1s$','$9.0s$'])
                        # axs.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(1))
                        # axs.tick_params(which='major',width=2,length=4)
                        # axs.tick_params(which='minor',width=1,length=2)
            #     # axs[0][0].set_ylabel('7-8s Str')
            #     axs.set_ylabel('6-7s Grid Set')
            #     # axs[2][2].set_xlabel('5th DM')
            #     axs.set_title('7-8s Random Set')
            #     # axs[1].set_title('8-9s Random Set')
            #     # axs[1][2].set_xlabel('14-15s Rand')
            #     # axs[2][0].set_ylabel('5th DM')
            #     handles,labels=axs[1].get_legend_handles_labels()
            #     fig.legend(handles,labels,bbox_to_anchor=(0.26, -0.03, .5, .102),ncol=6,mode="expand")
            #     # fig.legend(handles,labels,loc='lower right')
# plt.show()


# for train in range(2):
#     for test in range(2):
#         axs[train][test].plot(accuracy_pca[train,test,0,:],'r-o',label='PC='+str(n_pca[0]))
#         axs[train][test].plot(accuracy_pca[train,test,1,:],'b-o',label='PC='+str(n_pca[1]))
#         axs[train][test].plot(accuracy_pca[train,test,2,:],'g-o',label='PC='+str(n_pca[2]))
#         axs[train][test].plot(accuracy_pca[train,test,3,:],'c-o',label='PC='+str(n_pca[3]))
#         # axs[train][test].plot(accuracy_pca[train,test,4,:],'m-o',label='PC=3367')
#         # axs[train][test].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s & '+train_sets[train+shift][0]+\
#         #     '.'+train_sets[train+shift][1]+'s & '+train_sets[train+2*shift][0]+'.'+train_sets[train+2*shift][1]+'s')
#         axs[train][test].set_xticklabels([])
# axs[1][0].set_xlabel('7-8s Str')
# axs[1][1].set_xlabel('6-7s Str')
# # axs[2][2].set_xlabel('5th DM')
# axs[0][0].set_ylabel('7-8s Rand')
# axs[1][0].set_ylabel('8-9s Rand')
# # axs[2][0].set_ylabel('5th DM')
# handles,labels=axs[1][1].get_legend_handles_labels()
# fig.legend(handles,labels,loc='lower right')
# # plt.xticks([])
# plt.show()
