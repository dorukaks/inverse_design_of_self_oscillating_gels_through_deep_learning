"""
Classification trial 
Changelog:
v0 Created on Fri Mar 20 2020 14.40

Author: Doruk
"""
#This python file is created to do the type classification of the simulation
#results based on the previous classifiers (on 1st dataset) written by DK

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import time
import pickle
import joblib
import torch
import torch.nn as nn
import torch.nn.functional as F
import Regresor as reg
from os import remove
#import matplotlib.cm as cm
#import pandas as pd

# from sklearn.pipeline import Pipeline
# from mpl_toolkits.mplot3d import Axes3D
# from matplotlib.font_manager import FontProperties
#form sklearn.decomposition import FastICA, KernelPCA, NMF
# from sklearn.cluster import KMeans
# from sklearn.metrics import silhouette_score, silhouette_samples

# from sklearn.neural_network import MLPRegressor, MLPClassifier
# from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, r2_score, explained_variance_score, mean_squared_error

# 3rd dm = 71-80
# 4th dm = 61-70
# 5th dm = 71-90, 141-150
mean_squared_error
# n_pca=np.array([5,10,15,20,25])
n_pca=np.array([12,16,18,20,22])
n_par=3
dms=["4th"]
test_dms=["5th","6th"]
# test_dms=["5th","6th","7th"]
# dms=["3rd","4th","5th"]
# shift=3 #make the training set 0.3s apart
rsq_score=np.zeros((len(dms),len(test_dms),len(n_pca),10))
ms_error=np.zeros((len(dms),len(test_dms),len(n_pca),10))
mean_percent_error=np.zeros((len(dms),len(test_dms),len(n_pca),10,n_par))
relative_mse=np.zeros((len(dms),len(test_dms),len(n_pca),10,n_par))
pca_scores=np.zeros((len(dms),len(n_pca)))
train_time=np.zeros((len(dms),len(n_pca)))
training_loss=[]
instances=10
split=True
force_instance=10
preprocess="pca"
steps=9

if split:
    splitstatement="split"
else:
    splitstatement="nosplit"
typeind=1

start_time=time.time()

for train in range(len(dms)):
    training_interval=train+4
    if training_interval==3:
        train_sets=['71','72','73','74','75','76','77','78','79','80']
        test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90'],['141','142','143','144','145','146','147','148','149','150']]
        dm='3rd'
    elif training_interval==4:
        train_sets=['61','62','63','64','65','66','67','68','69','70']
        test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90']]
        dm='4th'
    else:
        train_sets=['141','142','143','144','145','146','147','148','149','150']
        test_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70']]
        dm='5th'

    print('Iteration for Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
        +train_sets[-1][0]+'.'+train_sets[-1][1]+'s has started.')

    params=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Parameters_type'+str(typeind))['CA_Gelsdata']
    params=params[:6400,:n_par].reshape(-1,n_par)
    paramscaler=StandardScaler()
    paramscaler.fit(params)
    params=paramscaler.transform(params)

    fbfsbulk=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_FbFs_type'+str(typeind))['CA_Gelsdata']
    trinst=np.array([int(item+'0') for item in train_sets])
    fb=fbfsbulk[0,0]
    fs=fbfsbulk[0,1]
    for idx in range(1,6400):
        fb=np.append(fb,fbfsbulk[idx,0][:].reshape(1,-1),axis=0)
        fs=np.append(fs,fbfsbulk[idx,1][:].reshape(1,-1),axis=0)
    print(fb.shape,fs.shape)
    del fbfsbulk
    if force_instance==1:
        fbtrain=fb[:,trinst[0]].reshape(-1,force_instance)
        fstrain=fb[:,trinst[0]].reshape(-1,force_instance)
        for idx in range(1,len(trinst)):
            fbtrain=np.append(fbtrain,fb[:,trinst[idx]-1].reshape(-1,1),axis=0) 
            fstrain=np.append(fstrain,fs[:,trinst[idx]-1].reshape(-1,1),axis=0)
    else:
        fbtrain=fb[:,trinst[0]-force_instance:trinst[0]].reshape(-1,force_instance)
        fstrain=fs[:,trinst[0]-force_instance:trinst[0]].reshape(-1,force_instance)
        for idx in range(1,len(trinst)):
            fbtrain=np.append(fbtrain,fb[:,trinst[idx]-force_instance:trinst[idx]].reshape(-1,force_instance),axis=0) 
            fstrain=np.append(fstrain,fs[:,trinst[idx]-force_instance:trinst[idx]].reshape(-1,force_instance),axis=0)
    print(fbtrain.shape,fstrain.shape)
    del fb,fs

    fbscaler=StandardScaler()
    fsscaler=StandardScaler()
    fb_scaled=fbscaler.fit_transform(fbtrain) 
    fs_scaled=fsscaler.fit_transform(fstrain)

    
    epochs = 100
    batch_size=50
    fbfs_number=np.ceil(fb_scaled.shape[0]/batch_size).astype(int)

    AE_fb=reg.AutoEncoder(fb_scaled.shape[1]).cuda()
    fb_optimizer = torch.optim.Adam(AE_fb.parameters(),lr=0.001)
    fb_criterion = nn.MSELoss().cuda()
    AE_fb.train()
    autoencoder_time=time.time()
    for i in range(epochs):
        current_batch=0
        loss1sayi=[]
        while current_batch<fbfs_number:
            fb_optimizer.zero_grad()
            if current_batch<fb_scaled.shape[0]//batch_size:
                fb_batch=fb_scaled[current_batch*batch_size:(current_batch+1)*batch_size]
                # fb_batch=torch.from_numpy(fb_scaled[current_batch*batch_size:(current_batch+1)*batch_size]).type(torch.cuda.FloatTensor)
                # anan=AE_fb.forward(fb_scaled[current_batch*batch_size:(current_batch+1)*batch_size,:])

                # anan=AE_fb.forward(fb_batch)
                # loss1=fb_criterion(anan,torch.from_numpy(fb_batch).type(torch.cuda.FloatTensor))
                loss1=fb_criterion(AE_fb.forward(fb_batch),torch.from_numpy(fb_batch).type(torch.cuda.FloatTensor))
            else:
                fb_batch=fb_scaled[current_batch*batch_size:,:]
                # fb_batch=torch.from_numpy(fb_scaled[current_batch*batch_size:]).type(torch.cuda.FloatTensor)
                anan=AE_fb.forward(fb_batch)
                # anan=AE_fb.forward(fb_scaled[current_batch*batch_size:,:])
                loss1=fb_criterion(anan,torch.from_numpy(fb_batch).type(torch.cuda.FloatTensor))
            loss1sayi.append(loss1.item())
            loss1.backward()
            fb_optimizer.step()
            current_batch+=1
        if (i+1)%5==0: print("Epoch: ",i+1,"/",epochs," Current AE Loss: ",np.mean(loss1sayi))
        if i+1>5 and np.mean(loss1sayi)<1e-4:
            print("Terminiated at Epoch:",i+1,"/",epochs," Current AE Losses: ",np.mean(loss1sayi))
            break
    del fb_batch

    AE_fs=reg.AutoEncoder(fs_scaled.shape[1]).cuda()
    fs_optimizer = torch.optim.Adam(AE_fs.parameters(),lr=0.001)
    fs_criterion = nn.MSELoss().cuda()
    AE_fs.train()
    for i in range(epochs):
        current_batch=0
        while current_batch<fbfs_number:            
            fs_optimizer.zero_grad()
            if current_batch<fs_scaled.shape[0]//batch_size: 
                fs_batch=fs_scaled[current_batch*batch_size:(current_batch+1)*batch_size]
                # fs_batch=torch.from_numpy(fs_scaled[current_batch*batch_size:(current_batch+1)*batch_size]).type(torch.cuda.FloatTensor)
                baban=AE_fs.forward(fs_batch)
                # baban=AE_fs.forward(fs_scaled[current_batch*batch_size:(current_batch+1)*batch_size,:])
                loss2=fs_criterion(baban,torch.from_numpy(fs_batch).type(torch.cuda.FloatTensor))
            else:
                fs_batch=fs_scaled[current_batch*batch_size:,:]
                # fs_batch=torch.from_numpy(fs_scaled[current_batch*batch_size:]).type(torch.cuda.FloatTensor)
                baban=AE_fs.forward(fs_batch)
                # baban=AE_fs.forward(fs_scaled[current_batch*batch_size:,:])
                loss2=fs_criterion(baban,torch.from_numpy(fs_batch).type(torch.cuda.FloatTensor))
            loss2sayi=loss2.item()
            loss2.backward()
            fs_optimizer.step()
            current_batch+=1

        if (i+1)%5==0: print("Epoch: ",i+1,"/",epochs," Current AE Loss: ",loss2sayi)
        if i+1>5 and loss2.item()<1e-4:
            print("Terminiated at Epoch:",i+1,"/",epochs," Current AE Losses: ",loss2.item())
            break
    del fs_batch
    print('AE train time ',time.time()-autoencoder_time,'s')
    AE_fb.eval()
    AE_fs.eval()
    print('training errors: ',mean_squared_error(AE_fb.forward(fb_scaled).cpu().detach(),fb_scaled),\
        mean_squared_error(AE_fs.forward(fs_scaled).cpu().detach(),fs_scaled))
    fb_scaled=AE_fb.transform(fb_scaled)
    fs_scaled=AE_fs.transform(fs_scaled)

    save_name='ae_fb.pckl'
    save=open(save_name,'wb')
    pickle.dump(AE_fb,save)
    save.close
    save_name='ae_fs.pckl'
    save=open(save_name,'wb')
    pickle.dump(AE_fs,save)
    save.close

    del fb_criterion,fs_criterion,fb_optimizer,fs_optimizer,AE_fb,AE_fs
    torch.cuda.empty_cache()

    # for idx in range(n_par):
    #     coefname="coef"+str(idx)
    #     exec(coefname+"=max(params[:,"+str(idx)+"])")
    #     exec("params[:,"+str(idx)+"]=params[:,"+str(idx)+"]/"+coefname)

    #Load files and extract the cell structures from files
    #For Windows
    # mat_contents_t0_p1=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type1')['CA_Gelsdata']
    # mat_contents_t1_p1=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type2')['CA_Gelsdata']
    # mat_contents_t0_p2=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+shift]+'_type1')['CA_Gelsdata']
    # mat_contents_t1_p2=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+shift]+'_type2')['CA_Gelsdata']
    # mat_contents_t0_p3=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+2*shift]+'_type1')['CA_Gelsdata']
    # mat_contents_t1_p3=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+2*shift]+'_type2')['CA_Gelsdata']
    #For Ubuntu
    # Certainly we need to find a better way to do this....
    for iii in range(len(train_sets)):
        print("data load loop #",iii+1)
        matnamex="mat_contents_x"+str(iii+1)
        matnamey="mat_contents_y"+str(iii+1)
        # matnamez="mat_contents_z"+str(iii+1)
        if dm!="5th":
            exec(matnamex+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_z"+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
            exec(matnamey+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_y"+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")         
            # exec(matnamez+"=sio.loadmat\
            # ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_z"+train_sets[iii]+"_type1')['CA_Gelsdata']")                   
        else:
            exec(matnamex+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_R1_Part1_Pts_z"+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
            exec(matnamey+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_R1_Part1_Pts_y"+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")    
            # exec(matnamez+"=sio.loadmat\
            # ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_R1_Part1_Pts_z"+train_sets[iii]+"_type1')['CA_Gelsdata']")    
            
            # exec(matname0+"=sio.loadmat\
            # ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_x"+train_sets[iii]+"_type1')['CA_Gelsdata']")
            # exec(matname1+"=sio.loadmat\
            # ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_x"+train_sets[iii]+"_type1')['CA_Gelsdata']")
        # exec("print("+matnamex+".shape,"+matnamey+".shape)")
        exec(matnamex+"="+matnamex+"[:6400]")
        exec(matnamey+"="+matnamey+"[:6400]")
        # exec(matnamez+"="+matnamez+"[:6400]")

        # labelname="train_label"+str(iii+1)
        # labelname1="train_label_1_p"+str(iii+1)
        # exec(labelname+"=np.zeros(mat_contents_x"+str(iii+1)+".shape)")
        # exec(labelnamey+"=np.ones(mat_contents_y"+str(iii+1)+".shape)")

        arrnamex="train_x"+str(iii+1)
        arrnamey="train_y"+str(iii+1)
        # arrnamez="train_z"+str(iii+1)
        exec(arrnamex+"=np.zeros(("+matnamex+".shape[0],"+matnamex+"[0][0].shape[1]))")
        exec(arrnamey+"=np.zeros(("+matnamey+".shape[0],"+matnamey+"[0][0].shape[1]))")
        # exec(arrnamez+"=np.zeros(("+matnamez+".shape[0],"+matnamez+"[0][0].shape[1]))")
        exec("for idx in range(len("+matnamex+")):"+arrnamex+"[idx]="+matnamex+"[idx][0]")
        exec("for idx in range(len("+matnamey+")):"+arrnamey+"[idx]="+matnamey+"[idx][0]")
        # exec("for idx in range(len("+matnamez+")):"+arrnamez+"[idx]="+matnamez+"[idx][0]")
        # exec("print("+arrnamex+".shape,"+arrnamey+".shape)")
        # arrcombined="arrcombined_p"+str(iii+1)
        all_labeledname="train_all_labeled_p"+str(iii+1)
        exec("del "+matnamex+","+matnamey)
        exec("x_size="+arrnamex+".shape[1]")
        exec(all_labeledname+"=np.append("+arrnamex+","+arrnamey+",axis=1)")
        exec("del "+arrnamex+","+arrnamey)

        # exec(all_labeledname+"=np.append("+all_labeledname+","+arrnamez+",axis=1)")
        # exec("del "+matnamez+","+arrnamez)

        print(params.shape)
        # all_arrname="train_all_p"+str(iii+1)
        # all_labelname="label_train_all_p"+str(iii+1)
        exec(all_labeledname+"=np.append("+all_labeledname+",params,axis=1)")
        # exec("print("+all_labeledname+".shape)")
        # exec("del "+arrcombined)
        # exec(all_labelname+"=np.append("+labelname0+","+labelname1+",axis=0)")
        # exec(all_labeledname+"=np.append("+all_arrname+","+all_labelname+",axis=1)")
        # exec("del "+all_arrname)
        exec("np.random.shuffle("+all_labeledname+")")
        # shuffled_labelname="x_label_shuffled_p"+str(iii+1)
        # shuffled_labelname="x_train_p"+str(iii+1)
        # shuffled_dataname="x_train_shuffled_p"+str(iii+1)
        # shuffled_dataname="x_label_train_p"+str(iii+1)
        training_data="train_p"+str(iii+1)
        training_label="label_train_p"+str(iii+1)
        exec(training_label+"="+all_labeledname+"[:,-"+str(n_par)+":]")
        exec(training_data+"="+all_labeledname+"[:,:-"+str(n_par)+"]")
        exec("del "+all_labeledname)
        exec("print("+training_label+".shape,"+training_data+".shape)")
        exec(training_label+"="+training_label+".astype(float)")
        exec(training_data+"="+training_data+".astype(float)")


        # training_data="x_train_p"+str(iii+1)
        # testing_data="x_test_p"+str(iii+1)
        # training_label="x_label_train_p"+str(iii+1)
        # testing_label="x_label_test_p"+str(iii+1)
        # exec(training_data+","+testing_data+","+training_label+","+testing_label+"=train_test_split("+shuffled_dataname+","+shuffled_labelname+",test_size=0.30)")
        # exec(training_data+"="+shuffled_dataname)
        # exec(training_label+"="+shuffled_labelname)
        # exec("del "+shuffled_dataname+","+shuffled_labelname)

    # print("line ",145)
    data_train=train_p1
    del train_p1
    data_label=label_train_p1
    del label_train_p1

    #Found it, lol
    for iii in range(len(train_sets)-1):
        # print("append loop no ",iii)
        # print("size of x_label: ",x_label.shape)
        # print("size of x_train: ",x_train.shape)
        train_k="train_p"+str(iii+2)
        train_all="data_train"
        label_k="label_train_p"+str(iii+2)
        label_all="data_label"
        exec(train_all+"=np.append("+train_all+","+train_k+",axis=0)")
        exec(label_all+"=np.append("+label_all+","+label_k+",axis=0)")
        exec("del "+train_k+","+label_k)
    data_label=np.reshape(data_label,(data_train.shape[0],n_par))

    split_len=x_size
    print('data train shape:',data_train.shape)
    print('data label shape:',data_label.shape)
    data_train_all=np.append(data_train,data_label,axis=1)
    np.random.shuffle(data_train_all)
    data_train=data_train_all[:,0:-n_par]
    data_label_train=data_train_all[:,-n_par:]

    save_name='temp_training_data.pckl'
    data_size=data_train.shape[0]
    save1=open('temp_training_data1.pckl','wb')
    pickle.dump(data_train[:data_size//2,:],save1)
    save1.close
    save2=open('temp_training_data2.pckl','wb')
    pickle.dump(data_train[data_size//2:,:],save2)
    save2.close
    
    # for "kisaltma icin yapildi" in range(2):
        # mat_contents_t0_p1=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[0]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p1=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[0]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p2=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[1]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p2=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[1]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p3=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[2]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p3=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[2]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p4=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[3]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p4=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[3]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p5=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[4]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p5=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[4]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p6=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[5]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p6=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[5]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p7=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[6]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p7=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[6]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p8=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[7]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p8=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[7]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p9=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[8]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p9=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[8]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p10=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[9]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p10=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[9]+'_type2')['CA_Gelsdata']

        # for xxx in range():
            #buraya oyle bir dongu yaz ki butun datayi tekte kompakt bir sekilde cagirsin #

        # print(mat_contents_t1_p1.shape,mat_contents_t1_p1[0].shape,mat_contents_t1_p1[0][0].shape)

        #create label vectors for the data

        # train_label_0_p1=np.zeros(mat_contents_t0_p1.shape)
        # train_label_1_p1=np.ones(mat_contents_t1_p1.shape)
        # train_label_0_p2=np.zeros(mat_contents_t0_p2.shape)
        # train_label_1_p2=np.ones(mat_contents_t1_p2.shape)
        # train_label_0_p3=np.zeros(mat_contents_t0_p3.shape)
        # train_label_1_p3=np.ones(mat_contents_t1_p3.shape)
        # train_label_0_p4=np.zeros(mat_contents_t0_p4.shape)
        # train_label_1_p4=np.ones(mat_contents_t1_p4.shape)
        # train_label_0_p5=np.zeros(mat_contents_t0_p5.shape)
        # train_label_1_p5=np.ones(mat_contents_t1_p5.shape)
        # train_label_0_p6=np.zeros(mat_contents_t0_p6.shape)
        # train_label_1_p6=np.ones(mat_contents_t1_p6.shape)
        # train_label_0_p7=np.zeros(mat_contents_t0_p7.shape)
        # train_label_1_p7=np.ones(mat_contents_t1_p7.shape)
        # train_label_0_p8=np.zeros(mat_contents_t0_p8.shape)
        # train_label_1_p8=np.ones(mat_contents_t1_p8.shape)
        # train_label_0_p9=np.zeros(mat_contents_t0_p9.shape)
        # train_label_1_p9=np.ones(mat_contents_t1_p9.shape)
        # train_label_0_p10=np.zeros(mat_contents_t0_p10.shape)
        # train_label_1_p10=np.ones(mat_contents_t1_p10.shape)

        # x_train_0_p1=np.zeros((mat_contents_t0_p1.shape[0],mat_contents_t0_p1[0][0].shape[1]))
        # x_train_1_p1=np.zeros((mat_contents_t1_p1.shape[0],mat_contents_t1_p1[0][0].shape[1]))
        # x_train_0_p2=np.zeros((mat_contents_t0_p2.shape[0],mat_contents_t0_p2[0][0].shape[1]))
        # x_train_1_p2=np.zeros((mat_contents_t1_p2.shape[0],mat_contents_t1_p2[0][0].shape[1]))
        # x_train_0_p3=np.zeros((mat_contents_t0_p3.shape[0],mat_contents_t0_p3[0][0].shape[1]))
        # x_train_1_p3=np.zeros((mat_contents_t1_p3.shape[0],mat_contents_t1_p3[0][0].shape[1]))
        # x_train_0_p4=np.zeros((mat_contents_t0_p4.shape[0],mat_contents_t0_p4[0][0].shape[1]))
        # x_train_1_p4=np.zeros((mat_contents_t1_p4.shape[0],mat_contents_t1_p4[0][0].shape[1]))
        # x_train_0_p5=np.zeros((mat_contents_t0_p5.shape[0],mat_contents_t0_p5[0][0].shape[1]))
        # x_train_1_p5=np.zeros((mat_contents_t1_p5.shape[0],mat_contents_t1_p5[0][0].shape[1]))
        # x_train_0_p6=np.zeros((mat_contents_t0_p6.shape[0],mat_contents_t0_p6[0][0].shape[1]))
        # x_train_1_p6=np.zeros((mat_contents_t1_p6.shape[0],mat_contents_t1_p6[0][0].shape[1]))
        # x_train_0_p7=np.zeros((mat_contents_t0_p7.shape[0],mat_contents_t0_p7[0][0].shape[1]))
        # x_train_1_p7=np.zeros((mat_contents_t1_p7.shape[0],mat_contents_t1_p7[0][0].shape[1]))
        # x_train_0_p8=np.zeros((mat_contents_t0_p8.shape[0],mat_contents_t0_p8[0][0].shape[1]))
        # x_train_1_p8=np.zeros((mat_contents_t1_p8.shape[0],mat_contents_t1_p8[0][0].shape[1]))
        # x_train_0_p9=np.zeros((mat_contents_t0_p9.shape[0],mat_contents_t0_p9[0][0].shape[1]))
        # x_train_1_p9=np.zeros((mat_contents_t1_p9.shape[0],mat_contents_t1_p9[0][0].shape[1]))
        # x_train_0_p10=np.zeros((mat_contents_t0_p10.shape[0],mat_contents_t0_p10[0][0].shape[1]))
        # x_train_1_p10=np.zeros((mat_contents_t1_p10.shape[0],mat_contents_t1_p10[0][0].shape[1]))

        # # print(x_train_0_p1.shape)

        # #Extract the x points from the cells to a big array
        # for idx in range(len(mat_contents_t0_p1)):
        #     x_train_0_p1[idx]=mat_contents_t0_p1[idx][0]
        # for idx in range(len(mat_contents_t0_p2)):
        #     x_train_0_p2[idx]=mat_contents_t0_p2[idx][0]
        # for idx in range(len(mat_contents_t0_p3)):
        #     x_train_0_p3[idx]=mat_contents_t0_p3[idx][0]
        # for idx in range(len(mat_contents_t0_p3)):
        #     x_train_0_p3[idx]=mat_contents_t0_p3[idx][0]
        #     # if idx<len(mat_contents_t1_p1):
        # for idx in range(len(mat_contents_t1_p1)):
        #         x_train_1_p1[idx]=mat_contents_t1_p1[idx][0]
        # for idx in range(len(mat_contents_t1_p2)):
        #         x_train_1_p2[idx]=mat_contents_t1_p2[idx][0]
        # for idx in range(len(mat_contents_t1_p3)):
        #         x_train_1_p3[idx]=mat_contents_t1_p3[idx][0]

        #Here we have the x positions of the 8000+6400 simulations at t=6.1s (type1 and
        # type2) extracted from the .mat file. Then the next step is to reduce the dimension
        #of this huge sized matrix by applying principle component analysis (PCA)

        # x_train_all_p1=np.append(x_train_0_p1,x_train_1_p1,axis=0)
        # x_train_all_p2=np.append(x_train_0_p2,x_train_1_p2,axis=0)
        # x_train_all_p3=np.append(x_train_0_p3,x_train_1_p3,axis=0)

        # label_train_all_p1=np.append(train_label_0_p1,train_label_1_p1,axis=0)
        # label_train_all_p2=np.append(train_label_0_p2,train_label_1_p2,axis=0)
        # label_train_all_p3=np.append(train_label_0_p3,train_label_1_p3,axis=0)

        # x_train_all_labeled_p1=np.append(x_train_all_p1,label_train_all_p1,axis=1)
        # x_train_all_labeled_p2=np.append(x_train_all_p2,label_train_all_p2,axis=1)
        # x_train_all_labeled_p3=np.append(x_train_all_p3,label_train_all_p3,axis=1)

        # #Shuffle the labeled data to get a "neutral" dataset#
        # np.random.shuffle(x_train_all_labeled_p1)
        # np.random.shuffle(x_train_all_labeled_p2)
        # np.random.shuffle(x_train_all_labeled_p3)
        # #Separate the labels and data part of the shuffled dataset#
        # x_label_shuffled_p1=x_train_all_labeled_p1[:,-1]
        # x_label_shuffled_p2=x_train_all_labeled_p2[:,-1]
        # x_label_shuffled_p3=x_train_all_labeled_p3[:,-1]

        # x_train_shuffled_p1=x_train_all_labeled_p1[:,0:-1]
        # x_train_shuffled_p2=x_train_all_labeled_p2[:,0:-1]
        # x_train_shuffled_p3=x_train_all_labeled_p3[:,0:-1]

        # x_label_shuffled_p1=x_label_shuffled_p1.astype(int)
        # x_label_shuffled_p2=x_label_shuffled_p2.astype(int)
        # x_label_shuffled_p3=x_label_shuffled_p3.astype(int)

        # x_train_shuffled_p1=x_train_shuffled_p1.astype(float)
        # x_train_shuffled_p2=x_train_shuffled_p2.astype(float)
        # x_train_shuffled_p3=x_train_shuffled_p3.astype(float)
        # # print('Dimension of the training data:',x_train_all_p1.shape)

        # #divide the set as training/test set#
        # x_train_p1 , x_test_p1 , x_label_train_p1 ,x_label_test_p1 = \
        #     train_test_split(x_train_shuffled_p1,x_label_shuffled_p1,test_size=0.40)
        # x_train_p2 , x_test_p2 , x_label_train_p2 ,x_label_test_p2 = \
        #     train_test_split(x_train_shuffled_p2,x_label_shuffled_p2,test_size=0.40)
        # x_train_p3 , x_test_p3 , x_label_train_p3 ,x_label_test_p3 = \
        #     train_test_split(x_train_shuffled_p3,x_label_shuffled_p3,test_size=0.40)

        # x_train=np.append(x_train_p1,x_train_p2,axis=0)
        # x_train=np.append(x_train,x_train_p3,axis=0)

        # x_label=np.append(x_label_train_p1,x_label_train_p2,axis=0)
        # x_label=np.append(x_label,x_label_train_p3,axis=0)

        # x_label=np.reshape(x_label,(x_train.shape[0],1))

        # print('xtrain shape:',x_train.shape)
        # print('xlabel shape:',x_label.shape)
        # x_train_all=np.append(x_train,x_label,axis=1)
        # np.random.shuffle(x_train_all)
        # x_train=x_train_all[:,0:-1]
        # x_label=x_train_all[:,-1]


        # a,b = x_train.shape
        # print('Size of the combined training set: ',a,b)

    # print(338,'buyuk commented loopun arkasi')
    #buraya pca loop'u gelecek bu sayede en dista train loopu sonra icerde pca loopu en icte de 
    # test loopu olacak sekilde butun simulasyon kosacak#
    for idx in range(len(n_pca)):
        if idx!=0:
            del data_train
            f=open('temp_training_data1.pckl','rb')
            data_train=pickle.load(f)
            f.close
            f=open('temp_training_data2.pckl','rb')
            data_train=np.append(data_train,pickle.load(f),axis=0)
            f.close
        try:
            network
        except NameError:
            pass
        else:
            del network
        print('Iteration with #PC:',n_pca[idx],' has started.')
        preprocess_time=time.time()
        if split:
            if preprocess=='pca':
                ipcax=IncrementalPCA(n_components=n_pca[idx],batch_size=300)
                ipcay=IncrementalPCA(n_components=n_pca[idx],batch_size=300)
                ipcax.fit(data_train[:,:split_len])
                ipcay.fit(data_train[:,split_len:])
                x_train=ipcax.transform(data_train[:,:split_len])
                y_train=ipcay.transform(data_train[:,split_len:])
            elif preprocess=='ae':
                epochs = 100
                batch_size=500
                ae_number=np.ceil(data_train.shape[0]/batch_size).astype(int)
                ae_x=reg.AE_Preprocess(n_pca[idx],split_len,steps).cuda()
                ae_y=reg.AE_Preprocess(n_pca[idx],split_len,steps).cuda()
                print(ae_x)
                ae_x_optimizer=torch.optim.Adam(ae_x.parameters(),lr=0.001)
                ae_y_optimizer=torch.optim.Adam(ae_y.parameters(),lr=0.001)
                ae_x.train()
                ae_y.train()
                ae_x_criterion = nn.MSELoss().cuda()
                ae_y_criterion = nn.MSELoss().cuda()
                for pre_i in range(epochs):
                    current_batch=0
                    batch_loss_x=[]
                    batch_loss_y=[]
                    while current_batch<ae_number:
                        ae_x_optimizer.zero_grad()
                        if current_batch<data_train.shape[0]//batch_size:
                            # ae_x_batch=data_train[current_batch*batch_size:(current_batch+1)*batch_size,:split_len]
                            # ae_x.forward(data_train[current_batch*batch_size:(current_batch+1)*batch_size,:split_len])
                            ae_x_loss=ae_x_criterion(ae_x.forward(data_train[current_batch*batch_size:(current_batch+1)*batch_size,:split_len]),\
                                torch.from_numpy(data_train[current_batch*batch_size:(current_batch+1)*batch_size,:split_len]).type(torch.cuda.FloatTensor))
                        else:
                            ae_x_loss=ae_x_criterion(ae_x.forward(data_train[current_batch*batch_size:,:split_len]),\
                                torch.from_numpy(data_train[current_batch*batch_size:,:split_len]).type(torch.cuda.FloatTensor))
                        batch_loss_x.append(ae_x_loss.item())
                        ae_x_loss.backward()
                        ae_x_optimizer.step()
                        current_batch+=1
                    current_batch=0
                    while current_batch<ae_number:
                        ae_y_optimizer.zero_grad()
                        if current_batch<data_train.shape[0]//batch_size:
                            # ae_x_batch=data_train[current_batch*batch_size:(current_batch+1)*batch_size,:split_len]
                            # ae_x.forward(data_train[current_batch*batch_size:(current_batch+1)*batch_size,:split_len])
                            ae_y_loss=ae_y_criterion(ae_y.forward(data_train[current_batch*batch_size:(current_batch+1)*batch_size,split_len:]),\
                                torch.from_numpy(data_train[current_batch*batch_size:(current_batch+1)*batch_size,split_len:]).type(torch.cuda.FloatTensor))
                        else:
                            ae_y_loss=ae_y_criterion(ae_y.forward(data_train[current_batch*batch_size:,split_len:]),\
                                torch.from_numpy(data_train[current_batch*batch_size:,split_len:]).type(torch.cuda.FloatTensor))
                        batch_loss_y.append(ae_y_loss.item())
                        ae_y_loss.backward()
                        ae_y_optimizer.step()
                        current_batch+=1
                    if (pre_i+1)%5==0: print("Epoch: ",pre_i+1,"/",epochs," Current AE Losses: ",np.mean(batch_loss_x),np.mean(batch_loss_y))
                    if pre_i+1>5 and np.mean(batch_loss_x)<1e-4 and np.mean(batch_loss_y)<1e-4:
                        print("Terminiated at Epoch:",pre_i+1,"/",epochs," Current AE Losses: ",np.mean(batch_loss_x),np.mean(batch_loss_y))
                        break
                ae_x.eval()
                ae_y.eval()
                div=100
                xlen=data_train.shape[0]//div
                x_train=ae_x.transform(data_train[:xlen,:split_len])
                y_train=ae_y.transform(data_train[:xlen,split_len:])
                for bat in range(1,div):
                    if bat<div:
                        x_train=np.append(x_train,ae_x.transform(data_train[bat*xlen:(bat+1)*xlen,:split_len]),axis=0)
                        y_train=np.append(y_train,ae_y.transform(data_train[bat*xlen:(bat+1)*xlen,split_len:]),axis=0)
                    else:
                        x_train=np.append(x_train,ae_x.transform(data_train[bat*xlen:,:split_len]),axis=0)
                        y_train=np.append(y_train,ae_y.transform(data_train[bat*xlen:,split_len:]),axis=0)
                save_name='ae_x.pckl'
                save=open(save_name,'wb')
                pickle.dump(ae_x,save)
                save.close
                save_name='ae_y.pckl'
                save=open(save_name,'wb')
                pickle.dump(ae_y,save)
                save.close
                del ae_x_criterion,ae_x_loss,ae_x_optimizer,ae_y_loss,ae_y_optimizer,ae_y_criterion,ae_x,ae_y
                torch.cuda.empty_cache()

                # x_train=ae_x.transform(data_train[:xlen,:split_len])
                # x_train=np.append(x_train,ae_x.transform(data_train[xlen:,:split_len]),axis=0)
                # y_train=ae_y.transform(data_train[:xlen,split_len:])
                # y_train=np.append(y_train,ae_y.transform(data_train[:xlen,split_len:]),axis=0)
            else:
                raise ValueError('Preprocessing algorithm not defined, please define the preprocessing correctly')
            # print("shapes after pca",x_train.shape,y_train.shape,data_train.shape)
            data_train=np.append(x_train,y_train,axis=1)
            # data_train=np.append(data_train,np.append(fb_scaled,fs_scaled,axis=1),axis=1)
            N_p=n_pca[idx]*2
            del y_train, x_train
            torch.cuda.empty_cache()
        else:
            if preprocess=='pca':
                ipca=IncrementalPCA(n_components=n_pca[idx],batch_size=500)
                ipca.fit(data_train)
                data_train=ipca.transform(data_train)
                # data_train=np.append(data_train,np.append(fb_scaled,fs_scaled,axis=1),axis=1)
            elif preprocess=='ae':
                epochs = 100
                batch_size=100
                ae_number=np.ceil(data_train.shape[0]/batch_size).astype(int)
                ae=reg.AE_Preprocess(n_pca[idx],split_len,steps).cuda()
                print(ae)
                ae_optimizer=torch.optim.Adam(ae_x.parameters(),lr=0.001)
                ae.train()
                ae_criterion = nn.MSELoss().cuda()
                for pre_i in range(epochs):
                    current_batch=0
                    batch_loss=[]
                    while current_batch<ae_number:
                        ae_x_optimizer.zero_grad()
                        if current_batch<data_train.shape[0]//batch_size:
                            # ae_x_batch=data_train[current_batch*batch_size:(current_batch+1)*batch_size,:split_len]
                            # ae_x.forward(data_train[current_batch*batch_size:(current_batch+1)*batch_size,:split_len])
                            ae_loss=ae_criterion(ae.forward(data_train[current_batch*batch_size:(current_batch+1)*batch_size,:]),\
                                torch.from_numpy(data_train[current_batch*batch_size:(current_batch+1)*batch_size,:]).type(torch.cuda.FloatTensor))
                        else:
                            ae_loss=ae_criterion(ae.forward(data_train[current_batch*batch_size:,:]),\
                                torch.from_numpy(data_train[current_batch*batch_size:,:]).type(torch.cuda.FloatTensor))
                        batch_loss.append(ae_loss.item())
                        ae_loss.backward()
                        ae_optimizer.step()
                        current_batch+=1
                    if (pre_i+1)%5==0: print("Epoch: ",pre_i+1,"/",epochs," Current AE preprocess Losses: ",np.mean(batch_loss))
                    if pre_i+1>5 and np.mean(batch_loss)<1e-3:
                        print("Terminiated at Epoch:",pre_i+1,"/",epochs," Current AE preprocess Losses: ",np.mean(batch_loss))
                        break
                ae.eval()
                save_name='ae.pckl'
                save=open(save_name,'wb')
                pickle.dump(ae,save)
                save.close
                data_train=ae.transform(data_train)
                del ae_criterion,ae_optimizer,ae
                torch.cuda.empty_cache()
            else:
                raise ValueError('Preprocessing algorithm not defined, please define the preprocessing correctly')
            N_p=n_pca[idx]
            
        # if preprocess=='ae':
        #     if split:
        #         ae_x.eval()
        #         ae_y.eval()
        #     else: ae.eval()
        print("Data preprocessing completed in ",np.round(time.time()-preprocess_time,decimals=2)," s")
        # pca=PCA(n_components = n_pca[idx])
        # print('pca tanimlandi')
        # pca.fit(x_train)
        # print('pca oturduldu')
        # x_train=pca.transform(x_train)
        # print('pca transform sonrasi')
        N_p_variance=1
        # N_p=n_pca[idx]#number of principal components used
        

        # data_train=data_train[:,0:N_p]
        # pca_score = ipca.explained_variance_ratio_
        # pca_score_sum=sum(pca_score[0:N_p])
        # pca_scores[train,idx]=pca_score_sum
        # print('datanin pcasi alindriktan sonra')
        scaler=StandardScaler()
        data_train=np.append(data_train,np.append(fb_scaled,fs_scaled,axis=1),axis=1)
        scaler.fit(data_train)
        data_train=scaler.transform(data_train)
        # print('data scale edildikten sonra')
        # for iii in range(len(train_sets)):
        #     testing_data="x_test_p"+str(iii+1)
        #     x_pca_name="x_test_pca_p"+str(iii+1)
        #     x_name_after_pca="x_test_after_pca_p"+str(iii+1)
        #     exec(x_pca_name+"=pca.transform("+testing_data+")")
        #     exec(x_name_after_pca+"="+x_pca_name+"[:,0:N_p]")
        #     exec(x_name_after_pca+"=scaler.transform("+x_name_after_pca+")")
        

        # scaler.fit(x_test_after_pca)


            
        # x_test_pca_p1 = pca.transform(x_test_p1)
        # x_test_pca_p2 = pca.transform(x_test_p2)
        # x_test_pca_p3 = pca.transform(x_test_p3)

        # x_test_after_pca_p1=x_test_pca_p1[:,0:N_p]
        # x_test_after_pca_p2=x_test_pca_p2[:,0:N_p]
        # x_test_after_pca_p3=x_test_pca_p3[:,0:N_p]

        # x_test_after_pca_p1=scaler.transform(x_test_after_pca_p1)
        # x_test_after_pca_p2=scaler.transform(x_test_after_pca_p2)
        # x_test_after_pca_p3=scaler.transform(x_test_after_pca_p3)

        #create the hidden layer architecture#
        u=1000
        v=1000
        w=1000
        y=500
        hidden_layer = [y,y,y,y,y,y,y,y,y,y,y,y]

        # print('\n\nTrain t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
        #     +'.'+test_sets[test][1]+'s results:')

        # print('\n [MLP Classifier Result]')
        # print('\n* The number of PCs :', N_p, \
        #         '\n* Total PCA score :',pca_score_sum,\
        #         '\n* Hidden Layer :', hidden_layer)
        # print('network tanimlaniyor')
        network = reg.Regresor(hidden_layer,data_train.shape[1],n_par)
        print(network)
        network.train()
        criterion = nn.MSELoss().cuda()
        optimizer = torch.optim.Adam(network.parameters(),lr=0.001)
        epochs = 100
        network=network.cuda()
        try:
            data_label_train=torch.from_numpy(data_label_train).type(torch.cuda.FloatTensor)
            data_label_train.requires_grad_()
        except TypeError:
            data_label_train=data_label_train.cuda()
            data_label_train.requires_grad_()

        # batch_size=len(x_label_train) #single batch
        batch_size=150 #26 batches
        batch_number=np.ceil(len(data_label_train)/batch_size).astype(int)
        print(batch_number," batches with ",batch_size," elements")
        losses=[]
        print('Neural network training started...')
        inter_time=time.time()
        for i in range(epochs):
            current_batch=0
            batch_loss=[]
            # print(i)
            while current_batch<batch_number:
                network.train()            
                optimizer.zero_grad()
                if current_batch<len(data_label_train)//batch_size:
                    # print(current_batch*batch_size,(current_batch+1)*batch_size)
                    # x_train_batch=x_train_after_pca[current_batch*batch_size:(current_batch+1)*batch_size,:]
                    data_label_batch=data_label_train[current_batch*batch_size:(current_batch+1)*batch_size]
                    # ypred=network.forward_ReLU(x_train_after_pca[current_batch*batch_size:(current_batch+1)*batch_size,:],len(hidden_layer)+2)
                    loss=criterion(network.forward_ReLU(data_train[current_batch*batch_size:\
                        (current_batch+1)*batch_size,:],len(hidden_layer)+2),\
                            data_label_batch.detach())
                    #hala memory sorunu yasarsan buraya bak, ypred ve x_label_batch'i de
                    # elimine edebilirsin!!!!!!!
                else:
                    # print(current_batch*batch_size)
                    # x_train_batch=x_train_after_pca[current_batch*batch_size:,:]
                    data_label_batch=data_label_train[current_batch*batch_size:]
                    # ypred=network.forward_ReLU(x_train_after_pca[current_batch*batch_size:,:],len(hidden_layer)+2)
                    loss=criterion(network.forward_ReLU(data_train[current_batch*batch_size:,:]\
                        ,len(hidden_layer)+2),data_label_batch.detach())

                # ypred=network.forward_ReLU(x_train_batch,len(hidden_layer)+2)
                # x_label_train=torch.from_numpy(x_label_train).type(torch.cuda.LongTensor)
                # print("ypred\n",ypred)
                # print("xlabeltrain\n",x_label_train.shape)
                # loss=criterion(ypred,x_label_batch.unsqueeze(1).detach())
                loss.backward()
                optimizer.step()
                batch_loss.append(loss.item())
                current_batch+=1
            losses.append(np.mean(batch_loss))
            if (i+1)%10==0: print("Epoch: ",i+1,"/",epochs," Current Training Loss: ",losses[i])
            if i+1>5 and abs(losses[-1])<1e-3:
                print("Terminiated at Epoch:",i+1,"/",epochs," Current Training Loss: ",losses[i])
                break
        del criterion,optimizer
        torch.cuda.empty_cache()
        # mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='tanh',\
        #                     max_iter = 2000)
        # mlp.fit(x_train_after_pca,x_label)
 
        train_time[train,idx]=time.time()-inter_time
        network.eval()
        # print("predict oncesi")
        label_predict=np.zeros((data_label_train.shape[0],n_par))
        print(label_predict.shape,data_label_train.shape)

        gpu_batch=100
        current_batch=0
        gpu_epoch=np.ceil(label_predict.shape[0]/gpu_batch)
        while current_batch<gpu_epoch:
            # if current_batch%10 or current_batch==gpu_epoch-1:
            #     print("gpu epoch: ",current_batch,"/",gpu_epoch)
            if current_batch<label_predict.shape[0]//gpu_batch:
                batch_predict=network.predict(data_train[current_batch*gpu_batch:(current_batch+1)*gpu_batch,:],len(hidden_layer)+2)
                label_predict[current_batch*gpu_batch:(current_batch+1)*gpu_batch,:]=batch_predict.cpu().numpy()
            else:
                batch_predict=network.predict(data_train[current_batch*gpu_batch:,:],len(hidden_layer)+2)
                label_predict[current_batch*gpu_batch:,:]=batch_predict.cpu().numpy()
            current_batch+=1
        # for iiii in range(n_par):
        #     coefname="coef"+str(iiii)
        #     # exec(coefname+"=max(params[:,"+str(idx)+"])")
        #     exec("label_predict[:,"+str(iiii)+"]=label_predict[:,"+str(iiii)+"]*"+coefname)

        # label_predict=network.predict(data_train,len(hidden_layer)+2)
        # print("predict sonrasi")
        # # print(label_predict.shape)
        # label_predict=label_predict.cpu()
        # label_predict=label_predict.numpy()
        data_label_train=data_label_train.cpu()
        data_label_train=data_label_train.detach().numpy()
        # # print(label_predict)
        print("New pytorch network mserror              : ",mean_squared_error(paramscaler.inverse_transform(data_label_train),paramscaler.inverse_transform(label_predict)))
        print("New pytorch network rsquared value       : ",r2_score(paramscaler.inverse_transform(data_label_train),paramscaler.inverse_transform(label_predict)))
        print("New pytorch network mean percentage error: ",np.mean(abs(paramscaler.inverse_transform(data_label_train)-paramscaler.inverse_transform(label_predict))/\
            paramscaler.inverse_transform(data_label_train),axis=0)*100)
        # print("New pytorch network mserror: ",mean_squared_error(data_label_train,label_predict))
        # print("New pytorch network rsquared value: ",r2_score(data_label_train,label_predict))
        # print("New pytorch network training accuracy: ",accuracy_score(label_predict,x_label_train)) 

        train_time[train,idx]=time.time()-inter_time
        print('Neural network trained in ',train_time[train,idx],'s.')
        training_loss.append(losses)
        network.eval()
        # print("predict oncesi")
        # label_predict=network.predict(x_train_after_pca,len(hidden_layer)+2)

        network_name=''
        for iii in range(len(hidden_layer)):
            network_name=network_name+str(hidden_layer[iii])+'_'
        network_name=layer_info=network_name[:-1]
        network_name='MLPClassifier_pytorch_pcasearch_fbfs_10instance_'+splitstatement+'_train'+train_sets[train]+'s_pc'+str(n_pca[idx])+'_'+\
            str(len(hidden_layer))+'layers_'+network_name+'.pth'
        if split:
            torch.save({
                # 'network_state_dict':network.state_dict(),
                # 'optimizer_state_dict':optimizer.state_dict(),
                # 'network':network,
                # 'optimizer':optimizer,
                'epoch':epochs,
                'paramscaler':paramscaler,
                'batch_size':batch_size,
                # 'pcax':ipcax,
                # 'pcay':ipcay,
                'scaler':scaler,
                'hidden_layer':hidden_layer,
                'N_p':N_p,
                'split':split,
                'instances':instances
            },network_name)
        else:
            torch.save({
                'network_state_dict':network.state_dict(),
                'optimizer_state_dict':optimizer.state_dict(),
                'network':network,
                'optimizer':optimizer,
                'epoch':epochs,
                'paramscaler':paramscaler,
                'batch_size':batch_size,
                # 'pca':ipca,
                'scaler':scaler,
                'hidden_layer':hidden_layer,
                'N_p':N_p,
                'split':split,
                'instances':instances
            },network_name)
        # network_name=''
        # for iii in range(len(hidden_layer)):
        #     network_name=network_name+str(hidden_layer[iii])+'_'
        # network_name=layer_info=network_name[:-1]
        # network_name='MLPClassifier_pytorch_10instance_train'+train_sets[train]+'s_pc'+str(n_pca[idx])+'_'+\
        #     str(len(hidden_layer))+'layers_'+network_name+'.sav'
        # joblib.dump(network,network_name)

        # # the count of true negatives is C_0,0, false negatives is C_1,0, 
        # # true positives is C_1,1 and false positives is C_0,1.
        # print('\n\nResults of the trained neural network:')
        # print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
        # print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
        # print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))        

        test_params=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Parameters_type'+str(typeind))['CA_Gelsdata']
        test_params=test_params[:,:n_par].reshape(-1,n_par)

        trig=0
        for test in range(len(test_sets)):
            # print("train=",train)
            # print("trig=",trig)
            # print("test=",test)
        # if test!=train: #bu kaldirilacak
            #call secondary test set#
            # #For Windows
            # secondary_mat_contents_0 =\
            #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type1')\
            #         ['CA_Gelsdata']
            # secondary_mat_contents_1 =\
            #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type2')\
            #         ['CA_Gelsdata']
            #For Ubuntu
            # print("trig=",trig)

            secondary_mat_contents_fbfs=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_FbFs_type'+str(typeind))['CA_Gelsdata']

            teinst=np.array([int(item+'0') for item in test_sets[test]])
            # fb=fbfsbulk[0,0]
            # fs=fbfsbulk[0,1]
            fbtest=secondary_mat_contents_fbfs[0,0][:].reshape(1,-1)
            fstest=secondary_mat_contents_fbfs[0,1][:].reshape(1,-1)
            for iidx in range(1,secondary_mat_contents_fbfs.shape[0]):
                fbtest=np.append(fbtest,secondary_mat_contents_fbfs[iidx,0][:].reshape(1,-1),axis=0)
                fstest=np.append(fstest,secondary_mat_contents_fbfs[iidx,1][:].reshape(1,-1),axis=0)
            del secondary_mat_contents_fbfs
            print(fbtest.shape,fstest.shape)

            for iii in range(len(test_sets[test])):
                # # print("test loop#:",iii)
                # matnamex="secondary_mat_contents_x"
                # matnamey="secondary_mat_contents_y"
                # # matnamez="secondary_mat_contents_z"
                
                # exec(matnamex+"=sio.loadmat\
                # ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_z"+test_sets[test][iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                # exec(matnamey+"=sio.loadmat\
                # ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_y"+test_sets[test][iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                if force_instance==1:
                    fb_test=fbtest[:,teinst[iii]-1].reshape(-1,1)
                    fs_test=fstest[:,teinst[iii]-1].reshape(-1,1)
                else:
                    fs_test=fstest[:,teinst[iii]-force_instance:teinst[iii]].reshape(-1,force_instance)
                    fb_test=fbtest[:,teinst[iii]-force_instance:teinst[iii]].reshape(-1,force_instance)
                print(fb_test.shape,fs_test.shape)

                fb_test_scaled=fbscaler.transform(fb_test)
                fs_test_scaled=fsscaler.transform(fs_test)
                del fb_test,fs_test
                f=open('ae_fb.pckl','rb')
                AE_fb=pickle.load(f)
                f.close
                AE_fb.eval()
                fb_test=AE_fb.transform(fb_test_scaled)
                del AE_fb
                torch.cuda.empty_cache()

                f=open('ae_fs.pckl','rb')
                AE_fs=pickle.load(f)
                f.close
                AE_fs.eval()
                fs_test=AE_fs.transform(fs_test_scaled)
                del AE_fs
                torch.cuda.empty_cache()

                fbfs_test=np.append(fb_test,fs_test,axis=1)
                del fb_test,fs_test
                
                secondary_mat_contents_x=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_z'+test_sets[test][iii]+'_type'+str(typeind))['CA_Gelsdata']
                secondary_mat_contents_y=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_y'+test_sets[test][iii]+'_type'+str(typeind))['CA_Gelsdata']
                
                # exec(matnamez+"=sio.loadmat\
                # ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_z"+test_sets[test][iii]+"_type1')['CA_Gelsdata']")
                
                # exec(matname0+"=sio.loadmat\
                # ('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/"+test_dms[test]+"_DM/CA_"+test_dms[test]+"DM_Pts_x"+test_sets[test][iii]+"_type1')['CA_Gelsdata']")
                # exec(matname1+"=sio.loadmat\
                # ('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/"+test_dms[test]+"_DM/CA_"+test_dms[test]+"DM_Pts_x"+test_sets[test][iii]+"_type2')['CA_Gelsdata']")
                # exec(matnamex+"="+matnamex+"[:6400]")
                # exec(matnamey+"="+matnamey+"[:6400]")

                test_x=np.zeros((secondary_mat_contents_x.shape[0],secondary_mat_contents_x[0][0].shape[1]))
                test_y=np.zeros((secondary_mat_contents_y.shape[0],secondary_mat_contents_y[0][0].shape[1]))
                # test_z=np.zeros((secondary_mat_contents_z.shape[0],secondary_mat_contents_z[0][0].shape[1]))
                for ii in range(len(secondary_mat_contents_x)):test_x[ii]=secondary_mat_contents_x[ii][0]
                del secondary_mat_contents_x
                for ii in range(len(secondary_mat_contents_y)):test_y[ii]=secondary_mat_contents_y[ii][0]
                del secondary_mat_contents_y
                # for ii in range(len(secondary_mat_contents_z)):test_z[ii]=secondary_mat_contents_z[ii][0]
                # del secondary_mat_contents_z

                data_test=np.append(test_x,test_y,axis=1)
                # data_test=np.append(data_test,test_z,axis=1)

                # label_test_all=np.append(test_label_0,test_label_1,axis=0)
                test_all_labeled=np.append(data_test,test_params,axis=1)
                del test_x,test_y,data_test
                # del test_z

                np.random.shuffle(test_all_labeled)
                label_test_shuffled=test_all_labeled[:,-n_par:]
                data_test_shuffled=test_all_labeled[:,:-n_par]
                del test_all_labeled

                secondary_test_label=label_test_shuffled.astype(float)
                del label_test_shuffled

                secondary_test_data=data_test_shuffled.astype(float)
                del data_test_shuffled
                if split:
                    if preprocess=='pca':
                        secondary_test_data_after_pca=ipcax.transform(secondary_test_data[:,:split_len])
                        secondary_test_data_after_pcay=ipcay.transform(secondary_test_data[:,split_len:])
                    else:
                        f=open('ae_x.pckl','rb')
                        ae_x=pickle.load(f)
                        f.close
                        ae_x.eval()
                        secondary_test_data_after_pca=ae_x.transform(secondary_test_data[:,:split_len])
                        del ae_x
                        torch.cuda.empty_cache()
                        f=open('ae_y.pckl','rb')
                        ae_y=pickle.load(f)
                        f.close
                        ae_y.eval()
                        secondary_test_data_after_pcay=ae_y.transform(secondary_test_data[:,split_len:])
                        del ae_y
                        torch.cuda.empty_cache()
                    secondary_test_data=np.append(secondary_test_data_after_pca,secondary_test_data_after_pcay,axis=1)
                    del secondary_test_data_after_pca,secondary_test_data_after_pcay
                    # secondary_test_data=np.append(secondary_test_data,fbfs_test,axis=1)
                else:
                    if preprocess=='pca':
                        secondary_test_data = ipca.transform(secondary_test_data)
                    else:
                        f=open('ae.pckl','rb')
                        ae=pickle.load(f)
                        f.close
                        ae.eval()
                        secondary_test_data = ae.transform(secondary_test_data)
                        del ae
                        torch.cuda.empty_cache()
                    # secondary_test_data=np.append(secondary_test_data,fbfs_test,axis=1)
                # secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]
                secondary_test_data=np.append(secondary_test_data,fbfs_test,axis=1)
                secondary_test_data=scaler.transform(secondary_test_data)
                secondary_label_predicted=network.predict(secondary_test_data,len(hidden_layer)+2)
                del secondary_test_data
                torch.cuda.empty_cache()
                secondary_label_predicted=secondary_label_predicted.cpu()
                secondary_label_predicted=secondary_label_predicted.numpy()
                # secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
                #recording order: train set#, test set#, #pca, instance # within the test set
                secondary_label_predicted=paramscaler.inverse_transform(secondary_label_predicted)

                # for iiii in range(n_par):
                #     coefname="coef"+str(iiii)
                #     # exec(coefname+"=max(params[:,"+str(idx)+"])")
                #     exec("secondary_label_predicted[:,"+str(iiii)+"]=secondary_label_predicted[:,"+str(iiii)+"]*"+coefname)
                # secondary_label_predicted[:,0]=secondary_label_predicted[:,0]*coef0
                rsq_score[train,test,idx,iii]=r2_score(secondary_test_label,secondary_label_predicted)
                ms_error[train,test,idx,iii]=mean_squared_error(secondary_test_label,secondary_label_predicted)
                mean_percent_error[train,test,idx,iii,:]=np.mean(abs(secondary_test_label-secondary_label_predicted)/secondary_test_label*100,axis=0)
                relative_mse[train,test,idx,iii,:]= np.mean((secondary_test_label-secondary_label_predicted)**2,axis=0)/np.mean(secondary_test_label**2,axis=0)
                print('Train t='+train_sets[0][:-1]+'.'+train_sets[0][-1]+'-'\
                    +train_sets[-1][:-1]+'.'+train_sets[-1][-1]+'s, Test t='+test_sets[test][iii][0]\
                        +'.'+test_sets[test][iii][1]+'s completed.')
                print('* Accuracy of the neural network: ',rsq_score[train,test,idx,iii])
                print('* Mean percentage error: ',np.round(mean_percent_error[train,test,idx,iii,:],decimals=2),'%')
                print('* Relative MSE: ',np.round(relative_mse[train,test,idx,iii,:],decimals=4))
                # plt.figure()
                # plt.plot(np.linspace(0,0.25,10),np.linspace(0,0.25,10))
                # plt.scatter(secondary_test_label,secondary_label_predicted)
                # plt.xlabel('actual label')
                # plt.ylabel('predicted label')
                # plt.show()
                del secondary_label_predicted
                # for "kisaltma icin" in range(31):
                    # all_arrname="x_train_all_p"+str(iii+1)
                    # all_labelname="label_train_all_p"+str(iii+1)
                    # all_labeledname="x_train_all_labeled_p"+str(iii+1)
                    # exec(all_arrname+"=np.append("+arrname0+","+arrname1+",axis=0)")
                    # exec(all_labelname+"=np.append("+labelname0+","+labelname1+",axis=0)")
                    # exec(all_labeledname+"=np.append("+all_arrname+","+all_labelname+",axis=1")
                    # exec("np.random.shuffle("+all_labeledname+")")
                    # shuffled_labelname="x_label_shuffled_p"+str(iii+1)
                    # shuffled_dataname="x_train_shuffled_p"+str(iii+1)
                    # exec(shuffled_labelname+"="+all_labeledname+"[:,-1]")
                    # exec(shuffled_dataname+"="+all_labeledname+"[:,0:-1]")
                    # exec(shuffled_labelname+"="+shuffled_labelname+".astype(int)")
                    # exec(shuffled_dataname+"="+shuffled_dataname+".astype(float)")

                    # training_data="x_train_p"+str(iii+1)
                    # testing_data="x_test_p"+str(iii+1)
                    # training_label="x_label_train_p"+str(iii+1)
                    # testing_label="x_label_test_p"+str(iii+1)
                    # exec(training_data+","+testing_data+","+training_label+","+testing_label+"train_test_split("+shuffled_dataname+","+shuffled_labelname+",test_size=0.50)")

                    # secondary_mat_contents_0 =\
                    #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type1')\
                    #         ['CA_Gelsdata']
                    # secondary_mat_contents_1 =\
                    #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type2')\
                    #         ['CA_Gelsdata']


                    # #Extract the data and create labels etc.#
                    # secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
                    # secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
                    # secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
                    # secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
                    # #Extract the x points from the cells to a big array
                    # for ii in range(len(secondary_mat_contents_0)):secondary_test_data_0[ii]=secondary_mat_contents_0[ii][0]
                    #     # if ii<len(secondary_mat_contents_1):
                    # for ii in range(len(secondary_mat_contents_1)):secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]
                    # secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
                    # secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
                    # secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

                    # #Shuffle the labeled data to get a "neutral" dataset#
                    # np.random.shuffle(secondary_test_shuffled)
                    # #Separate the labels and data part of the shuffled dataset#
                    # secondary_test_label=secondary_test_shuffled[:,-1]
                    # secondary_test_data=secondary_test_shuffled[:,0:-1]
                    # secondary_test_label=secondary_test_label.astype(int)
                    # secondary_test_data=secondary_test_data.astype(float)
            
                    # secondary_test_data_after_pca = pca.transform(secondary_test_data)
                    # secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]

                    # secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
                    # secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
                    # accuracy_pca[train,idx,test]=accuracy_score(secondary_test_label,secondary_label_predicted)
            
        
            trig=trig+1
        
            # elif test==train:
            #     for iii in range(len(train_sets)):
            #         exec("x_label_predicted=mlp.predict(x_test_after_pca_p"+str(iii+1)+")")
            #         exec("accuracy_pca[train,test,idx,"+str(iii)+"]=accuracy_score(x_label_test_p"+str(iii+1)+",x_label_predicted)")
            #         print('Iteration for Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
            #             +train_sets[-1][0]+'.'+train_sets[-1][1]+'s, Test t='+train_sets[iii][0]\
            #                 +'.'+train_sets[iii][1]+'s is complete.')
            #         print('* Accuracy: ',accuracy_pca[train,test,idx,iii])                    
                # x_label_predicted=mlp.predict(x_test_after_pca_p1)
                # accuracy_pca[train,test,idx,0]=accuracy_score(x_label_test_p1,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p2)
                # accuracy_pca[train,test,idx,1]=accuracy_score(x_label_test_p2,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p3)
                # accuracy_pca[train,test,idx,2]=accuracy_score(x_label_test_p3,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p4)
                # accuracy_pca[train,test,idx,3]=accuracy_score(x_label_test_p4,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p5)
                # accuracy_pca[train,test,idx,4]=accuracy_score(x_label_test_p5,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p6)
                # accuracy_pca[train,test,idx,5]=accuracy_score(x_label_test_p6,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p7)
                # accuracy_pca[train,test,idx,6]=accuracy_score(x_label_test_p7,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p8)
                # accuracy_pca[train,test,idx,7]=accuracy_score(x_label_test_p8,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p9)
                # accuracy_pca[train,test,idx,8]=accuracy_score(x_label_test_p9,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p10)
                # accuracy_pca[train,test,idx,9]=accuracy_score(x_label_test_p10,x_label_predicted)
    # remove("temp_training_data.pckl")
    torch.cuda.empty_cache()

#end of train for loop

print('Execution of the whole code completed. Total runtime: ',time.time()-start_time,'s')
save_name='PCA_pytorch_pcasearch_10instance_'+splitstatement+'_'+str(len(hidden_layer))+'layers_'+\
    layer_info+'.pckl'
save=open(save_name,'wb')
train_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70']]
test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90']]
# test_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70'],['141','142','143','144','145','146','147','148','149','150']]
pickle.dump([train_sets,test_sets,n_pca,hidden_layer,n_par,train_time,ms_error,rsq_score,mean_percent_error,relative_mse,training_loss,instances],save)
save.close



line_colors=['r-o','b-o','g-o','c-o','m-o','k-o']
fig,axs=plt.subplots(len(dms),len(test_dms),sharex=True,sharey=True)
fig.suptitle('Mean Square Error for Different Number of PCs')

for train in range(len(dms)):
    for test in range(len(test_dms)):
        for pcs in range(len(n_pca)):
            axs[test].plot(ms_error[train,test,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))
        axs[test].set_xticklabels([])
# axs[0][0].set_ylabel('7-8s Str')
axs[0].set_ylabel('6-7s Str')
# axs[2][2].set_xlabel('5th DM')
axs[0].set_xlabel('7-8s Rand')
axs[1].set_xlabel('8-9s Rand')
# axs[2].set_xlabel('14-15s Rand')
# axs[2][0].set_ylabel('5th DM')
handles,labels=axs[1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')

fig,axs=plt.subplots(len(dms),len(test_dms),sharex=True,sharey=True)
fig.suptitle('$R^2$ Score for Different Number of PCs')
for train in range(len(dms)):
    for test in range(len(test_dms)):
        for pcs in range(len(n_pca)):
            axs[test].plot(rsq_score[train,test,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))
        axs[test].set_xticklabels([])
# axs[0][0].set_ylabel('7-8s Str')
axs[0].set_ylabel('6-7s Str')
# axs[2][2].set_xlabel('5th DM')
axs[0].set_xlabel('7-8s Rand')
axs[1].set_xlabel('8-9s Rand')
# axs[2].set_xlabel('14-15s Rand')
# axs[2][0].set_ylabel('5th DM')
handles,labels=axs[1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')

# plt.xticks([])

for idx in range(n_par):
    fig,axs=plt.subplots(len(dms),len(test_dms),sharex=True,sharey=True)
    fig.suptitle('Mean Percentage error for different number of PCs, Parameter '+str(idx+1))
    for train in range(len(dms)):
        for test in range(len(test_dms)):
            for pcs in range(len(n_pca)):
                axs[test].plot(mean_percent_error[train,test,pcs,:,idx],line_colors[pcs],label='PC='+str(n_pca[pcs]))
            axs[test].set_xticklabels([])
    # axs[0][0].set_ylabel('7-8s Str')
    axs[0].set_ylabel('6-7s Str')
    # axs[2][2].set_xlabel('5th DM')
    axs[0].set_xlabel('7-8s Rand')
    axs[1].set_xlabel('8-9s Rand')
    # axs[2].set_xlabel('14-15s Rand')
    # axs[2][0].set_ylabel('5th DM')
    handles,labels=axs[1].get_legend_handles_labels()
    fig.legend(handles,labels,loc='lower right')

# plt.xticks([])
plt.show()
                    

'''

#second test set #
secondary_mat_contents_0 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type1')['CA_Gelsdata']
secondary_mat_contents_1 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type2')['CA_Gelsdata']


#create label vectors for the data
secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
print(secondary_test_data_0.shape)

#Extract the x points from the cells to a big array
for idx in range(len(secondary_mat_contents_0)):
    secondary_test_data_0[idx]=secondary_mat_contents_0[idx][0]
    if idx<len(secondary_mat_contents_1):
# for idx in range(len(mat_contents_t1_p1)):
        secondary_test_data_1[idx]=secondary_mat_contents_1[idx][0]

secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

#Shuffle the labeled data to get a "neutral" dataset#
np.random.shuffle(secondary_test_shuffled)
#Separate the labels and data part of the shuffled dataset#
secondary_test_label=secondary_test_shuffled[:,-1]
secondary_test_data=secondary_test_shuffled[:,0:-1]
secondary_test_label=secondary_test_label.astype(int)
secondary_test_data=secondary_test_data.astype(float)



N_p_variance=1
N_p=30 #number of principal components used

#create the hidden layer architecture#
u=30
# v=30
# w=30

hidden_layer = [u]
x_train_after_pca=x_train_pca[:,0:N_p]
x_test_after_pca=x_test_pca[:,0:N_p]
pca_score_sum=sum(pca_score[0:N_p])

scaler=StandardScaler()
scaler.fit(x_train_after_pca)
# scaler.fit(x_test_after_pca)
x_train_after_pca=scaler.transform(x_train_after_pca)
x_test_after_pca=scaler.transform(x_test_after_pca)

print('\n\nTrain t=6.1s, Test t=6.1s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                    max_iter = 1000)
mlp.fit(x_train_after_pca,x_label_train)
x_label_predicted=mlp.predict(x_test_after_pca)

# the count of true negatives is C_0,0, false negatives is C_1,0, 
# true positives is C_1,1 and false positives is C_0,1.
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))

x_train_shuffled_62_pca=pca.transform(secondary_test_data)
pca_score = pca.explained_variance_ratio_
pca_score_sum=sum(pca_score[0:N_p])
x_train_shuffled_62_pca=scaler.transform(x_train_shuffled_62_pca)
print('\n\nTrain t=6.1s, Test t=6.2s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

x_label_shuffled_pred_62=mlp.predict(x_train_shuffled_62_pca)
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(secondary_test_label,x_label_shuffled_pred_62))                
print('* Classification report\n',classification_report(secondary_test_label,x_label_shuffled_pred_62))
print('* Accuracy of the neural network: ',accuracy_score(secondary_test_label,x_label_shuffled_pred_62))
# print(precision_recall)

'''