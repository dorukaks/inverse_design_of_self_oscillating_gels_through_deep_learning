'''
In order to keep the main code concise, the regressor module is moved to an external file.
'''
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

class Regresor(nn.Module):
    def __init__(self,n_units,n_pcs,n_par,d_o=False):
        self.dropout=d_o
        self.device = torch.device("cuda:0")
        n_layers = len(n_units)+2
        #The number of layers should be written including the input and output
        # layers. E.g: for a network with 1 hidden layer, n_layer should be 3#
        super(Regresor,self).__init__()
        n_units=np.array(n_units)
        if n_layers-2==1:
            self.fc1=nn.Linear(n_pcs,n_units[0])
            if self.dropout:
                self.drop1=nn.Dropout(p=0.4)
            # self.activ1=nn,ReLU()
            self.fc2=nn.Linear(n_units[0],n_par)
            # self.relu=nn.ReLU()
            # self.out=nn.Softmax(dim=1)
        else:
            self.fc1=nn.Linear(n_pcs,n_units[0])
            if self.dropout:
                self.drop1=nn.Dropout(p=0.8)
            # self.activ1=nn.ReLU()
            ctr=1
            name="self.fc"
            # activname="self.activ"
            while ctr+1<n_layers-1:
                exec(name+str(ctr+1)+"=nn.Linear("+str(n_units[ctr-1])+","+\
                    str(n_units[ctr])+")")
                if d_o:
                    exec("self.drop"+str(ctr+1)+"=nn.Dropout(p=0.8)")
                # exec(activname+str(ctr+1)+"=nn.ReLU()")
                ctr+=1
            exec(name+str(n_layers-1)+"=nn.Linear("+str(n_units[-1])+","+str(n_par)+")")
        # self.dropout=nn.Dropout(0.2)
            # self.out=nn.Softmax(dim=1)
            # self.relu=nn.ReLU()
    def forward_ReLU(self,x,n_layers):
        x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        x = self.fc1(x)
        if self.dropout:
            x=self.drop1(x)
        ctr = 2
        while ctr<n_layers-1:
            name="self.fc"+str(ctr)
            # activname="self.activ"+str(ctr)
            # exec("x="+activname+"(x)")
            # exec("x="+name+"(x)")
            x=F.relu(x)
            x=eval(name+'(x)')
            if self.dropout:
                x=eval("self.drop"+str(ctr)+"(x)")
            ctr+=1
        name="self.fc"+str(ctr)
        x=F.relu(x)
        x=eval(name+'(x)')
        # x=F.relu(x)
        # x=torch.sigmoid(x)
        # ans=[]
        # for t in x:
        #     if t[0]>t[1]:
        #         ans.append(0)
        #     else:
        #         ans.append(1)
        # return torch.tensor(ans).type(torch.cuda.IntTensor)
        return x
    
    def predict(self,x,n_layers):
        # x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        pred= self.forward_ReLU(x,n_layers)
        pred=pred.cpu()
        # pred=np.round(pred.detach())
        # ans=[]
        # for t in pred:
        #     if t[0]>t[1]:
        #         ans.append(0)
        #     else:
        #         ans.append(1)
        # return torch.tensor(ans).type(torch.cuda.FloatTensor)
        return pred.detach()

class AutoEncoder(nn.Module):
    def __init__(self,inptdim):
        super(AutoEncoder,self).__init__()
        self.device = torch.device("cuda:0")
        self.enc1 = nn.Linear(in_features=inptdim,out_features=9)
        self.enc2 = nn.Linear(in_features=9,out_features=8)
        self.enc3 = nn.Linear(in_features=8,out_features=7)
        self.dec1 = nn.Linear(in_features=7,out_features=8)
        self.dec2 = nn.Linear(in_features=8,out_features=9)
        self.dec3 = nn.Linear(in_features=9,out_features=inptdim)
    def forward(self,x):
        x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        x=F.relu(self.enc1(x))
        x=F.relu(self.enc2(x))
        x=F.relu(self.enc3(x))
        x=F.relu(self.dec1(x))
        x=F.relu(self.dec2(x))
        x=self.dec3(x)
        return x
    def transform(self,x):
        x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        x=F.relu(self.enc1(x))
        x=F.relu(self.enc2(x))
        x=F.relu(self.enc3(x))
        x=x.cpu()
        return x.detach()

class AE_Preprocess(nn.Module):
    def __init__(self,outdim,inpdim,steps):
        self.device = torch.device("cuda:0")
        self.stepping = np.flip(np.linspace(outdim,inpdim,num=steps,dtype=int)) 
        if self.stepping[0]!=inpdim:
            self.stepping = np.append(inpdim,self.stepping)
        super(AE_Preprocess,self).__init__()
        self.enc1 = nn.Linear(in_features=self.stepping[0],out_features=self.stepping[1])
        layno=2
        lastneurons=self.stepping[1]
        for neurons in self.stepping[2:]:
            name="self.enc"+str(layno)+"=nn.Linear(in_features="+str(lastneurons)+",out_features="+str(neurons)+")"
            exec(name)
            lastneurons=neurons
            layno+=1
        self.stepping = np.flip(self.stepping)
        self.dec1=nn.Linear(in_features=self.stepping[0],out_features=self.stepping[1])
        layno=2
        lastneurons=self.stepping[1]
        for neurons in self.stepping[2:]:
            name="self.dec"+str(layno)+"=nn.Linear(in_features="+str(lastneurons)+",out_features="+str(neurons)+")"
            exec(name)
            lastneurons=neurons
            layno+=1
        self.length=len(self.stepping)
    def forward(self,x):
        x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        for i in range(self.length-1):
            name="self.enc"+str(i+1)
            x=eval('F.relu('+name+'(x))')
        for i in range(self.length-2):
            name="self.dec"+str(i+1)
            x=eval("F.relu("+name+"(x))")
        x=eval("self.dec"+str(self.length-1)+"(x)")
        return x
    def transform(self,x):
        x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        for i in range(self.length-1):
            name="self.enc"+str(i+1)
            x=eval("F.relu("+name+"(x))")
        x=x.cpu()
        return x.detach()

class AE_Regresor(nn.Module):
    def __init__(self,n_units,n_pcs,n_par,split:bool):
        self.device = torch.device("cuda:0")
        n_layers = len(n_units)+2
        # The number of layers should be written including the input and output
        # layers. E.g: for a network with 1 hidden layer, n_layer should be 3#
        super(AE_Regresor,self).__init__()
        n_ae=2
        if split:
            n_pca=(n_pcs-n_ae)//2
        else:
            n_pca=n_pcs-n_ae
        n_units=np.array(n_units)
        if n_layers-2==1:
            if split:
                self.sc1=nn.Linear(in_features=n_pca,out_features=10*n_pca)
                self.sc2=nn.Linear(in_features=n_pca,out_features=10*n_pca)
                self.sc3=nn.Linear(in_features=n_ae,out_features=10*n_ae)
            else:
                self.sc1=nn.Linear(in_features=npca,out_features=10*n_pca)
                self.sc2=nn.Linear(in_features=n_ae,out_features=10*n_ae)
            # self.fc1=nn.Linear(10*n_pcs,n_units[0])
            self.fc1=nn.Linear(n_units[0],n_par)
            # self.activ1=nn,ReLU()
            # self.relu=nn.ReLU()
            # self.out=nn.Softmax(dim=1)
        else:
            if split:
                self.sc1=nn.Linear(in_features=n_pca,out_features=(n_units-10*n_ae)//2)
                self.sc2=nn.Linear(in_features=n_pca,out_features=(n_units-10*n_ae)//2)
                self.sc3=nn.Linear(in_features=n_ae,out_features=10*n_ae)
            else:
                self.sc1=nn.Linear(in_features=npca,out_features=n_units[0]-10*n_ae)
                self.sc2=nn.Linear(in_features=n_ae,out_features=10*n_ae)

            # self.fc1=nn.Linear(n_pcs,n_units[0])
            # self.activ1=nn.ReLU()
            ctr=1
            name="self.fc"
            # activname="self.activ"
            while ctr+1<n_layers-1:
                exec(name+str(ctr)+"=nn.Linear("+str(n_units[ctr-1])+","+\
                    str(n_units[ctr])+")")
                # exec(activname+str(ctr+1)+"=nn.ReLU()")
                ctr+=1
            exec(name+str(n_layers-1)+"=nn.Linear("+str(n_units[-1])+","+str(n_par)+")")
        # self.dropout=nn.Dropout(0.2)
            # self.out=nn.Softmax(dim=1)
            # self.relu=nn.ReLU()
    def forward():
        n_ae=2
        if split:
            n_pca=(n_pcs-n_ae)//2
        else:
            n_pca=n_pcs-n_ae
        if split:
            x1=self.sc1(x[:,:n_pca])
            x2=self.sc2(x[:,n_pca:-n_ae])
            x3=self.sc3(x[:,-n_ae:])
            x=torch.cat((x1,x2,x3),axis=1)
            del x1,x2,x3
        else:
            x1=self.sc1(x[:,:-n_ae])
            x2=self.sc2(x[:,-n_ae:])
            x=torch.cat((x1,x2),axis=1)
            del x1,x2
        name="self.fc"
        ctr=1
        while ctr<n_layers:
            name="self.fc"+str(ctr)
            x=F.relu(x)
            x=eval(name+'(x)')
            ctr+=1        
        return x
    def predict():
        x=self.forward().cpu()
        return x.detach()

class LSTM(nn.Module):
    def __init__(self,input_size=1,hidden_layer_size=100,output_size=3,num_layers=1):
        super().__init__()
        self.num_lstm_layers = num_layers
        self.input_size        = input_size
        self.hidden_layer_size = hidden_layer_size
        self.outout_size       = output_size

        self.lstm = nn.LSTM(input_size,hidden_layer_size,num_layers=self.num_lstm_layers)

        self.linear1 = nn.Linear(hidden_layer_size,hidden_layer_size)
        self.linear2 = nn.Linear(hidden_layer_size,hidden_layer_size)
        self.linear3 = nn.Linear(hidden_layer_size,hidden_layer_size)
        self.linear4 = nn.Linear(hidden_layer_size,50*output_size)
        self.linear5 = nn.Linear(50*output_size,5*output_size)
        self.linear6 = nn.Linear(5*output_size,output_size)

        self.hidden_cell = (torch.zeros(self.num_lstm_layers,10,self.hidden_layer_size).cuda(),
                            torch.zeros(self.num_lstm_layers,10,self.hidden_layer_size).cuda())

    def forward(self,input_seq):
        lstm_out, self.hidden_cell = self.lstm(torch.from_numpy(input_seq).cuda().float(),
                                                 self.hidden_cell) #.view(len(input_seq),10,-1)

        predictions = self.linear1(lstm_out[:,-1,:]) #.view(len(input_seq),10,-1)
        predictions = F.relu(predictions)
        predictions = self.linear2(predictions)
        predictions = F.relu(predictions)
        predictions = self.linear3(predictions)
        predictions = F.relu(predictions)
        predictions = self.linear4(predictions)
        predictions = F.relu(predictions)
        predictions = self.linear5(predictions)
        predictions = F.relu(predictions)
        predictions = self.linear6(predictions)

        return predictions
