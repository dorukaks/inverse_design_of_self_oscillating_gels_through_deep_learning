"""
Regression trial 
v0 Created on Wed Mar 11 2020 15.42
v1 Created on Thur Mar 19 2020 09.48
    Changes: Added y coordinates of the lattice points as discussed with AG


Author: Doruk
"""
#This python file is created to do the type classification of the simulation
#results based on the previous classifiers (on 1st dataset) written by DK

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import time
import pickle
import joblib
import torch
import torch.nn as nn
import torch.nn.functional as F
import Regresor as reg
#import matplotlib.cm as cm
#import pandas as pd

# from sklearn.pipeline import Pipeline
# from mpl_toolkits.mplot3d import Axes3D
# from matplotlib.font_manager import FontProperties
#form sklearn.decomposition import FastICA, KernelPCA, NMF
# from sklearn.cluster import KMeans
# from sklearn.metrics import silhouette_score, silhouette_samples

# from sklearn.neural_network import MLPRegressor, MLPClassifier
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, r2_score, explained_variance_score, mean_squared_error


train_sets=['61','62','63','64','65','66','67','68','69','70']
# ['61','62','63','64','65','66','67','68','69','70']
# test_sets=['61','62','63','64','65','66','67','68','69','70']
# test_sets=['71','72','73','74','75','76','77','78','79','80']
test_sets=['71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','141','142','143','144','145','146','147','148','149','150']
# n_pca=np.array([10])
# n_pca=np.array([10,15,20,25,30])
n_pca=np.array([5,20,40,50,60,80])
n_par=3
ms_error=np.zeros((len(train_sets),len(n_pca),len(test_sets)))
mean_percent_error=np.zeros((len(train_sets),len(n_pca),len(test_sets),n_par))
relative_mse=np.zeros((len(train_sets),len(n_pca),len(test_sets),n_par))
rsq_score=np.zeros((len(train_sets),len(n_pca),len(test_sets)))
pca_scores=np.zeros((len(train_sets),len(n_pca)))
train_time=np.zeros((len(train_sets),len(n_pca)))
training_loss=[]
instances=1
split=False

params=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Parameters_type1')['CA_Gelsdata']
# Take everything excetp the label, we don't need the label we already know it eh?
params=params[:6400,:n_par]

# Record the max values for the parameters
# coef0=max(params[:,0])
# coef1=max(params[:,1])
# coef2=max(params[:,2])
# # scale the parameters so that everything is in [0,1]
# params[:,0]=params[:,0]/coef0
# params[:,1]=params[:,1]/coef1
# params[:,2]=params[:,2]/coef2

params=params[:,:n_par]
# scaler=StandardScaler()
# scaler.fit(train_after_pca)
# # scaler.fit(x_test_after_pca)
# train_after_pca=scaler.transform(train_after_pca) 
paramscaler=StandardScaler()
paramscaler.fit(params)
params=paramscaler.transform(params)

start_time=time.time()
for train in range(len(train_sets)):
    print('Iteration for train t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, has started.')

    # training_loss_train=[]
    #Load files and extract the cell structures from files

    #For Windows
    # mat_contents_t0=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type1')['CA_Gelsdata']
    # mat_contents_t1=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type2')['CA_Gelsdata']
    
    #For Ubuntu
    mat_contents_x=sio.loadmat\
        ('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'\
            +train_sets[train]+'_type1')['CA_Gelsdata']
    mat_contents_y=sio.loadmat\
        ('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_y'\
            +train_sets[train]+'_type1')['CA_Gelsdata']
    
    # print(mat_contents_t0.shape,mat_contents_t0[0].shape,mat_contents_t0[0][0].shape)
    
    mat_contents_x=mat_contents_x[:6400]
    mat_contents_y=mat_contents_y[:6400]
    # mat_contents_t1=mat_contents_t1[:6400]

    #create label vectors for the data
    # train_label_0=np.zeros(mat_contents_t0.shape)
    # train_label_1=np.ones(mat_contents_t1.shape)
    x_train=np.zeros((mat_contents_x.shape[0],mat_contents_x[0][0].shape[1]))
    y_train=np.zeros((mat_contents_y.shape[0],mat_contents_y[0][0].shape[1]))
    # x_train_1=np.zeros((mat_contents_t1.shape[0],mat_contents_t1[0][0].shape[1]))
    # print(x_train_0.shape)

    #Extract the x points from the cells to a big array
    for idx in range(len(mat_contents_x)): 
        x_train[idx]=mat_contents_x[idx][0]
        y_train[idx]=mat_contents_y[idx][0]
    # for idx in range(len(mat_contents_t1)): x_train_1[idx]=mat_contents_t1[idx][0]

    #Here we have the x positions of the 2*6400=12800 simulations at one time instant(e.g. 
    # t=6.1s) for type1 and type2) extracted from the .mat file. Then the next
    # step is to reduce the dimension of this huge sized matrix by applying principle
    # component analysis (PCA)

    train_all=np.append(x_train,y_train,axis=1)
    # label_train_all=np.append(train_label_0,train_label_1,axis=0)

    train_all_labeled=np.append(train_all,params,axis=1)

    #Shuffle the labeled data to get a "neutral" dataset for batching in future#
    np.random.shuffle(train_all_labeled)
    #Separate the labels and data part of the shuffled dataset#
   # x_label_shuffled=x_train_all_labeled[:,-3:]
   # x_train_shuffled=x_train_all_labeled[:,0:-3]
    label_shuffled=train_all_labeled[:,-n_par:]
    train_shuffled=train_all_labeled[:,0:-n_par]
    label_shuffled=label_shuffled.astype(float)
    train_shuffled=train_shuffled.astype(float)

    # print('Dimension of the training data:',x_train_all.shape)

    #divide the set as training/test set#
    # x_train , x_test , x_label_train ,x_label_test = \
    #     train_test_split(x_train_shuffled,x_label_shuffled,test_size=0.001)
    # This is not used since the training will be done on the structured grid set
    # and the testing is done on the randomized grid set#
    
    train_set=train_shuffled
    label_train=label_shuffled
    # Removing the unnecessary data from the memory for efficiency
    del label_shuffled,train_shuffled,train_all_labeled,mat_contents_x,mat_contents_y

    a,b = train_set.shape
#buraya pca loop'u gelecek bu sayede en dista train loopu sonra icerde pca loopu en icte de 
# test loopu olacak sekilde butun simulasyon kosacak#
    for idx in range(len(n_pca)):
        print('Iteration with N_PCA:',n_pca[idx],' has started.')
        ipca=IncrementalPCA(n_components = n_pca[idx],batch_size=2000)
        ipca.fit(train_set)
        train_pca=ipca.transform(train_set)
        pca_score = ipca.explained_variance_ratio_
        # x_test_pca = pca.transform(x_test)



        N_p_variance=1
        N_p=n_pca[idx]#number of principal components used

        #create the hidden layer architecture#
        u=1000
        v=1000
        w=1000
        y=1000
        z=1000
        hidden_layer = [y,y,y,y]

        train_after_pca=train_pca[:,0:N_p]
        # x_test_after_pca=x_test_pca[:,0:N_p]

        pca_score_sum=sum(pca_score[0:N_p])
        pca_scores[train,idx]=pca_score_sum

        scaler=StandardScaler()
        scaler.fit(train_after_pca)
        # scaler.fit(x_test_after_pca)
        train_after_pca=scaler.transform(train_after_pca)
        # x_test_after_pca=scaler.transform(x_test_after_pca)
        print("train shape\n",train_after_pca.shape)


        # print('\n\nTrain t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
        #     +'.'+test_sets[test][1]+
        # 's results:')

        # print('\n [MLP Classifier Result]')
        # print('\n* The number of PCs :', N_p, \
        #         '\n* Total PCA score :',pca_score_sum,\
        #         '\n* Hidden Layer :', hidden_layer)
        print('Neural network training started...')
        inter_time=time.time()

        network = reg.Regresor(hidden_layer,N_p,n_par)
        print(network)
        network.train()
        criterion = nn.MSELoss().cuda()
        # criterion = nn.BCEWithLogitsLoss().cuda()
        optimizer = torch.optim.Adam(network.parameters(),lr=0.001)
        epochs = 50
        # training_loss=np.zeros((len(train_sets),len(n_pca),epochs))
        # training_loss_sizes=np.zeros((len(train_sets),len(n_pca)))
        # training_loss=np.array()
        losses=[]
        network=network.cuda()

        try:
            label_train=torch.from_numpy(label_train).type(torch.cuda.FloatTensor)
            label_train.requires_grad_()
        except TypeError:
            label_train=label_train.cuda()
            label_train.requires_grad_()

        # batch_size=len(x_label_train) #single batch
        batch_size=25 #26 batches
        batch_number=np.ceil(len(label_train)/batch_size).astype(int)
        print(batch_number," batches with ",batch_size," elements")
        
        for i in range(epochs):
            current_batch=0
            batch_loss=[]
            # print(i)
            while current_batch<batch_number:
                if current_batch<len(label_train)//batch_size:
                    # print(current_batch*batch_size,(current_batch+1)*batch_size)
                    train_batch=train_after_pca[current_batch*batch_size:(current_batch+1)*batch_size,:]
                    label_batch=label_train[current_batch*batch_size:(current_batch+1)*batch_size]
                else:
                    # print(current_batch*batch_size)
                    train_batch=train_after_pca[current_batch*batch_size:,:]
                    label_batch=label_train[current_batch*batch_size:]
                network.train()            
                optimizer.zero_grad()
                ypred=network.forward_ReLU(train_batch,len(hidden_layer)+2)
                # x_label_train=torch.from_numpy(x_label_train).type(torch.cuda.LongTensor)
                # print("ypred\n",ypred)
                # print("xlabeltrain\n",x_label_train.shape)
                loss=criterion(ypred,label_batch.detach())
                loss.backward()
                optimizer.step()
                batch_loss.append(loss.item())
                current_batch+=1
            losses.append(np.mean(batch_loss))
            if (i+1)%5==0: print("Epoch: ",i+1,"/",epochs," Current Training Loss: ",losses[i])
            if i+1>5 and abs(losses[-1])<1e-4:
                print("Terminated at Epoch:",i+1,"/",epochs," Current Training Loss: ",losses[i])
                break

        training_loss.append(losses)
        # training_loss[train,idx,:len(losses)]=losses
        # training_loss_sizes[train,idx]=len(losses)
        train_time[train,idx]=time.time()-inter_time
        network.eval()
        label_predict=network.predict(train_after_pca,len(hidden_layer)+2)
        # print(label_predict.shape)
        label_predict=label_predict.cpu()
        label_predict=label_predict.numpy()
        label_train=label_train.cpu()
        label_train=label_train.detach().numpy()
        # print(label_predict)
        print("New pytorch network mserror: ",mean_squared_error(label_train,label_predict))
        print("New pytorch network rsquared value: ",r2_score(label_train,label_predict))
        print('Neural network trained in ',train_time[train,idx],'s.')

#        def predict(x):
#            x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
#            ans=network.predict(x)
#            return ans.numpy()

        # mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
        #                     max_iter = 2000)
        # mlp.fit(x_train_after_pca,x_label_train)

        network_name=''
        for iii in range(len(hidden_layer)):
            network_name=network_name+str(hidden_layer[iii])+'_'
        network_name=layer_info=network_name[:-1]
        network_name='MLPRegressor_pytorch_pcsearch_1instance_train'+train_sets[train]+'s_pc'+str(n_pca[idx])+'_'+\
            str(len(hidden_layer))+'layers_'+network_name+'.pth'
        torch.save({
            'network_state_dict':network.state_dict(),
            'optimizer_state_dict':optimizer.state_dict(),
            'network':network,
            'optimizer':optimizer,
            'epoch':epochs,
            'paramscaler':paramscaler,
            'batch_size':batch_size,
            'pca':ipca,
            'scaler':scaler,
            'hidden_layer':hidden_layer,
            'N_p':N_p,
            'split':split,
            'instances':instances
        },network_name)


        # # the count of true negatives is C_0,0, false negatives is C_1,0, 
        # # true positives is C_1,1 and false positives is C_0,1.
        # print('\n\nResults of the trained neural network:')
        # print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
        # print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
        # print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))        




        for test in range(len(test_sets)):
            if True:#change this to test!=train to switch back to grid-grid testing.
                network.eval()
                #call secondary test set#
                #For Windows
                # secondary_mat_contents_0 =\
                #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type1')\
                #         ['CA_Gelsdata']
                # secondary_mat_contents_1 =\
                #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type2')\
                #         ['CA_Gelsdata']
                #For Ubuntu
                # secondary_mat_contents_0 =\
                #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type1')\
                #         ['CA_Gelsdata']
                # secondary_mat_contents_1 =\
                #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type2')\
                #         ['CA_Gelsdata']

                # secondary_mat_contents =sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Pts_x'+test_sets[test]+'_type1')['CA_Gelsdata']
                
                # secondary_mat_contents_x=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+train_sets[test]+'_type1')['CA_Gelsdata']
                # secondary_mat_contents_y=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_y'+train_sets[test]+'_type1')['CA_Gelsdata']
                # secondary_params=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Parameters_type1')['CA_Gelsdata']

                # secondary_mat_contents_x=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Pts_x'+test_sets[test]+'_type1')['CA_Gelsdata']
                # secondary_mat_contents_y=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Pts_y'+test_sets[test]+'_type1')['CA_Gelsdata']
                # secondary_params=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Parameters_type1')['CA_Gelsdata']
                secondary_mat_contents_x=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_x'+test_sets[test]+'_type1')['CA_Gelsdata']       
                secondary_mat_contents_y=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_y'+test_sets[test]+'_type1')['CA_Gelsdata']
                secondary_params=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Parameters_type1')['CA_Gelsdata']

                # secondary_params =sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Parameters_type1')['CA_Gelsdata']
                # secondary_params=secondary_params[:,:-1]
                secondary_params=secondary_params[:,:n_par]
                # secondary_params=secondary_params[:,0]

                # secondary_mat_contents_1 =\
                #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Pts_x'+test_sets[test]+'_type2')\
                #         ['CA_Gelsdata']
                
                # secondary_mat_contents_x=secondary_mat_contents_x[:6400]
                # secondary_mat_contents_y=secondary_mat_contents_y[:6400]

                #Extract the data and create labels etc.#
                # secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
                # secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
                secondary_test_data_x=np.zeros((secondary_mat_contents_x.shape[0],secondary_mat_contents_x[0][0].shape[1]))
                secondary_test_data_y=np.zeros((secondary_mat_contents_y.shape[0],secondary_mat_contents_y[0][0].shape[1]))
                # secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
                #Extract the x points from the cells to a big array
                for ii in range(len(secondary_mat_contents_x)):
                    secondary_test_data_x[ii]=secondary_mat_contents_x[ii][0]
                    secondary_test_data_y[ii]=secondary_mat_contents_y[ii][0]
                    # if ii<len(secondary_mat_contents_1):
                # for ii in range(len(secondary_mat_contents_1)):
                #         secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]
                secondary_test_data_all=np.append(secondary_test_data_x,secondary_test_data_y,axis=1)
                # secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
                secondary_test_shuffled=np.append(secondary_test_data_all,secondary_params,axis=1)

                #Shuffle the labeled data to get a "neutral" dataset#
                np.random.shuffle(secondary_test_shuffled)
                #Separate the labels and data part of the shuffled dataset#
#                secondary_test_label=secondary_test_shuffled[:,-3:]
                secondary_test_label=secondary_test_shuffled[:,-n_par:]
                secondary_test_data=secondary_test_shuffled[:,:-n_par]
                secondary_test_label=secondary_test_label.astype(float)
                secondary_test_data=secondary_test_data.astype(float)
        
                secondary_test_data_after_pca = ipca.transform(secondary_test_data)
                secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]
                secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
                # secondary_test_data_after_pca=torch.from_numpy(secondary_test_data_after_pca).type(torch.cuda.FloatTensor)
                secondary_label_predicted=network.predict(secondary_test_data_after_pca,len(hidden_layer)+2)
                secondary_label_predicted=secondary_label_predicted.cpu()
                secondary_label_predicted=secondary_label_predicted.numpy()
                # secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)

                secondary_label_predicted=paramscaler.inverse_transform(secondary_label_predicted)
                # secondary_label_predicted[:,0]=secondary_label_predicted[:,0]*coef0
                #secondary_label_predicted[:,1]=secondary_label_predicted[:,1]*coef1
                #secondary_label_predicted[:,2]=secondary_label_predicted[:,2]*coef2
                
                ms_error[train,idx,test]=mean_squared_error(secondary_test_label,secondary_label_predicted)
                rsq_score[train,idx,test]=r2_score(secondary_test_label,secondary_label_predicted)
                mean_percent_error[train,idx,test,:]=np.mean(abs(secondary_test_label-secondary_label_predicted)/secondary_test_label*100,axis=0)
                relative_mse[train,idx,test,:]=np.mean((secondary_test_label-secondary_label_predicted)**2,axis=0)/np.mean(secondary_test_label**2,axis=0)
                # plt.figure()
                # plt.plot(np.linspace(0,1.5,10),np.linspace(0,1.5,10))
                # plt.scatter(secondary_test_label,secondary_label_predicted)
                # plt.xlabel('actual label')
                # plt.ylabel('predicted label')
                # plt.show()

            else:
                network.eval()
                # x_label_predicted=mlp.predict(x_test_after_pca)
                # x_test_after_pca=torch.from_numpy(x_test_after_pca).type(torch.cuda.FloatTensor)
                x_label_predicted=network.predict(x_test_after_pca,len(hidden_layer)+2)
                x_label_predicted=x_label_predicted.cpu()
                x_label_predicted=x_label_predicted.numpy()
                ms_error[train,idx,test]=accuracy_score(x_label_test,x_label_predicted)
            print('Train t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
            +'.'+test_sets[test][1]+'s completed.')
            print('* Accuracy of the neural network: ',rsq_score[train,idx,test])
            print('* Mean percentage error         : ',np.round(mean_percent_error[train,idx,test,:],decimals=2))
            
print('Execution of the whole code completed. Total runtime: ',time.time()-start_time,'s')

# Dumping the necessary information into a .pckl file for future post processing of the
# simulation results. #
save_name='PCA_pytorch_pcsearch_1instance_'+str(len(hidden_layer))+'layers_'+\
    layer_info+'.pckl'
save=open(save_name,'wb')
pickle.dump([train_sets,test_sets,n_pca,hidden_layer,n_par,train_time,ms_error,rsq_score,mean_percent_error,training_loss,instances],save)
save.close

line_colors=['r-o','b-o','g-o','c-o','m-o','k-o']
fig,axs=plt.subplots(1,len(train_sets),sharey=True)
fig.suptitle('Mean Square Error for different number of PCs')
for train in range(len(train_sets)):
    for pcs in range(len(n_pca)):
            axs[train].plot(ms_error[train,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))      
    axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
    axs[train].set_xticklabels([])

handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')
# plt.xticks([])

fig,axs=plt.subplots(1,len(train_sets),sharey=True)
fig.suptitle('$R^2$ Score for different number of PCs')
for train in range(len(train_sets)):
    for pcs in range(len(n_pca)):
            axs[train].plot(rsq_score[train,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))    
    axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
    axs[train].set_xticklabels([])

handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')
# plt.xticks([])
for idx in range(n_par):
    fig,axs=plt.subplots(1,len(train_sets),sharey=True)
    fig.suptitle('Mean Percentage error for parameter '+str(idx+1)+' with different number of PCs')
    for train in range(len(train_sets)):
        for pcs in range(len(n_pca)):
                axs[train].plot(mean_percent_error[train,pcs,:,idx],line_colors[pcs],label='PC='+str(n_pca[pcs]))
        axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
        axs[train].set_xticklabels([])

    handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
    fig.legend(handles,labels,loc='lower right')
# plt.xticks([])

plt.show()
                    

'''

#second test set #
secondary_mat_contents_0 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type1')['CA_Gelsdata']
secondary_mat_contents_1 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type2')['CA_Gelsdata']


#create label vectors for the data
secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
print(secondary_test_data_0.shape)

#Extract the x points from the cells to a big array
for idx in range(len(secondary_mat_contents_0)):
    secondary_test_data_0[idx]=secondary_mat_contents_0[idx][0]
    if idx<len(secondary_mat_contents_1):
# for idx in range(len(mat_contents_t1)):
        secondary_test_data_1[idx]=secondary_mat_contents_1[idx][0]

secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

#Shuffle the labeled data to get a "neutral" dataset#
np.random.shuffle(secondary_test_shuffled)
#Separate the labels and data part of the shuffled dataset#
secondary_test_label=secondary_test_shuffled[:,-1]
secondary_test_data=secondary_test_shuffled[:,0:-1]
secondary_test_label=secondary_test_label.astype(int)
secondary_test_data=secondary_test_data.astype(float)



N_p_variance=1
N_p=30 #number of principal components used

#create the hidden layer architecture#
u=30
# v=30
# w=30

hidden_layer = [u]
x_train_after_pca=x_train_pca[:,0:N_p]
x_test_after_pca=x_test_pca[:,0:N_p]
pca_score_sum=sum(pca_score[0:N_p])

scaler=StandardScaler()
scaler.fit(x_train_after_pca)
# scaler.fit(x_test_after_pca)
x_train_after_pca=scaler.transform(x_train_after_pca)
x_test_after_pca=scaler.transform(x_test_after_pca)

print('\n\nTrain t=6.1s, Test t=6.1s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                    max_iter = 1000)
mlp.fit(x_train_after_pca,x_label_train)
x_label_predicted=mlp.predict(x_test_after_pca)

# the count of true negatives is C_0,0, false negatives is C_1,0, 
# true positives is C_1,1 and false positives is C_0,1.
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))

x_train_shuffled_62_pca=pca.transform(secondary_test_data)
pca_score = pca.explained_variance_ratio_
pca_score_sum=sum(pca_score[0:N_p])
x_train_shuffled_62_pca=scaler.transform(x_train_shuffled_62_pca)
print('\n\nTrain t=6.1s, Test t=6.2s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

x_label_shuffled_pred_62=mlp.predict(x_train_shuffled_62_pca)
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(secondary_test_label,x_label_shuffled_pred_62))                
print('* Classification report\n',classification_report(secondary_test_label,x_label_shuffled_pred_62))
print('* Accuracy of the neural network: ',accuracy_score(secondary_test_label,x_label_shuffled_pred_62))
# print(precision_recall)

'''