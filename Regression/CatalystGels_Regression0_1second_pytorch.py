"""
Classification trial 
Changelog:
v0 Created on Fri Mar 20 2020 14.40
v1 Created on Wed May 20 2020 10.37
    Changes: Added the second "corrective" layer MLP

Author: Doruk
"""
#This python file is created to do the type classification of the simulation
#results based on the previous classifiers (on 1st dataset) written by DK

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import time
import pickle
import joblib
import torch
import torch.nn as nn
import torch.nn.functional as F
import Regresor as reg
from os import remove
#import matplotlib.cm as cm
#import pandas as pd

# from sklearn.pipeline import Pipeline
# from mpl_toolkits.mplot3d import Axes3D
# from matplotlib.font_manager import FontProperties
#form sklearn.decomposition import FastICA, KernelPCA, NMF
# from sklearn.cluster import KMeans
# from sklearn.metrics import silhouette_score, silhouette_samples

# from sklearn.neural_network import MLPRegressor, MLPClassifier
# from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA, IncrementalPCA,FastICA
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, r2_score, explained_variance_score, mean_squared_error

# 3rd dm = 71-80
# 4th dm = 61-70
# 5th dm = 71-90, 141-150
mean_squared_error
# n_pca=np.array([5,10,15,20,25])
# n_pca=np.array([20,40,60,80,100,120])
n_pca=np.array([40,50,100,200,250])
n_par=3
dms=["3rd","4th"]
test_dms=["5th","6th","7th"]
# dms=["3rd","4th","5th"]
# shift=3 #make the training set 0.3s apart
rsq_score=np.zeros((len(dms),len(test_dms),len(n_pca),10))
ms_error=np.zeros((len(dms),len(test_dms),len(n_pca),10))
mean_percent_error=np.zeros((len(dms),len(test_dms),len(n_pca),10,n_par))
relative_mse=np.zeros((len(dms),len(test_dms),len(n_pca),10,n_par))
pca_scores=np.zeros((len(dms),len(n_pca)))
train_time=np.zeros((len(dms),len(n_pca)))
training_loss=[]
instances=10
split=True
double_mlp=True
drop=False
add_z=True
inv_tr=False
preprocess="ica"
pri_1="z"
pri_2="y"
sec_1="x"


if double_mlp:
    double_statement="dualMLP_"
else:
    double_statement=""
if drop:
    drop_statement="dropout_"
else:
    drop_statement=""
if add_z:
    z_statement="addZ_"
else:
    z_statement=""

if split:
    splitstatement="split_"
else:
    splitstatement="nosplit_"
if preprocess=="ica":
    preprocess_statement="ica_"
elif preprocess=="pca":
    preprocess_statement="pca_"
typeind=1
sets=['3rd','4th']

start_time=time.time()
for idx in range(len(n_pca)):
    
    print("Ensemble learning loop for ",n_pca[idx],' principal components has started')
    try:
        ensemble_predicted
    except NameError:
        pass
    else:
        del ensemble_predicted
    for train in range(len(dms)):
        print("train loop #",train+1)
        # bu loop sanirim kalkacak
        training_interval=train+3
        if training_interval==3:
            train_sets=['71','72','73','74','75','76','77','78','79','80']
            test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90']]
            dm='3rd'
        elif training_interval==4:
            train_sets=['61','62','63','64','65','66','67','68','69','70']
            test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90']]
            dm='4th'
        else:
            train_sets=['141','142','143','144','145','146','147','148','149','150']
            test_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70']]
            dm='5th'

        print('Iteration for Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
            +train_sets[-1][0]+'.'+train_sets[-1][1]+'s has started.')

        params=sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Parameters_type'+str(typeind))['CA_Gelsdata']
        params=params[:6400,:n_par].reshape(-1,n_par)
        exec("paramscaler_"+dm+"=StandardScaler()")
        exec("paramscaler_"+dm+".fit(params)")
        params=eval("paramscaler_"+dm).transform(params)
        #For Ubuntu
        # Certainly we need to find a better way to do this....

        # data load loop
        for iii in range(len(train_sets)):
            print("data load #",iii+1)
            matnamex="mat_contents_x"+str(iii+1)
            matnamey="mat_contents_y"+str(iii+1)
            if dm!="5th":
                exec(matnamex+"=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_"+pri_1+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                exec(matnamey+"=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_"+pri_2+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")    
            else:
                exec(matnamex+"=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_R1_Part1_Pts_"+pri_1+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                exec(matnamey+"=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_R1_Part1_Pts_"+pri_2+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")

            exec(matnamex+"="+matnamex+"[:6400]")
            exec(matnamey+"="+matnamey+"[:6400]")

            arrnamex="train_x"+str(iii+1)
            arrnamey="train_y"+str(iii+1)
            exec(arrnamex+"=np.zeros(("+matnamex+".shape[0],"+matnamex+"[0][0].shape[1]))")
            exec(arrnamey+"=np.zeros(("+matnamey+".shape[0],"+matnamey+"[0][0].shape[1]))")
            exec("for i_idx in range(len("+matnamex+")):"+arrnamex+"[i_idx]="+matnamex+"[i_idx][0]")
            exec("for i_idx in range(len("+matnamey+")):"+arrnamey+"[i_idx]="+matnamey+"[i_idx][0]")
            all_labeledname="train_all_labeled_p"+str(iii+1)
            exec("del "+matnamex+","+matnamey)
            exec("x_size="+arrnamex+".shape[1]")
            exec(all_labeledname+"=np.append("+arrnamex+","+arrnamey+",axis=1)")
            exec("del "+arrnamex+","+arrnamey)

            if add_z:
                # data call
                print("Extra data load #",iii+1)
                matname3="mat_contents_3"+str(iii+1)
                if dm!="5th":
                    exec(matname3+"=sio.loadmat\
                    ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_"+sec_1+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")    
                else:
                    exec(matname3+"=sio.loadmat\
                    ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_R1_Part1_Pts_"+sec_1+train_sets[iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                exec(matname3+"="+matname3+"[:6400]")
                arrname3="train_3"+str(iii+1)
                exec(arrname3+"=np.zeros(("+matname3+".shape[0],"+matname3+"[0][0].shape[1]))")
                exec("for i_idx in range(len("+matname3+")):"+arrname3+"[i_idx]="+matname3+"[i_idx][0]")
                exec("del "+matname3)
                exec("size_3="+arrname3+".shape[1]")
                exec(all_labeledname+"=np.append("+all_labeledname+","+arrname3+",axis=1)")
                exec("del "+arrname3)


            exec(all_labeledname+"=np.append("+all_labeledname+",params,axis=1)")
            exec("np.random.shuffle("+all_labeledname+")")

            training_data="train_p"+str(iii+1)
            training_label="label_train_p"+str(iii+1)
            exec(training_label+"="+all_labeledname+"[:,-"+str(n_par)+":]")
            exec(training_data+"="+all_labeledname+"[:,:-"+str(n_par)+"]")
            exec("del "+all_labeledname)
            exec(training_label+"="+training_label+".astype(float)")
            exec(training_data+"="+training_data+".astype(float)")

        # print("line ",145)
        data_train=train_p1
        del train_p1
        data_label=label_train_p1
        del label_train_p1

        #Found it, lol
        for iii in range(len(train_sets)-1):
            train_k="train_p"+str(iii+2)
            train_all="data_train"
            label_k="label_train_p"+str(iii+2)
            label_all="data_label"
            exec(train_all+"=np.append("+train_all+","+train_k+",axis=0)")
            exec(label_all+"=np.append("+label_all+","+label_k+",axis=0)")
            exec("del "+train_k+","+label_k)
        data_label=np.reshape(data_label,(data_train.shape[0],n_par))

        split_len=x_size

        print('data train shape:',data_train.shape)
        print('data label shape:',data_label.shape)
        data_train_all=np.append(data_train,data_label,axis=1)
        del data_label
        np.random.shuffle(data_train_all)
        data_train=data_train_all[:,0:-n_par]
        data_label_train=data_train_all[:,-n_par:]
        del data_train_all
        if add_z:
            extra_data=data_train[:,-size_3:]
            print(extra_data.shape)
            data_train=data_train[:,0:-size_3]
            extra_save_name="extra_training_data.pckl"
            save_extra=open('extra_training_data.pckl','wb')
            pickle.dump(extra_data,save_extra)
            save_extra.close
            del extra_data

        save_name='temp_training_data.pckl'
        data_size=data_train.shape[0]
        save1=open('temp_training_data1.pckl','wb')
        pickle.dump(data_train[:data_size//2,:],save1)
        save1.close
        save2=open('temp_training_data2.pckl','wb')
        pickle.dump(data_train[data_size//2:,:],save2)
        save2.close

    # for idx in range(len(n_pca)):
        # bu loop kaldirilacak


        if idx!=0:
            # del data_train
            f=open('temp_training_data1.pckl','rb')
            data_train=pickle.load(f)
            f.close
            f=open('temp_training_data2.pckl','rb')
            data_train=np.append(data_train,pickle.load(f),axis=0)
            f.close
        try:
            network
        except NameError:
            pass

        else:
            del network
        print('Iteration with #PC:',n_pca[idx],' has started.')
        if split:
            if preprocess=="pca":
                exec("ipcax_"+dm+"=IncrementalPCA(n_components=n_pca[idx],copy=False,batch_size=500)")
                exec("ipcay_"+dm+"=IncrementalPCA(n_components=n_pca[idx],copy=False,batch_size=500)")
            elif preprocess=="ica":
                exec("ipcax_"+dm+"=FastICA(n_components=n_pca[idx],max_iter=1000,algorithm='deflation')")
                exec("ipcay_"+dm+"=FastICA(n_components=n_pca[idx],max_iter=1000,algorithm='deflation')")


            exec("scalery_"+dm+"=StandardScaler(copy=False)")
            # scalery.fit(data_train[:,split_len:])
            # y_train=scalery.transform(data_train[:,split_len:])

            exec("scalerx_"+dm+"=StandardScaler(copy=False)")
            # scalerx.fit(data_train[:,:split_len])
            # data_train=scalerx.transform(data_train[:,:split_len])
            # del data_train

            exec("ipcax_"+dm+".fit(data_train[:,split_len:])")
            exec("ipcay_"+dm+".fit(data_train[:,:split_len])")
            y_train=eval("ipcay_"+dm).transform(data_train[:,:split_len])
            data_train=eval("ipcax_"+dm).transform(data_train[:,split_len:])
            # print("shapes after pca",x_train.shape,y_train.shape,data_train.shape)
            exec("scalerx_"+dm+".fit(data_train)")
            data_train=eval("scalerx_"+dm).transform(data_train)
            exec("scalery_"+dm+".fit(y_train)")
            y_train=eval("scalery_"+dm).transform(y_train)
            data_train=np.append(data_train,y_train,axis=1)
            N_p=n_pca[idx]*2
            del y_train
        else:
            ipca=IncrementalPCA(n_components=n_pca[idx],batch_size=500)
            ipca.fit(data_train)
            data_train=ipca.transform(data_train)
            N_p=n_pca[idx]
            scaler=StandardScaler()
            scaler.fit(data_train)
            data_train=scaler.transform(data_train)

        N_p_variance=1

        #create the hidden layer architecture#
        u=300
        v=1000
        w=1000
        y=200
        hidden_layer = [y,y,y,y,y]

        exec("network_"+dm+" = reg.Regresor(hidden_layer,N_p,n_par)")
        print(eval("network_"+dm))
        eval("network_"+dm+".train()")
        criterion = nn.MSELoss().cuda()
        optimizer = torch.optim.Adam(eval("network_"+dm).parameters(),lr=0.001)
        epochs = 100
        exec("network_"+dm+"=network_"+dm+".cuda()")
        try:
            data_label_train=torch.from_numpy(data_label_train).type(torch.cuda.FloatTensor)
            data_label_train.requires_grad_()
        except TypeError:
            data_label_train=data_label_train.cuda()
            data_label_train.requires_grad_()

        # batch_size=len(x_label_train) #single batch
        batch_size=250 #26 batches
        batch_number=np.ceil(len(data_label_train)/batch_size).astype(int)
        print(batch_number," batches with ",batch_size," elements")
        losses=[]
        print('Neural network training started...')
        inter_time=time.time()
        for i in range(epochs):
            current_batch=0
            batch_loss=[]
            # print(i)
            while current_batch<batch_number:
                exec("network_"+dm+".train()")            
                optimizer.zero_grad()
                if current_batch<len(data_label_train)//batch_size:
                    # print(current_batch*batch_size,(current_batch+1)*batch_size)
                    # x_train_batch=x_train_after_pca[current_batch*batch_size:(current_batch+1)*batch_size,:]
                    data_label_batch=data_label_train[current_batch*batch_size:(current_batch+1)*batch_size]
                    # ypred=network.forward_ReLU(x_train_after_pca[current_batch*batch_size:(current_batch+1)*batch_size,:],len(hidden_layer)+2)
                    loss=criterion(eval("network_"+dm).forward_ReLU(data_train[current_batch*batch_size:\
                        (current_batch+1)*batch_size,:],len(hidden_layer)+2),\
                            data_label_batch.detach())
                    #hala memory sorunu yasarsan buraya bak, ypred ve x_label_batch'i de
                    # elimine edebilirsin!!!!!!!
                else:
                    # print(current_batch*batch_size)
                    # x_train_batch=x_train_after_pca[current_batch*batch_size:,:]
                    data_label_batch=data_label_train[current_batch*batch_size:]
                    # ypred=network.forward_ReLU(x_train_after_pca[current_batch*batch_size:,:],len(hidden_layer)+2)
                    loss=criterion(eval("network_"+dm).forward_ReLU(data_train[current_batch*batch_size:,:]\
                        ,len(hidden_layer)+2),data_label_batch.detach())

                # ypred=network.forward_ReLU(x_train_batch,len(hidden_layer)+2)
                # x_label_train=torch.from_numpy(x_label_train).type(torch.cuda.LongTensor)
                # print("ypred\n",ypred)
                # print("xlabeltrain\n",x_label_train.shape)
                # loss=criterion(ypred,x_label_batch.unsqueeze(1).detach())
                loss.backward()
                optimizer.step()
                batch_loss.append(loss.item())
                current_batch+=1
            losses.append(np.mean(batch_loss))
            if (i+1)%10==0: print("Epoch: ",i+1,"/",epochs," Current Training Loss: ",losses[i])
            if i+1>5 and abs(losses[-1])<1e-3:
                print("Terminiated at Epoch:",i+1,"/",epochs," Current Training Loss: ",losses[i])
                break

        train_time[train,idx]=time.time()-inter_time
        exec("network_"+dm+".eval()")
        # print("predict oncesi")
        label_predict=np.zeros((data_label_train.shape[0],n_par))
        print(label_predict.shape,data_label_train.shape)

        gpu_batch=100
        current_batch=0
        gpu_epoch=np.ceil(label_predict.shape[0]/gpu_batch)
        while current_batch<gpu_epoch:
            # if current_batch%10 or current_batch==gpu_epoch-1:
            #     print("gpu epoch: ",current_batch,"/",gpu_epoch)
            if current_batch<label_predict.shape[0]//gpu_batch:
                batch_predict=eval("network_"+dm).predict(data_train[current_batch*gpu_batch:(current_batch+1)*gpu_batch,:],len(hidden_layer)+2)
                label_predict[current_batch*gpu_batch:(current_batch+1)*gpu_batch,:]=batch_predict.cpu().numpy()
            else:
                batch_predict=eval("network_"+dm).predict(data_train[current_batch*gpu_batch:,:],len(hidden_layer)+2)
                label_predict[current_batch*gpu_batch:,:]=batch_predict.cpu().numpy()
            current_batch+=1

        data_label_train=data_label_train.cpu()
        data_label_train=data_label_train.detach().numpy()
        # # print(label_predict)
        mpe_1=np.mean(abs(eval("paramscaler_"+dm).inverse_transform(label_predict)-eval("paramscaler_"+dm).inverse_transform\
            (data_label_train))/eval("paramscaler_"+dm).inverse_transform(data_label_train)*100,axis=0)
        # print("New pytorch network mserror: ",mean_squared_error(paramscaler.inverse_transform(data_label_train),paramscaler.inverse_transform(label_predict)))
        print("Pytorch network rsquared value: ",r2_score(eval("paramscaler_"+dm).inverse_transform(data_label_train),eval("paramscaler_"+dm).inverse_transform(label_predict)))
        print('Pytorch Mean percentage error: ',np.round(mpe_1,decimals=2),'%')
        # print("New pytorch network mserror: ",mean_squared_error(data_label_train,label_predict))
        # print("New pytorch network rsquared value: ",r2_score(data_label_train,label_predict))
        # print("New pytorch network training accuracy: ",accuracy_score(label_predict,x_label_train))

        train_time[train,idx]=time.time()-inter_time
        print('Neural network trained in ',train_time[train,idx],'s.')
        training_loss.append(losses)
        del data_train

        if double_mlp:
            no_inps=n_par
            if inv_tr:
                label_predict=eval("paramscaler_"+dm).inverse_transform(label_predict)
                data_label_train=eval("paramscaler_"+dm).inverse_transform(data_label_train)
            if add_z:
                f=open('extra_training_data.pckl','rb')
                extra_train=pickle.load(f)
                f.close
                if preprocess=="pca":
                    exec("ipca3_"+dm+"=IncrementalPCA(n_components=n_pca[idx],copy=False,batch_size=500)")
                elif preprocess=="ica":
                    exec("ipca3_"+dm+"=FastICA(n_components=n_pca[idx],max_iter=1000)")
                exec("ipca3_"+dm+".fit(extra_train)")
                extra_train=eval("ipca3_"+dm).transform(extra_train)
                exec("extra_scaler_"+dm+"=StandardScaler(copy=False)")
                exec("extra_scaler_"+dm+".fit(extra_train)")
                extra_train=eval("extra_scaler_"+dm).transform(extra_train)
                no_inps=n_par+extra_train.shape[-1]
                label_predict=np.append(extra_train,label_predict,axis=1)
                del extra_train

            hidden_layer2=[u,u,u]
            exec("second_mlp_"+dm+"= reg.Regresor(hidden_layer2,no_inps,n_par,drop)")
            # second_mlp = reg.Regresor(hidden_layer2,no_inps,n_par,drop)
            exec("print(second_mlp_"+dm+")")
            exec("second_mlp_"+dm+".train()")
            exec("second_mlp_"+dm+"=second_mlp_"+dm+".cuda()")
            criterion = nn.MSELoss().cuda()
            optimizer = torch.optim.Adam(eval("second_mlp_"+dm).parameters(),lr=0.001)
            epochs = 150
            try:
                data_label_train=torch.from_numpy(data_label_train).type(torch.cuda.FloatTensor)
                data_label_train.requires_grad_()
            except TypeError:
                data_label_train=data_label_train.cuda()
                data_label_train.requires_grad_()
            
                # data process

            batch_size=250
            batch_number=np.ceil(len(data_label_train)/batch_size).astype(int)
            losses=[]
            print('Second Layer Neural network training started...')
            inter_time=time.time()
            for i in range(epochs):
                current_batch=0
                batch_loss=[]
                # print(i)
                while current_batch<batch_number:
                    exec("network_"+dm+".train()")            
                    optimizer.zero_grad()
                    if current_batch<len(data_label_train)//batch_size:
                        # print(current_batch*batch_size,(current_batch+1)*batch_size)
                        # x_train_batch=x_train_after_pca[current_batch*batch_size:(current_batch+1)*batch_size,:]
                        data_label_batch=data_label_train[current_batch*batch_size:(current_batch+1)*batch_size]
                        # ypred=network.forward_ReLU(x_train_after_pca[current_batch*batch_size:(current_batch+1)*batch_size,:],len(hidden_layer)+2)
                        loss=criterion(eval("second_mlp_"+dm).forward_ReLU(label_predict[current_batch*batch_size:\
                            (current_batch+1)*batch_size,:],len(hidden_layer2)+2),\
                                data_label_batch.detach())
                        #hala memory sorunu yasarsan buraya bak, ypred ve x_label_batch'i de
                        # elimine edebilirsin!!!!!!!
                    else:
                        # print(current_batch*batch_size)
                        # x_train_batch=x_train_after_pca[current_batch*batch_size:,:]
                        data_label_batch=data_label_train[current_batch*batch_size:]
                        # ypred=network.forward_ReLU(x_train_after_pca[current_batch*batch_size:,:],len(hidden_layer)+2)
                        loss=criterion(eval("second_mlp_"+dm).forward_ReLU(label_predict[current_batch*batch_size:,:]\
                            ,len(hidden_layer2)+2),data_label_batch.detach())

                    # ypred=network.forward_ReLU(x_train_batch,len(hidden_layer)+2)
                    # x_label_train=torch.from_numpy(x_label_train).type(torch.cuda.LongTensor)
                    # print("ypred\n",ypred)
                    # print("xlabeltrain\n",x_label_train.shape)
                    # loss=criterion(ypred,x_label_batch.unsqueeze(1).detach())
                    loss.backward()
                    optimizer.step()
                    batch_loss.append(loss.item())
                    current_batch+=1
                losses.append(np.mean(batch_loss))
                del batch_loss,data_label_batch
                if (i+1)%10==0: print("Epoch: ",i+1,"/",epochs," Current Training Loss: ",losses[i])
                if i+1>5 and abs(losses[-1])<1e-3:
                    print("Terminiated at Epoch:",i+1,"/",epochs," Current Training Loss: ",losses[i])
                    break
            del losses
            print("second layer mlp trained in ",np.round(time.time()-inter_time,decimals=3),"s")
            exec("second_mlp_"+dm+".eval()")
            label_second_predict=np.zeros(data_label_train.shape)
            gpu_batch=100
            current_batch=0
            gpu_epoch=np.ceil(label_predict.shape[0]/gpu_batch)
            while current_batch<gpu_epoch:
                # if current_batch%10 or current_batch==gpu_epoch-1:
                #     print("gpu epoch: ",current_batch,"/",gpu_epoch)
                if current_batch<label_predict.shape[0]//gpu_batch:
                    batch_predict=eval("second_mlp_"+dm).predict(label_predict[current_batch*gpu_batch:(current_batch+1)*gpu_batch,:],len(hidden_layer2)+2)
                    label_second_predict[current_batch*gpu_batch:(current_batch+1)*gpu_batch,:]=batch_predict.cpu().numpy()
                else:
                    batch_predict=eval("second_mlp_"+dm).predict(data_train[current_batch*gpu_batch:,:],len(hidden_layer2)+2)
                    label_second_predict[current_batch*gpu_batch:,:]=batch_predict.cpu().numpy()
                current_batch+=1
                del batch_predict
            # for iiii in range(n_par):
            #     coefname="coef"+str(iiii)
            #     # exec(coefname+"=max(params[:,"+str(idx)+"])")
            #     exec("label_predict[:,"+str(iiii)+"]=label_predict[:,"+str(iiii)+"]*"+coefname)

            # label_predict=network.predict(data_train,len(hidden_layer)+2)
            # print("predict sonrasi")
            # # print(label_predict.shape)
            # label_predict=label_predict.cpu()
            # label_predict=label_predict.numpy()
            data_label_train=data_label_train.cpu()
            data_label_train=data_label_train.detach().numpy()
            if inv_tr:
                data_label_train=eval("paramscaler_"+dm).transform(data_label_train)
                label_second_predict=eval("paramscaler_"+dm).transform(label_second_predict)
            mpe_2=np.mean(abs(eval("paramscaler_"+dm).inverse_transform(label_second_predict)-\
                eval("paramscaler_"+dm).inverse_transform(data_label_train))/eval("paramscaler_"+dm).inverse_transform(data_label_train)*100,axis=0)
            print("Second Layer network mserror: ",mean_squared_error(eval("paramscaler_"+dm).inverse_transform(data_label_train),eval("paramscaler_"+dm).inverse_transform(label_second_predict)))
            print("Second Layer network rsquared value: ",r2_score(eval("paramscaler_"+dm).inverse_transform(data_label_train),eval("paramscaler_"+dm+"").inverse_transform(label_second_predict)))
            print('Second Layer Mean percentage error: ',np.round(mpe_2,decimals=2),'%')
            exec("second_mlp_"+dm+".eval()")
        
        del data_label_train,label_predict,label_second_predict
        exec("network_"+dm+".eval()")
        # print("predict oncesi")
        # label_predict=network.predict(x_train_after_pca,len(hidden_layer)+2)

        network_name=''
        for iii in range(len(hidden_layer)):
            network_name=network_name+str(hidden_layer[iii])+'_'
        network_name=layer_info=network_name[:-1]

        set_name="network_set"+dm+".pth"
        if add_z:
            torch.save({
                'par_scaler':eval("paramscaler_"+dm),
                'x_scaler':eval("scalerx_"+dm),
                'y_scaler':eval("scalery_"+dm),
                'z_scaler':eval("extra_scaler_"+dm),
                'x_pca':eval("ipcax_"+dm),
                'y_pca':eval("ipcay_"+dm),
                'z_pca':eval("ipca3_"+dm),
                'first_layer':eval("network_"+dm),
                'second_layer':eval("second_mlp_"+dm),
            },set_name)
            exec("del paramscaler_"+dm+", scalerx_"+dm+", scalery_"+dm+", extra_scaler_"+dm+", ipcax_"+dm+", ipcay_"+dm+", ipca3_"+dm)
            exec("del network_"+dm+",second_mlp_"+dm)
        else:
            torch.save({
                'par_scaler':eval("paramscaler_"+dm),
                'x_scaler':eval("scalerx_"+dm),
                'y_scaler':eval("scalery_"+dm),
                'x_pca':eval("ipcax_"+dm),
                'y_pca':eval("ipcay_"+dm),
                'first_layer':eval("network_"+dm),
                'second_layer':eval("second_mlp_"+dm),
            },set_name)

        # network_name='MLPClassifier_pytorch_pcasearch_10instance_'+splitstatement+'_train'+train_sets[train]+'s_pc'+str(n_pca[idx])+'_'+\
        #     str(len(hidden_layer))+'layers_'+network_name+'.pth'
        # if split:
        #     torch.save({
        #         'network_state_dict':network.state_dict(),
        #         'optimizer_state_dict':optimizer.state_dict(),
        #         'network':network,
        #         'optimizer':optimizer,
        #         'epoch':epochs,
        #         'paramscaler':paramscaler,
        #         'batch_size':batch_size,
        #         'pcax':ipcax,
        #         'pcay':ipcay,
        #         # 'scaler':scaler,
        #         'hidden_layer':hidden_layer,
        #         'N_p':N_p,
        #         'split':split,
        #         'instances':instances
        #     },network_name)
        # else:
        #     torch.save({
        #         'network_state_dict':network.state_dict(),
        #         'optimizer_state_dict':optimizer.state_dict(),
        #         'network':network,
        #         'optimizer':optimizer,
        #         'epoch':epochs,
        #         'paramscaler':paramscaler,
        #         'batch_size':batch_size,
        #         'pca':ipca,
        #         'scaler':scaler,
        #         'hidden_layer':hidden_layer,
        #         'N_p':N_p,
        #         'split':split,
        #         'instances':instances
        #     },network_name)      

    test_params=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Parameters_type'+str(typeind))['CA_Gelsdata']
    test_params=test_params[:,:n_par].reshape(-1,n_par)

    trig=0
    for test in range(len(test_sets)):
        for iii in range(len(test_sets[test])):
            ensemble_predicted=np.zeros(test_params.shape)
            for set_idx in range(len(sets)):
                # print("test loop#:",iii)
                matnamex="secondary_mat_contents_x"
                matnamey="secondary_mat_contents_y"
                
                exec(matnamex+"=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_"+pri_1+test_sets[test][iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                exec(matnamey+"=sio.loadmat\
                ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_"+pri_2+test_sets[test][iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")

                test_x=np.zeros((secondary_mat_contents_x.shape[0],secondary_mat_contents_x[0][0].shape[1]))
                test_y=np.zeros((secondary_mat_contents_y.shape[0],secondary_mat_contents_y[0][0].shape[1]))
                for ii in range(len(secondary_mat_contents_x)):test_x[ii]=secondary_mat_contents_x[ii][0]
                del secondary_mat_contents_x
                for ii in range(len(secondary_mat_contents_y)):test_y[ii]=secondary_mat_contents_y[ii][0]
                del secondary_mat_contents_y
                

                data_test=np.append(test_x,test_y,axis=1)
                # data_test=np.append(data_test,test_z,axis=1)

                # label_test_all=np.append(test_label_0,test_label_1,axis=0)
                test_all_labeled=np.append(data_test,test_params,axis=1)
                del test_x,test_y,data_test
                # del test_z

                # np.random.shuffle(test_all_labeled)
                label_test_shuffled=test_all_labeled[:,-n_par:]
                data_test_shuffled=test_all_labeled[:,:-n_par]
                del test_all_labeled

                secondary_test_label=label_test_shuffled.astype(float)
                del label_test_shuffled

                secondary_test_data=data_test_shuffled.astype(float)
                # secondary_test_data=scaler.transform(secondary_test_data)
                del data_test_shuffled
                
                if split:
                    package=torch.load('network_set'+sets[set_idx]+'.pth')
                    scalerx=package['x_scaler']
                    scalery=package['y_scaler']
                    extra_scaler=package['z_scaler']
                    par_scaler=package['par_scaler']
                    ipcax=package['x_pca']
                    ipcay=package['y_pca']
                    ipca3=package['z_pca']
                    network=package['first_layer']
                    second_mlp=package['second_layer']
                    del package
                    # secondary_test_data=np.append(scalerx.transform(secondary_test_data[:,split_len:]),\
                    #     scalery.transform(secondary_test_data[:,:split_len]),axis=1)
                    secondary_test_data_after_pca=ipcax.transform(secondary_test_data[:,:split_len])
                    secondary_test_data_after_pcay=ipcay.transform(secondary_test_data[:,split_len:])
                    del secondary_test_data
                    secondary_test_data_after_pca=np.append(scalerx.transform(secondary_test_data_after_pca),\
                        scalery.transform(secondary_test_data_after_pcay),axis=1)
                    del secondary_test_data_after_pcay
                else:
                    # buraya bakarsin yine, biraz duzeltme istiyor
                    package=torch.load('network_set'+sets[set_idx]+'.pth')
                    scalerx=package['x_scaler']
                    scalery=package['y_scaler']
                    ipcax=package['x_pca']
                    ipcay=package['y_pca']
                    network=package['first_layer']
                    second_mlp=package['second_layer']
                    del package
                    secondary_test_data_after_pca = ipca.transform(secondary_test_data)
                # secondary_test_data_after_pca=secondary_test_data_after_pca
                # secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
                secondary_label_predicted=network.predict(secondary_test_data_after_pca,len(hidden_layer)+2)
                del secondary_test_data_after_pca
                secondary_label_predicted=secondary_label_predicted.cpu()
                secondary_label_predicted=secondary_label_predicted.numpy()
                if double_mlp:
                    if inv_tr:
                        secondary_label_predicted=paramscaler.inverse_transform(secondary_label_predicted)
                    if add_z:
                        matname3="secondary_mat_contents_3"
                        exec(matname3+"=sio.loadmat\
                        ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_"+sec_1+test_sets[test][iii]+"_type"+str(typeind)+"')['CA_Gelsdata']")
                        test_3=np.zeros((secondary_mat_contents_3.shape[0],secondary_mat_contents_3[0][0].shape[1]))
                        for ii in range(len(secondary_mat_contents_3)):test_3[ii]=secondary_mat_contents_3[ii][0]
                        del secondary_mat_contents_3
                        test_3=ipca3.transform(test_3)
                        test_3=extra_scaler.transform(test_3)
                        secondary_label_predicted=np.append(test_3,secondary_label_predicted,axis=1)
                        del test_3
                    secondary_label_predicted=second_mlp.predict(secondary_label_predicted,len(hidden_layer2)+2)
                    secondary_label_predicted=secondary_label_predicted.cpu()
                    secondary_label_predicted=secondary_label_predicted.numpy()
                # secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
                #recording order: train set#, test set#, #pca, instance # within the test set
                if inv_tr:
                    pass
                else:
                    secondary_label_predicted=par_scaler.inverse_transform(secondary_label_predicted)
                # ensemble_name=
                # ensemble sonuclarini denemeye gecmeden once iki networku de train edip butun datadan 
                # bilgisayari temizleyip oyle testinge gecmeyi ayarla bugun
                # torch.save({
                #     ''
                # })
                ensemble_predicted=ensemble_predicted+secondary_label_predicted/len(sets)


            # for iiii in range(n_par):
            #     coefname="coef"+str(iiii)
            #     # exec(coefname+"=max(params[:,"+str(idx)+"])")
            #     exec("secondary_label_predicted[:,"+str(iiii)+"]=secondary_label_predicted[:,"+str(iiii)+"]*"+coefname)
            # secondary_label_predicted[:,0]=secondary_label_predicted[:,0]*coef0

            rsq_score[train,test,idx,iii]=r2_score(secondary_test_label,secondary_label_predicted)
            ms_error[train,test,idx,iii]=mean_squared_error(secondary_test_label,secondary_label_predicted)
            mean_percent_error[train,test,idx,iii,:]=np.mean(abs(secondary_test_label-ensemble_predicted)/secondary_test_label*100,axis=0)
            relative_mse[train,test,idx,iii,:]= np.mean((secondary_test_label-secondary_label_predicted)**2,axis=0)/np.mean(secondary_test_label**2,axis=0)
            print('Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
                +train_sets[-1][0]+'.'+train_sets[-1][1]+'s, Test t='+test_sets[test][iii][0]\
                    +'.'+test_sets[test][iii][1]+'s completed.')
            print('* Accuracy of the neural network: ',rsq_score[train,test,idx,iii])
            print('* Mean percentage error: ',np.round(mean_percent_error[train,test,idx,iii,:],decimals=2),'%')
            print('* Relative MSE: ',np.round(relative_mse[train,test,idx,iii,:],decimals=4))
            # plt.figure()
            # plt.plot(np.linspace(0,0.25,10),np.linspace(0,0.25,10))
            # plt.scatter(secondary_test_label,secondary_label_predicted)
            # plt.xlabel('actual label')
            # plt.ylabel('predicted label')
            # plt.show()
            del secondary_label_predicted ,secondary_test_label
                
                # for "kisaltma icin" in range(31):
                    # all_arrname="x_train_all_p"+str(iii+1)
                    # all_labelname="label_train_all_p"+str(iii+1)
                    # all_labeledname="x_train_all_labeled_p"+str(iii+1)
                    # exec(all_arrname+"=np.append("+arrname0+","+arrname1+",axis=0)")
                    # exec(all_labelname+"=np.append("+labelname0+","+labelname1+",axis=0)")
                    # exec(all_labeledname+"=np.append("+all_arrname+","+all_labelname+",axis=1")
                    # exec("np.random.shuffle("+all_labeledname+")")
                    # shuffled_labelname="x_label_shuffled_p"+str(iii+1)
                    # shuffled_dataname="x_train_shuffled_p"+str(iii+1)
                    # exec(shuffled_labelname+"="+all_labeledname+"[:,-1]")
                    # exec(shuffled_dataname+"="+all_labeledname+"[:,0:-1]")
                    # exec(shuffled_labelname+"="+shuffled_labelname+".astype(int)")
                    # exec(shuffled_dataname+"="+shuffled_dataname+".astype(float)")

                    # training_data="x_train_p"+str(iii+1)
                    # testing_data="x_test_p"+str(iii+1)
                    # training_label="x_label_train_p"+str(iii+1)
                    # testing_label="x_label_test_p"+str(iii+1)
                    # exec(training_data+","+testing_data+","+training_label+","+testing_label+"train_test_split("+shuffled_dataname+","+shuffled_labelname+",test_size=0.50)")

                    # secondary_mat_contents_0 =\
                    #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type1')\
                    #         ['CA_Gelsdata']
                    # secondary_mat_contents_1 =\
                    #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type2')\
                    #         ['CA_Gelsdata']


                    # #Extract the data and create labels etc.#
                    # secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
                    # secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
                    # secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
                    # secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
                    # #Extract the x points from the cells to a big array
                    # for ii in range(len(secondary_mat_contents_0)):secondary_test_data_0[ii]=secondary_mat_contents_0[ii][0]
                    #     # if ii<len(secondary_mat_contents_1):
                    # for ii in range(len(secondary_mat_contents_1)):secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]
                    # secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
                    # secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
                    # secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

                    # #Shuffle the labeled data to get a "neutral" dataset#
                    # np.random.shuffle(secondary_test_shuffled)
                    # #Separate the labels and data part of the shuffled dataset#
                    # secondary_test_label=secondary_test_shuffled[:,-1]
                    # secondary_test_data=secondary_test_shuffled[:,0:-1]
                    # secondary_test_label=secondary_test_label.astype(int)
                    # secondary_test_data=secondary_test_data.astype(float)
            
                    # secondary_test_data_after_pca = pca.transform(secondary_test_data)
                    # secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]

                    # secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
                    # secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
                    # accuracy_pca[train,idx,test]=accuracy_score(secondary_test_label,secondary_label_predicted)
            if split:
                del scalerx,scalery,extra_scaler,par_scaler,ipcax,ipcay,ipca3,network,second_mlp
            else:
                del scalerx,scalery,ipcax,ipcay,network,second_mlp
            trig=trig+1
    del test_params


print('Execution of the whole code completed. Total runtime: ',time.time()-start_time,'s')
save_name='PCA_pytorch_pcasearch_10instance_'+preprocess_statement+splitstatement+double_statement+z_statement+drop_statement+"p05_"+str(len(hidden_layer))+'layers_'+\
    layer_info+'.pckl'
save=open(save_name,'wb')
train_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70']]
test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90']]
# test_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70'],['141','142','143','144','145','146','147','148','149','150']]
pickle.dump([train_sets,test_sets,n_pca,hidden_layer,n_par,train_time,ms_error,rsq_score,mean_percent_error,relative_mse,training_loss,instances],save)
save.close



line_colors=['r-o','b-o','g-o','c-o','m-o','k-o']
fig,axs=plt.subplots(len(dms),len(test_dms),sharex=True,sharey=True)
fig.suptitle('Mean Square Error for Different Number of PCs')

for train in range(len(dms)):
    for test in range(len(test_dms)):
        for pcs in range(len(n_pca)):
            axs[test].plot(ms_error[train,test,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))
        axs[test].set_xticklabels([])
# axs[0][0].set_ylabel('7-8s Str')
axs[0].set_ylabel('6-7s Str')
# axs[2][2].set_xlabel('5th DM')
axs[0].set_xlabel('7-8s Rand')
axs[1].set_xlabel('8-9s Rand')
# axs[1][2].set_xlabel('14-15s Rand')
# axs[2][0].set_ylabel('5th DM')
handles,labels=axs[1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')

fig,axs=plt.subplots(len(dms),len(test_dms),sharex=True,sharey=True)
fig.suptitle('$R^2$ Score for Different Number of PCs')
for train in range(len(dms)):
    for test in range(len(test_dms)):
        for pcs in range(len(n_pca)):
            axs[test].plot(rsq_score[train,test,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))
        axs[test].set_xticklabels([])
# axs[0][0].set_ylabel('7-8s Str')
axs[0].set_ylabel('6-7s Str')
# axs[2][2].set_xlabel('5th DM')
axs[0].set_xlabel('7-8s Rand')
axs[1].set_xlabel('8-9s Rand')
# axs[1][2].set_xlabel('14-15s Rand')
# axs[2][0].set_ylabel('5th DM')
handles,labels=axs[1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')

# plt.xticks([])

for idx in range(n_par):
    fig,axs=plt.subplots(len(dms),len(test_dms),sharex=True,sharey=True)
    fig.suptitle('Mean Percentage error for different number of PCs, Parameter '+str(idx+1))
    for train in range(len(dms)):
        for test in range(len(test_dms)):
            for pcs in range(len(n_pca)):
                axs[test].plot(mean_percent_error[train,test,pcs,:,idx],line_colors[pcs],label='PC='+str(n_pca[pcs]))
            axs[test].set_xticklabels([])
    # axs[0][0].set_ylabel('7-8s Str')
    axs[0].set_ylabel('6-7s Str')
    # axs[2][2].set_xlabel('5th DM')
    axs[0].set_xlabel('7-8s Rand')
    axs[1].set_xlabel('8-9s Rand')
    # axs[1][2].set_xlabel('14-15s Rand')
    # axs[2][0].set_ylabel('5th DM')
    handles,labels=axs[1].get_legend_handles_labels()
    fig.legend(handles,labels,loc='lower right')

# plt.xticks([])
plt.show()
                    

'''

#second test set #
secondary_mat_contents_0 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type1')['CA_Gelsdata']
secondary_mat_contents_1 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type2')['CA_Gelsdata']


#create label vectors for the data
secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
print(secondary_test_data_0.shape)

#Extract the x points from the cells to a big array
for idx in range(len(secondary_mat_contents_0)):
    secondary_test_data_0[idx]=secondary_mat_contents_0[idx][0]
    if idx<len(secondary_mat_contents_1):
# for idx in range(len(mat_contents_t1_p1)):
        secondary_test_data_1[idx]=secondary_mat_contents_1[idx][0]

secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

#Shuffle the labeled data to get a "neutral" dataset#
np.random.shuffle(secondary_test_shuffled)
#Separate the labels and data part of the shuffled dataset#
secondary_test_label=secondary_test_shuffled[:,-1]
secondary_test_data=secondary_test_shuffled[:,0:-1]
secondary_test_label=secondary_test_label.astype(int)
secondary_test_data=secondary_test_data.astype(float)



N_p_variance=1
N_p=30 #number of principal components used

#create the hidden layer architecture#
u=30
# v=30
# w=30

hidden_layer = [u]
x_train_after_pca=x_train_pca[:,0:N_p]
x_test_after_pca=x_test_pca[:,0:N_p]
pca_score_sum=sum(pca_score[0:N_p])

scaler=StandardScaler()
scaler.fit(x_train_after_pca)
# scaler.fit(x_test_after_pca)
x_train_after_pca=scaler.transform(x_train_after_pca)
x_test_after_pca=scaler.transform(x_test_after_pca)

print('\n\nTrain t=6.1s, Test t=6.1s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                    max_iter = 1000)
mlp.fit(x_train_after_pca,x_label_train)
x_label_predicted=mlp.predict(x_test_after_pca)

# the count of true negatives is C_0,0, false negatives is C_1,0, 
# true positives is C_1,1 and false positives is C_0,1.
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))

x_train_shuffled_62_pca=pca.transform(secondary_test_data)
pca_score = pca.explained_variance_ratio_
pca_score_sum=sum(pca_score[0:N_p])
x_train_shuffled_62_pca=scaler.transform(x_train_shuffled_62_pca)
print('\n\nTrain t=6.1s, Test t=6.2s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

x_label_shuffled_pred_62=mlp.predict(x_train_shuffled_62_pca)
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(secondary_test_label,x_label_shuffled_pred_62))                
print('* Classification report\n',classification_report(secondary_test_label,x_label_shuffled_pred_62))
print('* Accuracy of the neural network: ',accuracy_score(secondary_test_label,x_label_shuffled_pred_62))
# print(precision_recall)

'''