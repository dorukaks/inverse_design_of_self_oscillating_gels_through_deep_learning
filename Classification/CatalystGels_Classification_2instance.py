"""
Classification trial 
Changelog:
v0 Created on Sat Jan 25 2020 15:23
v1 Created on Sun Jan 26 2020
    Changes: instead of computing the accuracy of one training set at the test dataset
             split from the training set, the code scans over the given spectrum of data.
v1.1 Created on Mon Jan 27 2020
    Changes: Plotting function fixed.
v1.2 Created on Thur Jan 30 2020 13:00
    Changes: Modified the classification training setup so that (not timewise 
             neighboring) sets of data are used for training.
Author: Doruk
"""
#This python file is created to do the type classification of the simulation
#results based on the previous classifiers (on 1st dataset) written by DK

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.cm as cm
#import pandas as pd

from sklearn.pipeline import Pipeline
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.font_manager import FontProperties
from sklearn.decomposition import PCA
#form sklearn.decomposition import FastICA, KernelPCA, NMF
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, silhouette_samples

from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor, MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score


train_sets=['61','62','63','64','65','66','67','68','69','70']
# ['61','62','63','64','65','66','67','68','69','70']
test_sets=['61','62','63','64','65','66','67','68','69','70']
# n_pca=np.array([10,30])
# n_pca=np.array([10,30,100,1000,3367])
n_pca=np.array([10,20,30,40,50])
shift=len(train_sets)//2 #make the training set 0.5s apart
accuracy_pca=np.zeros((shift,len(n_pca),len(test_sets)))
pca_scores=np.zeros((shift,len(n_pca)))


for train in range(len(train_sets)//2):
    print('Iteration for train t='+train_sets[train][0]+'.'+train_sets[train][1]+'s and '+train_sets[train+shift][0]+'.'+train_sets[train+shift][1]+'s, has started.')


    #Load files and extract the cell structures from files

    mat_contents_t0_p1=sio.loadmat\
        ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type1')['CA_Gelsdata']
    mat_contents_t1_p1=sio.loadmat\
        ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type2')['CA_Gelsdata']
    mat_contents_t0_p2=sio.loadmat\
        ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+shift]+'_type1')['CA_Gelsdata']
    mat_contents_t1_p2=sio.loadmat\
        ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+shift]+'_type2')['CA_Gelsdata']
    # print(mat_contents_t1_p1.shape,mat_contents_t1_p1[0].shape,mat_contents_t1_p1[0][0].shape)

    #create label vectors for the data
    train_label_0_p1=np.zeros(mat_contents_t0_p1.shape)
    train_label_1_p1=np.ones(mat_contents_t1_p1.shape)
    train_label_0_p2=np.zeros(mat_contents_t0_p2.shape)
    train_label_1_p2=np.ones(mat_contents_t1_p2.shape)
    x_train_0_p1=np.zeros((mat_contents_t0_p1.shape[0],mat_contents_t0_p1[0][0].shape[1]))
    x_train_1_p1=np.zeros((mat_contents_t1_p1.shape[0],mat_contents_t1_p1[0][0].shape[1]))
    x_train_0_p2=np.zeros((mat_contents_t0_p2.shape[0],mat_contents_t0_p2[0][0].shape[1]))
    x_train_1_p2=np.zeros((mat_contents_t1_p2.shape[0],mat_contents_t1_p2[0][0].shape[1]))

    # print(x_train_0_p1.shape)

    #Extract the x points from the cells to a big array
    for idx in range(len(mat_contents_t0_p1)):
        x_train_0_p1[idx]=mat_contents_t0_p1[idx][0]
    for idx in range(len(mat_contents_t0_p2)):
        x_train_0_p2[idx]=mat_contents_t0_p2[idx][0]
        # if idx<len(mat_contents_t1_p1):
    for idx in range(len(mat_contents_t1_p1)):
            x_train_1_p1[idx]=mat_contents_t1_p1[idx][0]
    for idx in range(len(mat_contents_t1_p2)):
            x_train_1_p2[idx]=mat_contents_t1_p2[idx][0]

    #Here we have the x positions of the 8000+6400 simulations at t=6.1s (type1 and
    # type2) extracted from the .mat file. Then the next step is to reduce the dimension
    #of this huge sized matrix by applying principle component analysis (PCA)

    x_train_all_p1=np.append(x_train_0_p1,x_train_1_p1,axis=0)
    x_train_all_p2=np.append(x_train_0_p2,x_train_1_p2,axis=0)
    label_train_all_p1=np.append(train_label_0_p1,train_label_1_p1,axis=0)
    label_train_all_p2=np.append(train_label_0_p2,train_label_1_p2,axis=0)
    x_train_all_labeled_p1=np.append(x_train_all_p1,label_train_all_p1,axis=1)
    x_train_all_labeled_p2=np.append(x_train_all_p2,label_train_all_p2,axis=1)

    #Shuffle the labeled data to get a "neutral" dataset#
    np.random.shuffle(x_train_all_labeled_p1)
    np.random.shuffle(x_train_all_labeled_p2)
    #Separate the labels and data part of the shuffled dataset#
    x_label_shuffled_p1=x_train_all_labeled_p1[:,-1]
    x_label_shuffled_p2=x_train_all_labeled_p2[:,-1]
    x_train_shuffled_p1=x_train_all_labeled_p1[:,0:-1]
    x_train_shuffled_p2=x_train_all_labeled_p2[:,0:-1]
    x_label_shuffled_p1=x_label_shuffled_p1.astype(int)
    x_label_shuffled_p2=x_label_shuffled_p2.astype(int)
    x_train_shuffled_p1=x_train_shuffled_p1.astype(float)
    x_train_shuffled_p2=x_train_shuffled_p2.astype(float)
    # print('Dimension of the training data:',x_train_all_p1.shape)

    #divide the set as training/test set#
    x_train_p1 , x_test_p1 , x_label_train_p1 ,x_label_test_p1 = \
        train_test_split(x_train_shuffled_p1,x_label_shuffled_p1,test_size=0.40)
    x_train_p2 , x_test_p2 , x_label_train_p2 ,x_label_test_p2 = \
        train_test_split(x_train_shuffled_p2,x_label_shuffled_p2,test_size=0.40)
    
    x_train=np.append(x_train_p1,x_train_p2,axis=0)
    x_label=np.append(x_label_train_p1,x_label_train_p2,axis=0)
    x_label=np.reshape(x_label,(x_train.shape[0],1))
    print('xtrain shape:',x_train.shape)
    print('xlabel shape:',x_label.shape)
    x_train_all=np.append(x_train,x_label,axis=1)
    np.random.shuffle(x_train_all)
    x_train=x_train_all[:,0:-1]
    x_label=x_train_all[:,-1]


    a,b = x_train.shape
    print('Size of the combined training set: ',a,b)
#buraya pca loop'u gelecek bu sayede en dista train loopu sonra icerde pca loopu en icte de 
# test loopu olacak sekilde butun simulasyon kosacak#
    for idx in range(len(n_pca)):
        print('Iteration with N_PCA:',n_pca[idx],' has started.')
        pca=PCA(n_components = n_pca[idx])
        pca.fit(x_train)
        x_train_pca=pca.transform(x_train)
        pca_score = pca.explained_variance_ratio_
        x_test_pca_p1 = pca.transform(x_test_p1)
        x_test_pca_p2 = pca.transform(x_test_p2)



        N_p_variance=1
        N_p=n_pca[idx]#number of principal components used

        #create the hidden layer architecture#
        u=20
        v=20
        w=20
        y=20
        hidden_layer = [u,v,w,y]

        x_train_after_pca=x_train_pca[:,0:N_p]
        x_test_after_pca_p1=x_test_pca_p1[:,0:N_p]
        x_test_after_pca_p2=x_test_pca_p2[:,0:N_p]
        pca_score_sum=sum(pca_score[0:N_p])
        pca_scores[train,idx]=pca_score_sum

        scaler=StandardScaler()
        scaler.fit(x_train_after_pca)
        # scaler.fit(x_test_after_pca)
        x_train_after_pca=scaler.transform(x_train_after_pca)
        x_test_after_pca_p1=scaler.transform(x_test_after_pca_p1)
        x_test_after_pca_p2=scaler.transform(x_test_after_pca_p2)


        # print('\n\nTrain t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
        #     +'.'+test_sets[test][1]+'s results:')

        # print('\n [MLP Classifier Result]')
        # print('\n* The number of PCs :', N_p, \
        #         '\n* Total PCA score :',pca_score_sum,\
        #         '\n* Hidden Layer :', hidden_layer)
        print('Neural network training started...')
        mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                            max_iter = 2000)
        mlp.fit(x_train_after_pca,x_label)
        print('Neural network trained.')

        # # the count of true negatives is C_0,0, false negatives is C_1,0, 
        # # true positives is C_1,1 and false positives is C_0,1.
        # print('\n\nResults of the trained neural network:')
        # print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
        # print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
        # print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))        




        for test in range(len(test_sets)):
            if test!=train or test!=train+shift:
                #call secondary test set#
                secondary_mat_contents_0 =\
                    sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type1')\
                        ['CA_Gelsdata']
                secondary_mat_contents_1 =\
                    sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type2')\
                        ['CA_Gelsdata']
                
                #Extract the data and create labels etc.#
                secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
                secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
                secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
                secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
                #Extract the x points from the cells to a big array
                for ii in range(len(secondary_mat_contents_0)):
                    secondary_test_data_0[ii]=secondary_mat_contents_0[ii][0]
                    if ii<len(secondary_mat_contents_1):
                # for ii in range(len(mat_contents_t1_p1)):
                        secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]
                secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
                secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
                secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

                #Shuffle the labeled data to get a "neutral" dataset#
                np.random.shuffle(secondary_test_shuffled)
                #Separate the labels and data part of the shuffled dataset#
                secondary_test_label=secondary_test_shuffled[:,-1]
                secondary_test_data=secondary_test_shuffled[:,0:-1]
                secondary_test_label=secondary_test_label.astype(int)
                secondary_test_data=secondary_test_data.astype(float)
        
                secondary_test_data_after_pca = pca.transform(secondary_test_data)
                secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]

                secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
                secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
                accuracy_pca[train,idx,test]=accuracy_score(secondary_test_label,secondary_label_predicted)

            elif test==train:
                
                x_label_predicted=mlp.predict(x_test_after_pca_p1)
                accuracy_pca[train,idx,test]=accuracy_score(x_label_test_p1,x_label_predicted)
            else:
                x_label_predicted=mlp.predict(x_test_after_pca_p2)
                accuracy_pca[train,idx,test]=accuracy_score(x_label_test_p2,x_label_predicted)
            print('Train t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
            +'.'+test_sets[test][1]+'s completed.\n Results:')
            print('* Accuracy of the neural network: ',accuracy_pca[train,idx,test])

fig,axs=plt.subplots(1,shift,sharey=True)
fig.suptitle('Classification Results With Different Number of PCs')
for train in range(shift):
    axs[train].plot(accuracy_pca[train,0,:],'r-o',label='PC=10')
    axs[train].plot(accuracy_pca[train,1,:],'b-o',label='PC=20')
    axs[train].plot(accuracy_pca[train,2,:],'g-o',label='PC=30')
    axs[train].plot(accuracy_pca[train,3,:],'c-o',label='PC=40')
    axs[train].plot(accuracy_pca[train,4,:],'m-o',label='PC=50')
    axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s & '+train_sets[train+shift][0]+'.'+train_sets[train+shift][1]+'s')
    axs[train].set_xticklabels([])

handles,labels=axs[shift-1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')
# plt.xticks([])
plt.show()
                    

'''

#second test set #
secondary_mat_contents_0 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type1')['CA_Gelsdata']
secondary_mat_contents_1 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type2')['CA_Gelsdata']


#create label vectors for the data
secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
print(secondary_test_data_0.shape)

#Extract the x points from the cells to a big array
for idx in range(len(secondary_mat_contents_0)):
    secondary_test_data_0[idx]=secondary_mat_contents_0[idx][0]
    if idx<len(secondary_mat_contents_1):
# for idx in range(len(mat_contents_t1_p1)):
        secondary_test_data_1[idx]=secondary_mat_contents_1[idx][0]

secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

#Shuffle the labeled data to get a "neutral" dataset#
np.random.shuffle(secondary_test_shuffled)
#Separate the labels and data part of the shuffled dataset#
secondary_test_label=secondary_test_shuffled[:,-1]
secondary_test_data=secondary_test_shuffled[:,0:-1]
secondary_test_label=secondary_test_label.astype(int)
secondary_test_data=secondary_test_data.astype(float)



N_p_variance=1
N_p=30 #number of principal components used

#create the hidden layer architecture#
u=30
# v=30
# w=30

hidden_layer = [u]
x_train_after_pca=x_train_pca[:,0:N_p]
x_test_after_pca=x_test_pca[:,0:N_p]
pca_score_sum=sum(pca_score[0:N_p])

scaler=StandardScaler()
scaler.fit(x_train_after_pca)
# scaler.fit(x_test_after_pca)
x_train_after_pca=scaler.transform(x_train_after_pca)
x_test_after_pca=scaler.transform(x_test_after_pca)

print('\n\nTrain t=6.1s, Test t=6.1s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                    max_iter = 1000)
mlp.fit(x_train_after_pca,x_label_train)
x_label_predicted=mlp.predict(x_test_after_pca)

# the count of true negatives is C_0,0, false negatives is C_1,0, 
# true positives is C_1,1 and false positives is C_0,1.
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))

x_train_shuffled_62_pca=pca.transform(secondary_test_data)
pca_score = pca.explained_variance_ratio_
pca_score_sum=sum(pca_score[0:N_p])
x_train_shuffled_62_pca=scaler.transform(x_train_shuffled_62_pca)
print('\n\nTrain t=6.1s, Test t=6.2s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

x_label_shuffled_pred_62=mlp.predict(x_train_shuffled_62_pca)
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(secondary_test_label,x_label_shuffled_pred_62))                
print('* Classification report\n',classification_report(secondary_test_label,x_label_shuffled_pred_62))
print('* Accuracy of the neural network: ',accuracy_score(secondary_test_label,x_label_shuffled_pred_62))
# print(precision_recall)

'''