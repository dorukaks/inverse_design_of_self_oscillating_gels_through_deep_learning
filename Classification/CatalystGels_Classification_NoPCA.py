"""
Classification trial 
v0 Created on Sat Jan 25 2020 15:23

Author: Doruk
"""
#This python file is created to do the type classification of the simulation
#results based on the previous classifiers (on 1st dataset) written by DK

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import time
import pickle
import joblib
import torch
import torch.nn as nn
import torch.nn.functional as F
#import matplotlib.cm as cm
#import pandas as pd

# from sklearn.pipeline import Pipeline
# from mpl_toolkits.mplot3d import Axes3D
# from matplotlib.font_manager import FontProperties
#form sklearn.decomposition import FastICA, KernelPCA, NMF
# from sklearn.cluster import KMeans
# from sklearn.metrics import silhouette_score, silhouette_samples

# from sklearn.neural_network import MLPRegressor, MLPClassifier
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score


class MLP_Classifier(nn.Module):
    def __init__(self,n_layers,n_pcs,n_units):
        self.device = torch.device("cuda:0")
        #The number of layers should be written including the input and output
        # layers. E.g: for a network with 1 hidden layer, n_layer should be 3#
        super(MLP_Classifier,self).__init__()
        n_units=np.array(n_units)
        if n_layers-2==1:
            self.fc1=nn.Linear(n_pcs,n_units[0])
            # self.activ1=nn,ReLU()
            self.fc2=nn.Linear(n_units[0],1)
            # self.relu=nn.ReLU()
            # self.out=nn.Softmax(dim=1)
        else:
            self.fc1=nn.Linear(n_pcs,n_units[0])
            # self.activ1=nn.ReLU()
            ctr=1
            name="self.fc"
            # activname="self.activ"
            while ctr+1<n_layers-1:
                exec(name+str(ctr+1)+"=nn.Linear("+str(n_units[ctr-1])+","+\
                    str(n_units[ctr])+")")
                # exec(activname+str(ctr+1)+"=nn.ReLU()")
                ctr+=1
            exec(name+str(n_layers-1)+"=nn.Linear("+str(n_units[-1])+",1)")
        # self.dropout=nn.Dropout(0.2)
            # self.out=nn.Softmax(dim=1)
            # self.relu=nn.ReLU()
    def forward_ReLU(self,x,n_layers):
        x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        x = self.fc1(x)
        ctr = 2
        while ctr<n_layers:
            name="self.fc"+str(ctr)
            # activname="self.activ"+str(ctr)
            # exec("x="+activname+"(x)")
            # exec("x="+name+"(x)")
            x=F.relu(x)
            x=eval(name+'(x)')
            ctr+=1
        # x=self.relu(x)
        x=torch.sigmoid(x)
        # ans=[]
        # for t in x:
        #     if t[0]>t[1]:
        #         ans.append(0)
        #     else:
        #         ans.append(1)
        # return torch.tensor(ans).type(torch.cuda.IntTensor)
        return x
    
    def predict(self,x,n_layers):
        # x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        pred= self.forward_ReLU(x,n_layers)
        pred=pred.cpu()
        pred=np.round(pred.detach())
        # ans=[]
        # for t in pred:
        #     if t[0]>t[1]:
        #         ans.append(0)
        #     else:
        #         ans.append(1)
        # return torch.tensor(ans).type(torch.cuda.FloatTensor)
        return pred

train_sets=['61','62','63','64','65','66','67','68','69','70']
# ['61','62','63','64','65','66','67','68','69','70']
# test_sets=['61','62','63','64','65','66','67','68','69','70']
# test_sets=['71','72','73','74','75','76','77','78','79','80']
test_sets=[['71','72','73','74','75','76','77','78','79','80'],['81','82','83','84','85','86','87','88','89','90']]
# n_pca=np.array([10,30])
# n_pca=np.array([10,30,100,1000,3367])
n_pca=np.array([10])
accuracy_nopca=np.zeros((len(train_sets),len(n_pca),len(test_sets),10))
pca_scores=np.zeros((len(train_sets),len(n_pca)))
train_time=np.zeros((len(train_sets),len(n_pca)))
training_loss=[]
instances=1


start_time=time.time()
for train in range(len(train_sets)):
    print('Iteration for train t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, has started.')


    #Load files and extract the cell structures from files
    #For Windows
    # mat_contents_t0=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type1')['CA_Gelsdata']
    # mat_contents_t1=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type2')['CA_Gelsdata']
    # print(mat_contents_t0.shape,mat_contents_t0[0].shape,mat_contents_t0[0][0].shape)
    
    #For Ubuntu
    mat_contents_t0=sio.loadmat\
        ('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'\
            +train_sets[train]+'_type1')['CA_Gelsdata']
    mat_contents_t1=sio.loadmat\
        ('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'\
            +train_sets[train]+'_type2')['CA_Gelsdata']

    mat_contents_t0=mat_contents_t0[:6400]
    mat_contents_t1=mat_contents_t1[:6400]

    #create label vectors for the data
    train_label_0=np.zeros(mat_contents_t0.shape)
    train_label_1=np.ones(mat_contents_t1.shape)
    x_train_0=np.zeros((mat_contents_t0.shape[0],mat_contents_t0[0][0].shape[1]))
    x_train_1=np.zeros((mat_contents_t1.shape[0],mat_contents_t1[0][0].shape[1]))
    # print(x_train_0.shape)

    #Extract the x points from the cells to a big array
    for idx in range(len(mat_contents_t0)): x_train_0[idx]=mat_contents_t0[idx][0]
        # if idx<len(mat_contents_t1):
    for idx in range(len(mat_contents_t1)): x_train_1[idx]=mat_contents_t1[idx][0]

    #Here we have the x positions of the 8000+6400 simulations at t=6.1s (type1 and
    # type2) extracted from the .mat file. Then the next step is to reduce the dimension
    #of this huge sized matrix by applying principle component analysis (PCA)

    x_train_all=np.append(x_train_0,x_train_1,axis=0)
    label_train_all=np.append(train_label_0,train_label_1,axis=0)
    x_train_all_labeled=np.append(x_train_all,label_train_all,axis=1)

    #Shuffle the labeled data to get a "neutral" dataset#
    np.random.shuffle(x_train_all_labeled)
    #Separate the labels and data part of the shuffled dataset#
    x_label_shuffled=x_train_all_labeled[:,-1]
    x_train_shuffled=x_train_all_labeled[:,0:-1]
    x_label_shuffled=x_label_shuffled.astype(int)
    x_train_shuffled=x_train_shuffled.astype(float)
    # print('Dimension of the training data:',x_train_all.shape)

    #divide the set as training/test set#
    # x_train , x_test , x_label_train ,x_label_test = \
    #     train_test_split(x_train_shuffled,x_label_shuffled,test_size=0.20)
    # This is not used since the training will be done on the structured grid set
    # and the testing is done on the randomized grid set#
    
    x_train=x_train_shuffled
    x_label_train=x_label_shuffled
    # Removing the unnecessary data from the memory for efficiency
    del x_label_shuffled,x_train_shuffled,x_train_all_labeled,\
        x_train_all,x_train_0,x_train_1,train_label_0,train_label_1,\
            mat_contents_t0,mat_contents_t1

    a,b = x_train.shape
#buraya pca loop'u gelecek bu sayede en dista train loopu sonra icerde pca loopu en icte de 
# test loopu olacak sekilde butun simulasyon kosacak#
    for idx in range(len(n_pca)):
        print('Iteration with No PCA has started.')
        # pca=PCA(n_components = n_pca[idx])
        # pca.fit(x_train)
        # x_train_pca=pca.transform(x_train)
        # pca_score = pca.explained_variance_ratio_
        # x_test_pca = pca.transform(x_test)



        N_p_variance=1
        N_p=n_pca[idx]#number of principal components used

        #create the hidden layer architecture#
        u=300
        v=300
        w=300
        y=30
        z=30
        hidden_layer = [u,v,w]

        # x_train_after_pca=x_train_pca[:,0:N_p]
        # x_test_after_pca=x_test_pca[:,0:N_p]
        # pca_score_sum=sum(pca_score[0:N_p])
        # pca_scores[train,idx]=pca_score_sum

        scaler=StandardScaler()
        scaler.fit(x_train)
        # scaler.fit(x_test_after_pca)
        x_train=scaler.transform(x_train)
        # x_test_after_pca=scaler.transform(x_test_after_pca)
        print("train shape\n",x_train.shape)


        # print('\n\nTrain t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
        #     +'.'+test_sets[test][1]+'s results:')

        # print('\n [MLP Classifier Result]')
        # print('\n* The number of PCs :', N_p, \
        #         '\n* Total PCA score :',pca_score_sum,\
        #         '\n* Hidden Layer :', hidden_layer)
        print('Neural network training started...')
        inter_time=time.time()

        network = MLP_Classifier(len(hidden_layer)+2,x_train.shape[-1],hidden_layer)
        print(network)
        network.train()
        criterion = nn.BCELoss().cuda()
        # criterion = nn.BCEWithLogitsLoss().cuda()
        optimizer = torch.optim.Adam(network.parameters(),lr=0.001)
        epochs = 20
        # training_loss=np.zeros((len(train_sets),len(n_pca),epochs))
        # training_loss_sizes=np.zeros((len(train_sets),len(n_pca)))
        # training_loss=np.array()
        losses=[]
        network=network.cuda()
        try:
            x_label_train=torch.from_numpy(x_label_train).type(torch.cuda.FloatTensor)
            x_label_train.requires_grad_()
        except TypeError:
            x_label_train=x_label_train.cuda()
            x_label_train.requires_grad_()

        batch_size=500 #26 batches
        batch_number=np.ceil(len(x_label_train)/batch_size).astype(int)
        print(batch_number," batches with ",batch_size," elements")
        
        for i in range(epochs):
            current_batch=0
            batch_loss=[]
            # print(i)
            while current_batch<batch_number:
                if current_batch<len(x_label_train)//batch_size:
                    # print(current_batch*batch_size,(current_batch+1)*batch_size)
                    x_train_batch=x_train[current_batch*batch_size:(current_batch+1)*batch_size,:]
                    x_label_batch=x_label_train[current_batch*batch_size:(current_batch+1)*batch_size]
                else:
                    # print(current_batch*batch_size)
                    x_train_batch=x_train[current_batch*batch_size:,:]
                    x_label_batch=x_label_train[current_batch*batch_size:]
                network.train()            
                optimizer.zero_grad()
                ypred=network.forward_ReLU(x_train_batch,len(hidden_layer)+2)
                # x_label_train=torch.from_numpy(x_label_train).type(torch.cuda.LongTensor)
                # print("ypred\n",ypred)
                # print("xlabeltrain\n",x_label_train.shape)
                loss=criterion(ypred,x_label_batch.unsqueeze(1).detach())
                loss.backward()
                optimizer.step()
                batch_loss.append(loss.item())
                current_batch+=1
            losses.append(np.mean(batch_loss))
            if (i+1)%1==0: print("Epoch: ",i+1,"/",epochs," Current Training Loss: ",losses[i])
            if i+1>5 and abs(losses[-1])<1e-3:
                print("Terminated at Epoch:",i+1,"/",epochs," Current Training Loss: ",losses[i])
                break
        training_loss.append(losses)
        # training_loss[train,idx,:len(losses)]=losses
        # training_loss_sizes[train,idx]=len(losses)
        train_time[train,idx]=time.time()-inter_time
        network.eval()
        label_predict=network.predict(x_train,len(hidden_layer)+2)
        # print(label_predict.shape)
        label_predict=label_predict.cpu()
        label_predict=label_predict.numpy()
        x_label_train=x_label_train.cpu()
        x_label_train=x_label_train.detach().numpy()
        # print(label_predict)
        print("New pytorch network training accuracy: ",accuracy_score(label_predict,x_label_train))
        print('Neural network trained in ',train_time[train,idx],'s.')

        def predict(x):
            x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
            ans=network.predict(x)
            return ans.numpy()

        # mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
        #                     max_iter = 2000)
        # mlp.fit(x_train_after_pca,x_label_train)
        # print('Neural network trained.')
        N_p=['No PC']
        pca=['No PC Transformation']
        network_name=''
        for iii in range(len(hidden_layer)):
            network_name=network_name+str(hidden_layer[iii])+'_'
        network_name=layer_info=network_name[:-1]
        network_name='MLPClassifier_pytorch_nopca_1instance_train'+train_sets[train]+'s_pc'+str(n_pca[idx])+'_'+\
            str(len(hidden_layer))+'layers_'+network_name+'.pth'
        torch.save({
            'network_state_dict':network.state_dict(),
            'optimizer_state_dict':optimizer.state_dict(),
            'network':network,
            'optimizer':optimizer,
            'epoch':epochs,
            'batch_size':batch_size,
            'pca':pca,
            'scaler':scaler,
            'hidden_layer':hidden_layer,
            'N_p':N_p,
            'instances':instances
        },network_name)


        # # the count of true negatives is C_0,0, false negatives is C_1,0, 
        # # true positives is C_1,1 and false positives is C_0,1.
        # print('\n\nResults of the trained neural network:')
        # print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
        # print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
        # print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))        



        
        for trig in range(len(test_sets)):
            for test in range(len(test_sets[trig])):
                if True: #test!=train:
                    network.eval()
                    #call secondary test set#
                    #For Windows
                    # secondary_mat_contents_0 =\
                    #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type1')\
                    #         ['CA_Gelsdata']
                    # secondary_mat_contents_1 =\
                    #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type2')\
                    #         ['CA_Gelsdata']
                    #Foru Ubuntu
                    secondary_mat_contents_0=sio.loadmat\
                        ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_x'+test_sets[trig][test]+'_type1')['CA_Gelsdata']       
                    secondary_mat_contents_1=sio.loadmat\
                        ('/media/dorukaks/Database/Nathan_GelsData/5th_DM/CA_5thDM_R1_Part1_Pts_x'+test_sets[trig][test]+'_type2')['CA_Gelsdata']
                    # secondary_mat_contents_0 =\
                    #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Pts_x'+test_sets[test]+'_type1')\
                    #         ['CA_Gelsdata']
                    # secondary_mat_contents_1 =\
                    #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Pts_x'+test_sets[test]+'_type2')\
                    #         ['CA_Gelsdata']

                    # secondary_mat_contents_0=secondary_mat_contents_0[:6400]
                    # secondary_mat_contents_1=secondary_mat_contents_1[:6400]

                    #Extract the data and create labels etc.#
                    secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
                    secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
                    secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
                    secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
                    #Extract the x points from the cells to a big array
                    for ii in range(len(secondary_mat_contents_0)):
                        secondary_test_data_0[ii]=secondary_mat_contents_0[ii][0]
                        # if ii<len(secondary_mat_contents_1):
                    for ii in range(len(secondary_mat_contents_1)):
                        secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]        
                    secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
                    secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
                    secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

                    #Shuffle the labeled data to get a "neutral" dataset#
                    np.random.shuffle(secondary_test_shuffled)
                    #Separate the labels and data part of the shuffled dataset#
                    secondary_test_label=secondary_test_shuffled[:,-1]
                    secondary_test_data=secondary_test_shuffled[:,0:-1]
                    secondary_test_label=secondary_test_label.astype(int)
                    secondary_test_data=secondary_test_data.astype(float)
            
                    # secondary_test_data_after_pca = pca.transform(secondary_test_data)
                    # secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]

                    secondary_test_data=scaler.transform(secondary_test_data)
                    secondary_label_predicted=network.predict(secondary_test_data,len(hidden_layer)+2)
                    secondary_label_predicted=secondary_label_predicted.cpu()
                    secondary_label_predicted=secondary_label_predicted.numpy()
                    # secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
                    accuracy_nopca[train,idx,trig,test]=accuracy_score(secondary_test_label,secondary_label_predicted)

                else:
                    
                    x_label_predicted=mlp.predict(x_test_after_pca)
                    accuracy_pca[train,idx,trig,test]=accuracy_score(x_label_test,x_label_predicted)
                print('Train t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[trig][test][:-1]\
                +'.'+test_sets[trig][test][-1]+'s completed.')
                print('* Accuracy of the neural network: ',accuracy_nopca[train,idx,trig,test])

print('Execution of the whole code completed. Total runtime: ',time.time()-start_time,'s')

# Dumping the necessary information into a .pckl file for future post processing of the
# simulation results. #
save_name='PCA_pytorch_nopca_1instance_'+str(len(hidden_layer))+'layers_'+\
    layer_info+'.pckl'
save=open(save_name,'wb')
pickle.dump([train_sets,test_sets,n_pca,hidden_layer,train_time,accuracy_nopca,training_loss,instances],save)
save.close

line_colors=['r-o','b-o','g-o','c-o','m-o','k-o']
fig,axs=plt.subplots(1,len(train_sets),sharey=True)
fig.suptitle('Classification Results Without PC Transformation')
for train in range(len(train_sets)):
    for pcs in range(len(n_pca)):
            axs[train].plot(accuracy_nopca[train,pcs,:],line_colors[pcs],label='No PC')      
    # axs[train].plot(accuracy_pca[train,0,:],'r-o',label='PC='+str(n_pca[0]))
    # axs[train].plot(accuracy_pca[train,1,:],'b-o',label='PC='+str(n_pca[1]))
    # axs[train].plot(accuracy_pca[train,2,:],'g-o',label='PC='+str(n_pca[2]))
    # axs[train].plot(accuracy_pca[train,3,:],'c-o',label='PC='+str(n_pca[3]))
    # axs[train].plot(accuracy_pca[train,4,:],'m-o',label='PC='+str(n_pca[4]))
    axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
    axs[train].set_xticklabels([])

handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')
# plt.xticks([])
plt.show()

# fig,axs=plt.subplots(1,len(train_sets),sharey=True)
# fig.suptitle('Classification Results With Different Number of PCs')
# for train in range(len(train_sets)):
#     axs[train].plot(accuracy_pca[train,0,:],'r-o',label='PC=10')
#     axs[train].plot(accuracy_pca[train,1,:],'b-o',label='PC=20')
#     axs[train].plot(accuracy_pca[train,2,:],'g-o',label='PC=30')
#     axs[train].plot(accuracy_pca[train,3,:],'c-o',label='PC=40 ')
#     axs[train].plot(accuracy_pca[train,4,:],'m-o',label='PC=50')
#     axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
#     axs[train].set_xticklabels([])

# handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
# fig.legend(handles,labels,loc='lower right')
# # plt.xticks([])
# plt.show()
                    

'''

#second test set #
secondary_mat_contents_0 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type1')['CA_Gelsdata']
secondary_mat_contents_1 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type2')['CA_Gelsdata']


#create label vectors for the data
secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
print(secondary_test_data_0.shape)

#Extract the x points from the cells to a big array
for idx in range(len(secondary_mat_contents_0)):
    secondary_test_data_0[idx]=secondary_mat_contents_0[idx][0]
    if idx<len(secondary_mat_contents_1):
# for idx in range(len(mat_contents_t1)):
        secondary_test_data_1[idx]=secondary_mat_contents_1[idx][0]

secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

#Shuffle the labeled data to get a "neutral" dataset#
np.random.shuffle(secondary_test_shuffled)
#Separate the labels and data part of the shuffled dataset#
secondary_test_label=secondary_test_shuffled[:,-1]
secondary_test_data=secondary_test_shuffled[:,0:-1]
secondary_test_label=secondary_test_label.astype(int)
secondary_test_data=secondary_test_data.astype(float)



N_p_variance=1
N_p=30 #number of principal components used

#create the hidden layer architecture#
u=30
# v=30
# w=30

hidden_layer = [u]
x_train_after_pca=x_train_pca[:,0:N_p]
x_test_after_pca=x_test_pca[:,0:N_p]
pca_score_sum=sum(pca_score[0:N_p])

scaler=StandardScaler()
scaler.fit(x_train_after_pca)
# scaler.fit(x_test_after_pca)
x_train_after_pca=scaler.transform(x_train_after_pca)
x_test_after_pca=scaler.transform(x_test_after_pca)

print('\n\nTrain t=6.1s, Test t=6.1s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                    max_iter = 1000)
mlp.fit(x_train_after_pca,x_label_train)
x_label_predicted=mlp.predict(x_test_after_pca)

# the count of true negatives is C_0,0, false negatives is C_1,0, 
# true positives is C_1,1 and false positives is C_0,1.
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))

x_train_shuffled_62_pca=pca.transform(secondary_test_data)
pca_score = pca.explained_variance_ratio_
pca_score_sum=sum(pca_score[0:N_p])
x_train_shuffled_62_pca=scaler.transform(x_train_shuffled_62_pca)
print('\n\nTrain t=6.1s, Test t=6.2s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

x_label_shuffled_pred_62=mlp.predict(x_train_shuffled_62_pca)
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(secondary_test_label,x_label_shuffled_pred_62))                
print('* Classification report\n',classification_report(secondary_test_label,x_label_shuffled_pred_62))
print('* Accuracy of the neural network: ',accuracy_score(secondary_test_label,x_label_shuffled_pred_62))
# print(precision_recall)

'''