import pickle
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc, rcParams
import joblib
import torch
import torch.nn as nn
import torch.nn.functional as F
import scipy.io as sio
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
rc('text',usetex=True)
# matplotlib.use("pgf")
rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
# rcParams['text.latex.preamble']=[r"usepackage{lmodern}"]
# params={'text.usetex' : True,
#         'font.size' : 11,
#         'font.family' : 'lmodern',
#         'text.latex.unicode': True,
#         }
# rcParams.update(params)
class MLP_Classifier(nn.Module):
    def __init__(self,n_layers,n_pcs,n_units):
        self.device = torch.device("cuda:0")
        #The number of layers should be written including the input and output
        # layers. E.g: for a network with 1 hidden layer, n_layer should be 3#
        super(MLP_Classifier,self).__init__()
        n_units=np.array(n_units)
        if n_layers-2==1:
            self.fc1=nn.Linear(n_pcs,n_units[0])
            # self.activ1=nn,ReLU()
            self.fc2=nn.Linear(n_units[0],1)
            # self.relu=nn.ReLU()
            # self.out=nn.Softmax(dim=1)
        else:
            self.fc1=nn.Linear(n_pcs,n_units[0])
            # self.activ1=nn.ReLU()
            ctr=1
            name="self.fc"
            # activname="self.activ"
            while ctr+1<n_layers-1:
                exec(name+str(ctr+1)+"=nn.Linear("+str(n_units[ctr-1])+","+\
                    str(n_units[ctr])+")")
                # exec(activname+str(ctr+1)+"=nn.ReLU()")
                ctr+=1
            exec(name+str(n_layers-1)+"=nn.Linear("+str(n_units[-1])+",1)")
        # self.dropout=nn.Dropout(0.2)
            # self.out=nn.Softmax(dim=1)
            # self.relu=nn.ReLU()
    def forward_ReLU(self,x,n_layers):
        x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        x = self.fc1(x)
        ctr = 2
        while ctr<n_layers:
            name="self.fc"+str(ctr)
            # activname="self.activ"+str(ctr)
            # exec("x="+activname+"(x)")
            # exec("x="+name+"(x)")
            x=F.relu(x)
            x=eval(name+'(x)')
            ctr+=1
        # x=self.relu(x)
        x=torch.sigmoid(x)
        # ans=[]
        # for t in x:
        #     if t[0]>t[1]:
        #         ans.append(0)
        #     else:
        #         ans.append(1)
        # return torch.tensor(ans).type(torch.cuda.IntTensor)
        return x
    
    def predict(self,x,n_layers):
        # x=torch.from_numpy(x).type(torch.cuda.FloatTensor)
        pred= self.forward_ReLU(x,n_layers)
        pred=pred.cpu()
        pred=np.round(pred.detach())
        # ans=[]
        # for t in pred:
        #     if t[0]>t[1]:
        #         ans.append(0)
        #     else:
        #         ans.append(1)
        # return torch.tensor(ans).type(torch.cuda.FloatTensor)
        return pred


# deneme=np.zeros((3,5))
# deneme2=np.ones((2,6))
# train_sets=[]
# test_sets=[]
# n_pca=[]
# hidden_layer=[]
# train_time=[]
# accuracy_pca=[]
# training_loss=[]
# training_loss_sizes=[]

#loading the necessary file for figure reproduction
os.chdir("/home/dorukaks/Desktop/softgels/Bitbucket/catalystgelslearning")
# f=open('PCA_pytorch_pczoom_10instance_1layers_30.pckl','rb')
f=open('PCA_pytorch_pczoom_1instance_3layers_300_300_300.pckl','rb')
train_sets,test_sets,n_pca,hidden_layer,train_time,accuracy_pca,training_loss,instances=pickle.load(f)
f.close
# print(accuracy_pca.shape)
# print(train_time.shape)
# print(len(training_loss))
mean_loss=np.zeros((5,20))
training_loss=np.array(training_loss)
for idx in range(training_loss.shape[0]):
    if len(training_loss[idx])!=20:
        break
for idx in range(training_loss.shape[0]):
    if len(hidden_layer)==1:
        mean_loss[idx%5,:]=mean_loss[idx%5,:]+training_loss[idx,:] #for 1 layer
    elif len(training_loss[idx])!=20:
        training_loss[idx]=np.append(training_loss[idx],training_loss[idx][-1]*np.ones(20-len(training_loss[idx])))
        mean_loss[idx%5,:]=mean_loss[idx%5,:]+np.array(training_loss[idx])
    else:
        mean_loss[idx%5,:]=mean_loss[idx%5,:]+np.array(training_loss[idx])
mean_loss=mean_loss/(training_loss.shape[0]/mean_loss.shape[0])
print(len(hidden_layer)," layer(s), ",hidden_layer[0]," units")
print(n_pca)
# print(accuracy_pca.shape)
if instances==1:
    print("Average Accuracy:",np.round(np.mean(np.mean(np.mean(accuracy_pca,axis=3),axis=2),axis=0),decimals=4))
    print("Worst Case:      ",np.round(np.min(np.min(np.min(accuracy_pca,axis=3),axis=2),axis=0),decimals=4))
    print('Training times:  ',np.round(np.mean(train_time,axis=0),decimals=3))
else:
    accuracy_pca1=accuracy_pca[1,:-1,:,:].reshape(1,2,5,10)
    print("Average Accuracy scores: ",np.round(np.mean(np.mean(np.mean(accuracy_pca1,axis=3),axis=0),axis=0),decimals=4))
    print("Worst Case score:        ",np.round(np.min(np.min(np.min(accuracy_pca1,axis=3),axis=0),axis=0),decimals=4))
    # print("Worst Case score:",np.round(np.min(np.min(accuracy_pca,axis=3),axis=0),decimals=4))
    print('Average Training times:  ',np.round(np.mean(train_time,axis=0),decimals=3))

'''
#loading the network model
model=torch.load('MLPClassifier_pytorch_10instance_train71s_pc300_1layers_1000.pth')
# print(model)
N_p=model['N_p']
hidden_layerz=model['hidden_layer']
network=model['network']
pca=model['pca']
scaler=model['scaler']
optimizer=model['optimizer']
# instances=model['instances']
network.eval()
secondary_mat_contents_0 =\
    sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Pts_x72_type1')\
        ['CA_Gelsdata']
secondary_mat_contents_1 =\
    sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/Random_with_Pattern/5th_DM/CA_5thDM_Pts_x72_type2')\
        ['CA_Gelsdata']

secondary_mat_contents_0=secondary_mat_contents_0[:6400]
secondary_mat_contents_1=secondary_mat_contents_1[:6400]

#Extract the data and create labels etc.#
secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
#Extract the x points from the cells to a big array
for ii in range(len(secondary_mat_contents_0)):
    secondary_test_data_0[ii]=secondary_mat_contents_0[ii][0]
    # if ii<len(secondary_mat_contents_1):
for ii in range(len(secondary_mat_contents_1)):
        secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]
secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

#Shuffle the labeled data to get a "neutral" dataset#
np.random.shuffle(secondary_test_shuffled)
#Separate the labels and data part of the shuffled dataset#
secondary_test_label=secondary_test_shuffled[:,-1]
secondary_test_data=secondary_test_shuffled[:,0:-1]
secondary_test_label=secondary_test_label.astype(int)
secondary_test_data=secondary_test_data.astype(float)

secondary_test_data_after_pca = pca.transform(secondary_test_data)
secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]
secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
# secondary_test_data_after_pca=torch.from_numpy(secondary_test_data_after_pca).type(torch.cuda.FloatTensor)
secondary_label_predicted=network.predict(secondary_test_data_after_pca,len(hidden_layer)+2)
secondary_label_predicted=secondary_label_predicted.cpu()
secondary_label_predicted=secondary_label_predicted.numpy()
# secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
print("accuracy :",accuracy_score(secondary_test_label,secondary_label_predicted))
'''

# plt.figure()
# plt.title('Training Loss vs Number of Epochs')
# for idx in range(len(training_loss)):
#     plt.plot(training_loss[idx][:],label="set "+str((idx//5)+1)+", NPca "+str(n_pca[(idx)%5]))
# plt.xlabel('Epoch')
# plt.ylabel('Training Loss')
# plt.legend()

# plt.figure()
# plt.title('Mean Training Loss vs Number of Epochs')
# for idx in range(mean_loss.shape[0]):
#     plt.plot(np.linspace(1,20,num=20),mean_loss[idx,:],label=str(n_pca[idx])+" PCs")
# plt.xlabel('Epoch')
# plt.ylabel('Training Loss')
# plt.xticks(np.linspace(2,20,num=10))
# plt.legend()

# plt.figure()
# plt.title('Mean Training Time vs Number of PCs')
# # for idx in range(train_time.shape[1]):
# plt.plot(np.array(n_pca),np.mean(train_time,axis=0),label="Training Time")
# plt.xlabel('Number of Principal Components')
# plt.ylabel('Training Time [s]')
# plt.xticks(n_pca)
# plt.legend()


# plt.figure()
# plt.title('Training Loss vs Number of Epochs')
# for idx in range(len(training_loss)):
#     plt.plot(training_loss[idx][:],label="set "+str((idx//5)+1)+", NPca "+str(n_pca[(idx)%5]))
# plt.xlabel('Epoch')
# plt.ylabel('Training Loss')
# plt.legend()

line_colors=['r-o','b-v','g-^','c-s','m-p','k-*']

nopca=True
if nopca:
    os.chdir("/home/dorukaks/Desktop/softgels/Bitbucket/catalystgelslearning")
    if instances==1:
        f=open('PCA_pytorch_nopca_1instance_3layers_300_300_300.pckl','rb')
    else:
        f=open('PCA_pytorch_nopca_10instance_1layers_30.pckl','rb')
    _,_,_,_,train_time_nopca,accuracy_nopca,_,_=pickle.load(f)
    f.close
    print(accuracy_nopca.shape)
    accuracy_nopca1=accuracy_nopca[0,:,:,:].reshape(1,2,1,10)
    print("No PCA Mean Accuracy  :",np.round(np.mean(accuracy_nopca1),decimals=4))
    print("No PCA WC Accuracy    :",np.round(np.min(accuracy_nopca1),decimals=4))
    print("No PCA Training Time  :",np.round(np.mean(train_time_nopca),decimals=4))

test_sets=np.array(test_sets)
rc('axes',labelsize=25)
rc('axes',titlesize=25)
rc('legend',fontsize=20)
rc('ytick',labelsize=20)
rc('xtick',labelsize=20)
# rcParams["figure.titlesize"] = 'x-large'
if instances==1:
    fig,axs=plt.subplots(len(test_sets),len(train_sets),figsize=(18,10),sharey='all')
    # plt.setp(axs,yticks=[0.0,0.2,0.4,0.6,0.8,1.0])
    # fig.suptitle('Classification Results for $T_{train}$=0.1s, $N_{units}=$'+str(hidden_layer[0])+' With Different Number of PCs',fontsize=30)
    # fig.suptitle('Classification Results for $T_{train}$=0.1s, $N_{units}=$'+str(hidden_layer[0])+' Without PC Transformation',fontsize=30)
    # axs[0].set_ylabel("Classification accuracy")
    fig.text(0.07,0.5,'Classification Accuracy',va='center',ha='center',rotation='vertical',fontsize=rcParams['axes.labelsize'])
    for test in range(len(test_sets)):
        for train in range(len(train_sets)):
            for pcs in range(len(n_pca)):
                axs[test][train].plot(accuracy_pca[train,pcs,test,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))
            if nopca:
                axs[test][train].plot(accuracy_nopca[train,-1,test,:],line_colors[-1],label='No PC Tr.')
            if test==0:
                axs[test][train].title.set_text('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
                axs[test][train].set_xticks(np.array([0,9]))
                axs[test][train].set_xticklabels(['$7.1$','$8.0$'])
            else:
                axs[test][train].set_xticks(np.array([0,9]))
                axs[test][train].set_xticklabels(['$8.1$','$9.0$'])
            axs[test][train].set_ylim([0.4,1.0])
            axs[test][train].yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.1))
            axs[test][train].xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(1))
            axs[test][train].tick_params(which='major',width=2,length=4)
            axs[test][train].tick_params(which='minor',width=1,length=2)

    axs[0][0].set_ylabel('7-8s Random Set')
    axs[1][0].set_ylabel('8-9s Random Set')
    handles,labels=axs[len(test_sets)-1][len(train_sets)-1].get_legend_handles_labels()
    fig.legend(handles,labels,bbox_to_anchor=(0.15, -0.03, .7, .102),ncol=6,mode="expand")
    plt.setp(axs,yticks=[0.4,0.6,0.8,1.0])
    plt.setp(axs,yticklabels=["0.4","0.6","0.8","1.0"])
    axs[0][0].get_yticklabels()[0].set_color('red')
    axs[1][0].get_yticklabels()[0].set_color('red')

    # legend(, loc=3,
    #    ncol=2, mode="expand", borderaxespad=0.)
    # plt.yticks([0,0.2,0.4,0.6,0.8,1.0])
    # mng=plt.get_current_fig_manager()
    # mng.resize(*mng.window.maxsize())
    # fig.set_size_inches(w=4.7747, h=3.5)
    # savename=str(len(hidden_layer))+'layers_'+str(hidden_layer[0])+'units_rand_pczoom.pdf'
    # plt.savefig(savename)
    plt.subplots_adjust(wspace=0.25)
    # plt.gca().get_yticklabels()[1].set_color('red')
    # axs[0][0].get_yticks()[0].set_color('red')
    plt.show()
elif instances==10:
    rc('xtick',labelsize=25)
    rc('ytick',labelsize=25)
    accuracy_pca=accuracy_pca[1,:-1,:,:].reshape(1,2,5,10)
    # print(len(test_sets))
    # print(accuracy_pca.shape)
    fig,axs=plt.subplots(1,2,figsize=(10,8),sharey='all')
    # fig.suptitle('Classification Results for $T_{train}$=1s, $N_{units}=$'+str(hidden_layer[0])+' With Different Number of PCs',fontsize=30)
    # fig.suptitle('Classification Results for $T_{train}$=1s, $N_{units}=$'+str(hidden_layer[0])+' Without PC Transformation',fontsize=30)
    fig.text(0.03,0.5,'Classification Accuracy',va='center',ha='center',rotation='vertical',fontsize=rcParams['axes.labelsize'])
    fig.text(0.5,0.90,'30 Units Refined PC',va='center',ha='center',fontsize=rcParams['axes.labelsize'])
    for train in range(len(train_sets)-1):
        for test in range(len(test_sets)-1):
            for pcs in range(len(n_pca)):
                axs[test].plot(accuracy_pca[train,test,pcs,:],line_colors[pcs],label='PC='+str(n_pca[pcs]))
            if nopca:
                axs[test].plot(accuracy_nopca[train,test,-1,:],line_colors[-1],label='No PC')
            if test==0:
                axs[test].set_xticks(np.array([0,9]))
                axs[test].set_xticklabels(['$7.1$','$8.0$'])
            else:
                axs[test].set_xticks(np.array([0,9]))
                axs[test].set_xticklabels(['$8.1$','$9.0$'])
            # axs[test].set_ylim([0.95,1.0])
            axs[test].set_ylim([0.95,1.0])
            axs[test].yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.01))
            axs[test].xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(1))
            axs[test].tick_params(which='major',width=2,length=4)
            axs[test].tick_params(which='minor',width=1,length=2)
    # axs[0].set_title('$7-8$ Random Set')
    # axs[1].set_title('$8-9$ Random Set')

    # axs[2][2].set_xlabel('5th DM')
    # axs[0].set_ylabel('$6-7s$ Grid Set')
    # axs[0].set_ylabel('$6-7s$ Training Set')
    # axs[2][0].set_ylabel('5th DM')
    handles,labels=axs[1].get_legend_handles_labels()
    fig.legend(handles,labels,bbox_to_anchor=(-0.01, -0.02, 1.02, .102),ncol=6,mode="expand",handletextpad=0.1)
    plt.setp(axs,yticks=[0.95,.96,.97,.98,.99,1.0])
    # plt.setp(axs,yticks=[0.75,0.8,0.85,0.9,0.95,1.0])
    plt.setp(axs,yticklabels=['0.95','0.96','0.97','0.98','0.99','1.0'])
    # plt.setp(axs,yticklabels=['0.75','0.8','0.85','0.9','0.95','1.0'])
    axs[0].get_yticklabels()[0].set_color('red')
    # axs[1][0].get_yticklabels()[0].set_color('red')
    # fig.legend(handles,labels,loc='lower right')
    # plt.xticks([])
    # savename=str(len(hidden_layer))+'layers_'+str(hidden_layer[0])+'units_rand_10inst_lowpca.pdf'
    # plt.savefig(savename)
    plt.show()


# for train in range(2):
#     for test in range(2):
#         axs[train][test].plot(accuracy_pca[train,test,0,:],'r-o',label='PC='+str(n_pca[0]))
#         axs[train][test].plot(accuracy_pca[train,test,1,:],'b-o',label='PC='+str(n_pca[1]))
#         axs[train][test].plot(accuracy_pca[train,test,2,:],'g-o',label='PC='+str(n_pca[2]))
#         axs[train][test].plot(accuracy_pca[train,test,3,:],'c-o',label='PC='+str(n_pca[3]))
#         # axs[train][test].plot(accuracy_pca[train,test,4,:],'m-o',label='PC=3367')
#         # axs[train][test].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s & '+train_sets[train+shift][0]+\
#         #     '.'+train_sets[train+shift][1]+'s & '+train_sets[train+2*shift][0]+'.'+train_sets[train+2*shift][1]+'s')
#         axs[train][test].set_xticklabels([])
# axs[1][0].set_xlabel('7-8s Str')
# axs[1][1].set_xlabel('6-7s Str')
# # axs[2][2].set_xlabel('5th DM')
# axs[0][0].set_ylabel('7-8s Rand')
# axs[1][0].set_ylabel('8-9s Rand')
# # axs[2][0].set_ylabel('5th DM')
# handles,labels=axs[1][1].get_legend_handles_labels()
# fig.legend(handles,labels,loc='lower right')
# # plt.xticks([])
# plt.show()
