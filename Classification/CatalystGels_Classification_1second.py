"""
Classification trial 
Changelog:
v0 Created on Sat Jan 25 2020 15:23
v1 Created on Sun Jan 26 2020
    Changes: instead of computing the accuracy of one training set at the test dataset
             split from the training set, the code scans over the given spectrum of data.
v1.1 Created on Mon Jan 27 2020
    Changes: Plotting function fixed.
v1.2 Created on Thur Jan 30 2020 13:00
    Changes: Modified the classification training setup so that (not timewise 
             neighboring) sets of data are used for training.
v1.3 Created on Fri Jan 31 2020 14:57
    Changes: Trying to train the NN with 3 different time instances for increased 
             accuracy.
v1.4 Created on Wed Feb 05 2020 15:33
    Changes: Number of training instances is increased to 10 (1s). Saving option for
             trained networks is also added. Time is recorded for performance 
             purposes.
v1.5 Created on Sat Feb 08 18:33
    Changes: Due to memory issues raised (and as discussed with AG) the data
             pool is reduced. Also the 5th dataset is removed from the simulation due to
             irregular parameter choice.
Author: Doruk
"""
#This python file is created to do the type classification of the simulation
#results based on the previous classifiers (on 1st dataset) written by DK

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.cm as cm
#import pandas as pd
import pickle
import joblib

from sklearn.pipeline import Pipeline
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.font_manager import FontProperties
from sklearn.decomposition import PCA
#form sklearn.decomposition import FastICA, KernelPCA, NMF
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, silhouette_samples

from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor, MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import time





# 3rd dm = 71-80
# 4th dm = 61-70
# 5th dm = 71-90, 141-150

# train_sets=['61','62','63','64','65','66','67','68','69','70']
# ['61','62','63','64','65','66','67','68','69','70']
# test_sets=['61','62','63','64','65','66','67','68','69','70']
# n_pca=np.array([10,30])
n_pca=np.array([10,30,100,1000,3367])
# n_pca=np.array([10,20,30,40,50])
dms=["3rd","4th"]
# dms=["3rd","4th","5th"]
# shift=3 #make the training set 0.3s apart
accuracy_pca=np.zeros((len(dms),len(dms),len(n_pca),10))
pca_scores=np.zeros((len(dms),len(n_pca)))
train_time=np.zeros((len(dms),len(n_pca)))

start_time=time.time()
for train in range(len(dms)):
    training_interval=train+3
    if training_interval==3:
        train_sets=['71','72','73','74','75','76','77','78','79','80']
        test_sets=[['61','62','63','64','65','66','67','68','69','70']]
        dm='3rd'
    elif training_interval==4:
        train_sets=['61','62','63','64','65','66','67','68','69','70']
        test_sets=[['71','72','73','74','75','76','77','78','79','80']]
        dm='4th'
    else:
        train_sets=['141','142','143','144','145','146','147','148','149','150']
        test_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70']]
        dm='5th'

    print('Iteration for Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
        +train_sets[-1][0]+'.'+train_sets[-1][1]+'s has started.')


    #Load files and extract the cell structures from files
    #For Windows
    # mat_contents_t0_p1=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type1')['CA_Gelsdata']
    # mat_contents_t1_p1=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type2')['CA_Gelsdata']
    # mat_contents_t0_p2=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+shift]+'_type1')['CA_Gelsdata']
    # mat_contents_t1_p2=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+shift]+'_type2')['CA_Gelsdata']
    # mat_contents_t0_p3=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+2*shift]+'_type1')['CA_Gelsdata']
    # mat_contents_t1_p3=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train+2*shift]+'_type2')['CA_Gelsdata']
    #For Ubuntu
    # Certainly we need to find a better way to do this....
    for iii in range(len(train_sets)):
        print("data load loop #",iii)
        matname0="mat_contents_t0_p"+str(iii+1)
        matname1="mat_contents_t1_p"+str(iii+1)
        if dm!="5th":
            exec(matname0+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_x"+train_sets[iii]+"_type1')['CA_Gelsdata']")
            exec(matname1+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_x"+train_sets[iii]+"_type1')['CA_Gelsdata']")                   
        else:
            exec(matname0+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_R1_Part1_Pts_x"+train_sets[iii]+"_type1')['CA_Gelsdata']")
            exec(matname1+"=sio.loadmat\
            ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_R1_Part1_Pts_x"+train_sets[iii]+"_type1')['CA_Gelsdata']")    
            
            # exec(matname0+"=sio.loadmat\
            # ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_x"+train_sets[iii]+"_type1')['CA_Gelsdata']")
            # exec(matname1+"=sio.loadmat\
            # ('/media/dorukaks/Database/Nathan_GelsData/"+dm+"_DM/CA_"+dm+"DM_Pts_x"+train_sets[iii]+"_type1')['CA_Gelsdata']")
        exec(matname0+"="+matname0+"[6400:]")
        exec(matname1+"="+matname1+"[1600:]")

        labelname0="train_label_0_p"+str(iii+1)
        labelname1="train_label_1_p"+str(iii+1)
        exec(labelname0+"=np.zeros(mat_contents_t0_p"+str(iii+1)+".shape)")
        exec(labelname1+"=np.ones(mat_contents_t1_p"+str(iii+1)+".shape)")

        arrname0="x_train_0_p"+str(iii+1)
        arrname1="x_train_1_p"+str(iii+1)
        exec(arrname0+"=np.zeros(("+matname0+".shape[0],"+matname0+"[0][0].shape[1]))")
        exec(arrname1+"=np.zeros(("+matname1+".shape[0],"+matname1+"[0][0].shape[1]))")
        exec("for idx in range(len("+matname0+")):"+arrname0+"[idx]="+matname0+"[idx][0]")
        exec("for idx in range(len("+matname1+")):"+arrname1+"[idx]="+matname1+"[idx][0]")

        all_arrname="x_train_all_p"+str(iii+1)
        all_labelname="label_train_all_p"+str(iii+1)
        all_labeledname="x_train_all_labeled_p"+str(iii+1)
        exec(all_arrname+"=np.append("+arrname0+","+arrname1+",axis=0)")
        exec(all_labelname+"=np.append("+labelname0+","+labelname1+",axis=0)")
        exec(all_labeledname+"=np.append("+all_arrname+","+all_labelname+",axis=1)")
        exec("np.random.shuffle("+all_labeledname+")")
        shuffled_labelname="x_label_shuffled_p"+str(iii+1)
        shuffled_dataname="x_train_shuffled_p"+str(iii+1)
        exec(shuffled_labelname+"="+all_labeledname+"[:,-1]")
        exec(shuffled_dataname+"="+all_labeledname+"[:,0:-1]")
        exec(shuffled_labelname+"="+shuffled_labelname+".astype(int)")
        exec(shuffled_dataname+"="+shuffled_dataname+".astype(float)")

        training_data="x_train_p"+str(iii+1)
        testing_data="x_test_p"+str(iii+1)
        training_label="x_label_train_p"+str(iii+1)
        testing_label="x_label_test_p"+str(iii+1)
        exec(training_data+","+testing_data+","+training_label+","+testing_label+"=train_test_split("+shuffled_dataname+","+shuffled_labelname+",test_size=0.30)")
        exec("del "+matname0+","+matname1+","+arrname0+","+arrname1+","+all_arrname+","+all_labelname+","+all_labeledname+","+shuffled_dataname+","+shuffled_labelname)

    # print("line ",145)
    x_train=x_train_p1
    x_label=x_label_train_p1
    del x_train_p1,x_label_train_p1

    #Found it, lol
    for iii in range(len(train_sets)-1):
        # print("append loop no ",iii)
        # print("size of x_label: ",x_label.shape)
        # print("size of x_train: ",x_train.shape)
        train_k="x_train_p"+str(iii+2)
        train_all="x_train"
        label_k="x_label_train_p"+str(iii+2)
        label_all="x_label"
        exec(train_all+"=np.append("+train_all+","+train_k+",axis=0)")
        exec(label_all+"=np.append("+label_all+","+label_k+",axis=0)")
        exec("del "+train_k+","+label_k)
    x_label=np.reshape(x_label,(x_train.shape[0],1))

    print('xtrain shape:',x_train.shape)
    print('xlabel shape:',x_label.shape)
    x_train_all=np.append(x_train,x_label,axis=1)
    np.random.shuffle(x_train_all)
    x_train=x_train_all[:,0:-1]
    x_label=x_train_all[:,-1]
    # for "kisaltma icin yapildi" in range(2):
        # mat_contents_t0_p1=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[0]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p1=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[0]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p2=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[1]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p2=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[1]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p3=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[2]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p3=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[2]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p4=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[3]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p4=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[3]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p5=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[4]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p5=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[4]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p6=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[5]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p6=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[5]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p7=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[6]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p7=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[6]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p8=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[7]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p8=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[7]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p9=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[8]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p9=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[8]+'_type2')['CA_Gelsdata']
        # mat_contents_t0_p10=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[9]+'_type1')['CA_Gelsdata']
        # mat_contents_t1_p10=sio.loadmat\
        #     ('/media/dorukaks/Database/Nathan_GelsData/'+dm+'_DM/CA_'+dm+'DM_Pts_x'+train_sets[9]+'_type2')['CA_Gelsdata']

        # for xxx in range():
            #buraya oyle bir dongu yaz ki butun datayi tekte kompakt bir sekilde cagirsin #

        # print(mat_contents_t1_p1.shape,mat_contents_t1_p1[0].shape,mat_contents_t1_p1[0][0].shape)

        #create label vectors for the data

        # train_label_0_p1=np.zeros(mat_contents_t0_p1.shape)
        # train_label_1_p1=np.ones(mat_contents_t1_p1.shape)
        # train_label_0_p2=np.zeros(mat_contents_t0_p2.shape)
        # train_label_1_p2=np.ones(mat_contents_t1_p2.shape)
        # train_label_0_p3=np.zeros(mat_contents_t0_p3.shape)
        # train_label_1_p3=np.ones(mat_contents_t1_p3.shape)
        # train_label_0_p4=np.zeros(mat_contents_t0_p4.shape)
        # train_label_1_p4=np.ones(mat_contents_t1_p4.shape)
        # train_label_0_p5=np.zeros(mat_contents_t0_p5.shape)
        # train_label_1_p5=np.ones(mat_contents_t1_p5.shape)
        # train_label_0_p6=np.zeros(mat_contents_t0_p6.shape)
        # train_label_1_p6=np.ones(mat_contents_t1_p6.shape)
        # train_label_0_p7=np.zeros(mat_contents_t0_p7.shape)
        # train_label_1_p7=np.ones(mat_contents_t1_p7.shape)
        # train_label_0_p8=np.zeros(mat_contents_t0_p8.shape)
        # train_label_1_p8=np.ones(mat_contents_t1_p8.shape)
        # train_label_0_p9=np.zeros(mat_contents_t0_p9.shape)
        # train_label_1_p9=np.ones(mat_contents_t1_p9.shape)
        # train_label_0_p10=np.zeros(mat_contents_t0_p10.shape)
        # train_label_1_p10=np.ones(mat_contents_t1_p10.shape)

        # x_train_0_p1=np.zeros((mat_contents_t0_p1.shape[0],mat_contents_t0_p1[0][0].shape[1]))
        # x_train_1_p1=np.zeros((mat_contents_t1_p1.shape[0],mat_contents_t1_p1[0][0].shape[1]))
        # x_train_0_p2=np.zeros((mat_contents_t0_p2.shape[0],mat_contents_t0_p2[0][0].shape[1]))
        # x_train_1_p2=np.zeros((mat_contents_t1_p2.shape[0],mat_contents_t1_p2[0][0].shape[1]))
        # x_train_0_p3=np.zeros((mat_contents_t0_p3.shape[0],mat_contents_t0_p3[0][0].shape[1]))
        # x_train_1_p3=np.zeros((mat_contents_t1_p3.shape[0],mat_contents_t1_p3[0][0].shape[1]))
        # x_train_0_p4=np.zeros((mat_contents_t0_p4.shape[0],mat_contents_t0_p4[0][0].shape[1]))
        # x_train_1_p4=np.zeros((mat_contents_t1_p4.shape[0],mat_contents_t1_p4[0][0].shape[1]))
        # x_train_0_p5=np.zeros((mat_contents_t0_p5.shape[0],mat_contents_t0_p5[0][0].shape[1]))
        # x_train_1_p5=np.zeros((mat_contents_t1_p5.shape[0],mat_contents_t1_p5[0][0].shape[1]))
        # x_train_0_p6=np.zeros((mat_contents_t0_p6.shape[0],mat_contents_t0_p6[0][0].shape[1]))
        # x_train_1_p6=np.zeros((mat_contents_t1_p6.shape[0],mat_contents_t1_p6[0][0].shape[1]))
        # x_train_0_p7=np.zeros((mat_contents_t0_p7.shape[0],mat_contents_t0_p7[0][0].shape[1]))
        # x_train_1_p7=np.zeros((mat_contents_t1_p7.shape[0],mat_contents_t1_p7[0][0].shape[1]))
        # x_train_0_p8=np.zeros((mat_contents_t0_p8.shape[0],mat_contents_t0_p8[0][0].shape[1]))
        # x_train_1_p8=np.zeros((mat_contents_t1_p8.shape[0],mat_contents_t1_p8[0][0].shape[1]))
        # x_train_0_p9=np.zeros((mat_contents_t0_p9.shape[0],mat_contents_t0_p9[0][0].shape[1]))
        # x_train_1_p9=np.zeros((mat_contents_t1_p9.shape[0],mat_contents_t1_p9[0][0].shape[1]))
        # x_train_0_p10=np.zeros((mat_contents_t0_p10.shape[0],mat_contents_t0_p10[0][0].shape[1]))
        # x_train_1_p10=np.zeros((mat_contents_t1_p10.shape[0],mat_contents_t1_p10[0][0].shape[1]))

        # # print(x_train_0_p1.shape)

        # #Extract the x points from the cells to a big array
        # for idx in range(len(mat_contents_t0_p1)):
        #     x_train_0_p1[idx]=mat_contents_t0_p1[idx][0]
        # for idx in range(len(mat_contents_t0_p2)):
        #     x_train_0_p2[idx]=mat_contents_t0_p2[idx][0]
        # for idx in range(len(mat_contents_t0_p3)):
        #     x_train_0_p3[idx]=mat_contents_t0_p3[idx][0]
        # for idx in range(len(mat_contents_t0_p3)):
        #     x_train_0_p3[idx]=mat_contents_t0_p3[idx][0]
        #     # if idx<len(mat_contents_t1_p1):
        # for idx in range(len(mat_contents_t1_p1)):
        #         x_train_1_p1[idx]=mat_contents_t1_p1[idx][0]
        # for idx in range(len(mat_contents_t1_p2)):
        #         x_train_1_p2[idx]=mat_contents_t1_p2[idx][0]
        # for idx in range(len(mat_contents_t1_p3)):
        #         x_train_1_p3[idx]=mat_contents_t1_p3[idx][0]

        #Here we have the x positions of the 8000+6400 simulations at t=6.1s (type1 and
        # type2) extracted from the .mat file. Then the next step is to reduce the dimension
        #of this huge sized matrix by applying principle component analysis (PCA)

        # x_train_all_p1=np.append(x_train_0_p1,x_train_1_p1,axis=0)
        # x_train_all_p2=np.append(x_train_0_p2,x_train_1_p2,axis=0)
        # x_train_all_p3=np.append(x_train_0_p3,x_train_1_p3,axis=0)

        # label_train_all_p1=np.append(train_label_0_p1,train_label_1_p1,axis=0)
        # label_train_all_p2=np.append(train_label_0_p2,train_label_1_p2,axis=0)
        # label_train_all_p3=np.append(train_label_0_p3,train_label_1_p3,axis=0)

        # x_train_all_labeled_p1=np.append(x_train_all_p1,label_train_all_p1,axis=1)
        # x_train_all_labeled_p2=np.append(x_train_all_p2,label_train_all_p2,axis=1)
        # x_train_all_labeled_p3=np.append(x_train_all_p3,label_train_all_p3,axis=1)

        # #Shuffle the labeled data to get a "neutral" dataset#
        # np.random.shuffle(x_train_all_labeled_p1)
        # np.random.shuffle(x_train_all_labeled_p2)
        # np.random.shuffle(x_train_all_labeled_p3)
        # #Separate the labels and data part of the shuffled dataset#
        # x_label_shuffled_p1=x_train_all_labeled_p1[:,-1]
        # x_label_shuffled_p2=x_train_all_labeled_p2[:,-1]
        # x_label_shuffled_p3=x_train_all_labeled_p3[:,-1]

        # x_train_shuffled_p1=x_train_all_labeled_p1[:,0:-1]
        # x_train_shuffled_p2=x_train_all_labeled_p2[:,0:-1]
        # x_train_shuffled_p3=x_train_all_labeled_p3[:,0:-1]

        # x_label_shuffled_p1=x_label_shuffled_p1.astype(int)
        # x_label_shuffled_p2=x_label_shuffled_p2.astype(int)
        # x_label_shuffled_p3=x_label_shuffled_p3.astype(int)

        # x_train_shuffled_p1=x_train_shuffled_p1.astype(float)
        # x_train_shuffled_p2=x_train_shuffled_p2.astype(float)
        # x_train_shuffled_p3=x_train_shuffled_p3.astype(float)
        # # print('Dimension of the training data:',x_train_all_p1.shape)

        # #divide the set as training/test set#
        # x_train_p1 , x_test_p1 , x_label_train_p1 ,x_label_test_p1 = \
        #     train_test_split(x_train_shuffled_p1,x_label_shuffled_p1,test_size=0.40)
        # x_train_p2 , x_test_p2 , x_label_train_p2 ,x_label_test_p2 = \
        #     train_test_split(x_train_shuffled_p2,x_label_shuffled_p2,test_size=0.40)
        # x_train_p3 , x_test_p3 , x_label_train_p3 ,x_label_test_p3 = \
        #     train_test_split(x_train_shuffled_p3,x_label_shuffled_p3,test_size=0.40)

        # x_train=np.append(x_train_p1,x_train_p2,axis=0)
        # x_train=np.append(x_train,x_train_p3,axis=0)

        # x_label=np.append(x_label_train_p1,x_label_train_p2,axis=0)
        # x_label=np.append(x_label,x_label_train_p3,axis=0)

        # x_label=np.reshape(x_label,(x_train.shape[0],1))

        # print('xtrain shape:',x_train.shape)
        # print('xlabel shape:',x_label.shape)
        # x_train_all=np.append(x_train,x_label,axis=1)
        # np.random.shuffle(x_train_all)
        # x_train=x_train_all[:,0:-1]
        # x_label=x_train_all[:,-1]


        # a,b = x_train.shape
        # print('Size of the combined training set: ',a,b)

    # print(338,'buyuk commented loopun arkasi')
    #buraya pca loop'u gelecek bu sayede en dista train loopu sonra icerde pca loopu en icte de 
    # test loopu olacak sekilde butun simulasyon kosacak#
    for idx in range(len(n_pca)):
        try:
            mlp
        except NameError:
            pass
        else:
            del mlp
        print('Iteration with #PC:',n_pca[idx],' has started.')
        pca=PCA(n_components = n_pca[idx])
        pca.fit(x_train)
        x_train_pca=pca.transform(x_train)

        N_p_variance=1
        N_p=n_pca[idx]#number of principal components used

        x_train_after_pca=x_train_pca[:,0:N_p]
        pca_score = pca.explained_variance_ratio_
        pca_score_sum=sum(pca_score[0:N_p])
        pca_scores[train,idx]=pca_score_sum

        scaler=StandardScaler()
        scaler.fit(x_train_after_pca)
        x_train_after_pca=scaler.transform(x_train_after_pca)
        
        for iii in range(len(train_sets)):
            testing_data="x_test_p"+str(iii+1)
            x_pca_name="x_test_pca_p"+str(iii+1)
            x_name_after_pca="x_test_after_pca_p"+str(iii+1)
            exec(x_pca_name+"=pca.transform("+testing_data+")")
            exec(x_name_after_pca+"="+x_pca_name+"[:,0:N_p]")
            exec(x_name_after_pca+"=scaler.transform("+x_name_after_pca+")")
        

        # scaler.fit(x_test_after_pca)


            
        # x_test_pca_p1 = pca.transform(x_test_p1)
        # x_test_pca_p2 = pca.transform(x_test_p2)
        # x_test_pca_p3 = pca.transform(x_test_p3)

        # x_test_after_pca_p1=x_test_pca_p1[:,0:N_p]
        # x_test_after_pca_p2=x_test_pca_p2[:,0:N_p]
        # x_test_after_pca_p3=x_test_pca_p3[:,0:N_p]

        # x_test_after_pca_p1=scaler.transform(x_test_after_pca_p1)
        # x_test_after_pca_p2=scaler.transform(x_test_after_pca_p2)
        # x_test_after_pca_p3=scaler.transform(x_test_after_pca_p3)

        #create the hidden layer architecture#
        u=30
        v=30
        w=30
        y=30
        hidden_layer = [u,v]

        # print('\n\nTrain t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
        #     +'.'+test_sets[test][1]+'s results:')

        # print('\n [MLP Classifier Result]')
        # print('\n* The number of PCs :', N_p, \
        #         '\n* Total PCA score :',pca_score_sum,\
        #         '\n* Hidden Layer :', hidden_layer)

        print('Neural network training started...')
        inter_time=time.time()
        mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='tanh',\
                            max_iter = 2000)
        mlp.fit(x_train_after_pca,x_label)

        train_time[train,idx]=time.time()-inter_time
        print('Neural network trained in ',train_time[train,idx],'s.')

        network_name=''
        for iii in range(len(hidden_layer)):
            network_name=network_name+str(hidden_layer[iii])+'_'
        network_name=layer_info=network_name[:-1]
        network_name='MLPClassifier_10instance_train'+train_sets[train]+'s_pc'+str(n_pca[idx])+'_'+\
            str(len(hidden_layer))+'layers_'+network_name+'.sav'
        joblib.dump(mlp,network_name)


        # # the count of true negatives is C_0,0, false negatives is C_1,0, 
        # # true positives is C_1,1 and false positives is C_0,1.
        # print('\n\nResults of the trained neural network:')
        # print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
        # print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
        # print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))        



        trig=0
        for test in range(len(test_sets)+1):
            print("train=",train)
            print("trig=",trig)
            print("test=",test)
            if test!=train:
                #call secondary test set#
                # #For Windows
                # secondary_mat_contents_0 =\
                #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type1')\
                #         ['CA_Gelsdata']
                # secondary_mat_contents_1 =\
                #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type2')\
                #         ['CA_Gelsdata']
                #For Ubuntu
                # print("trig=",trig)
                for iii in range(len(test_sets[test-1])):
                    print("test!=train, test loop#:",iii)
                    matname0="secondary_mat_contents_t0"
                    matname1="secondary_mat_contents_t1"
                    if dms[test]=="5th":
                        exec(matname0+"=sio.loadmat\
                        ('/media/dorukaks/Database/Nathan_GelsData/"+dms[test]+"_DM/CA_"+dms[test]+"DM_R1_Part1_Pts_x"+test_sets[trig][iii]+"_type1')['CA_Gelsdata']")
                        exec(matname1+"=sio.loadmat\
                        ('/media/dorukaks/Database/Nathan_GelsData/"+dms[test]+"_DM/CA_"+dms[test]+"DM_R1_Part1_Pts_x"+test_sets[trig][iii]+"_type1')['CA_Gelsdata']")                       
                    else:
                        exec(matname0+"=sio.loadmat\
                        ('/media/dorukaks/Database/Nathan_GelsData/"+dms[test]+"_DM/CA_"+dms[test]+"DM_Pts_x"+test_sets[trig][iii]+"_type1')['CA_Gelsdata']")
                        exec(matname1+"=sio.loadmat\
                        ('/media/dorukaks/Database/Nathan_GelsData/"+dms[test]+"_DM/CA_"+dms[test]+"DM_Pts_x"+test_sets[trig][iii]+"_type1')['CA_Gelsdata']")
                    exec(matname0+"="+matname0+"[6400:]")
                    exec(matname1+"="+matname1+"[1600:]")

                    test_label_0=np.zeros(secondary_mat_contents_t0.shape)
                    test_label_1=np.ones(secondary_mat_contents_t1.shape)

                    test_x_0=np.zeros((secondary_mat_contents_t0.shape[0],secondary_mat_contents_t0[0][0].shape[1]))
                    test_x_1=np.zeros((secondary_mat_contents_t1.shape[0],secondary_mat_contents_t1[0][0].shape[1]))
                    for ii in range(len(secondary_mat_contents_t0)):test_x_0[ii]=secondary_mat_contents_t0[ii][0]
                    for ii in range(len(secondary_mat_contents_t1)):test_x_1[ii]=secondary_mat_contents_t1[ii][0]

                    del secondary_mat_contents_t0,secondary_mat_contents_t1

                    x_test_all=np.append(test_x_0,test_x_1,axis=0)
                    label_test_all=np.append(test_label_0,test_label_1,axis=0)
                    test_all_labeled=np.append(x_test_all,label_test_all,axis=1)
                    np.random.shuffle(test_all_labeled)
                    label_test_shuffled=test_all_labeled[:,-1]
                    secondary_test_label=label_test_shuffled.astype(int)
                    x_test_shuffled=test_all_labeled[:,0:-1]
                    secondary_test_data=x_test_shuffled.astype(float)
                    del x_test_all,x_test_shuffled,test_all_labeled,label_test_shuffled,label_test_all,test_label_0,test_label_1
                    del test_x_1,test_x_0

                    secondary_test_data_after_pca = pca.transform(secondary_test_data)
                    secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]

                    secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
                    secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)

                    #recording order: train set#, test set#, #pca, instance # within the test set
                    accuracy_pca[train,test,idx,iii]=accuracy_score(secondary_test_label,secondary_label_predicted)
                    print('Iteration for Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
                        +train_sets[-1][0]+'.'+train_sets[-1][1]+'s, Test t='+test_sets[trig][iii][0]\
                            +'.'+test_sets[trig][iii][1]+'s is complete.')
                    print('* Accuracy: ',accuracy_pca[train,test,idx,iii])
                    # for "kisaltma icin" in range(31):
                        # all_arrname="x_train_all_p"+str(iii+1)
                        # all_labelname="label_train_all_p"+str(iii+1)
                        # all_labeledname="x_train_all_labeled_p"+str(iii+1)
                        # exec(all_arrname+"=np.append("+arrname0+","+arrname1+",axis=0)")
                        # exec(all_labelname+"=np.append("+labelname0+","+labelname1+",axis=0)")
                        # exec(all_labeledname+"=np.append("+all_arrname+","+all_labelname+",axis=1")
                        # exec("np.random.shuffle("+all_labeledname+")")
                        # shuffled_labelname="x_label_shuffled_p"+str(iii+1)
                        # shuffled_dataname="x_train_shuffled_p"+str(iii+1)
                        # exec(shuffled_labelname+"="+all_labeledname+"[:,-1]")
                        # exec(shuffled_dataname+"="+all_labeledname+"[:,0:-1]")
                        # exec(shuffled_labelname+"="+shuffled_labelname+".astype(int)")
                        # exec(shuffled_dataname+"="+shuffled_dataname+".astype(float)")

                        # training_data="x_train_p"+str(iii+1)
                        # testing_data="x_test_p"+str(iii+1)
                        # training_label="x_label_train_p"+str(iii+1)
                        # testing_label="x_label_test_p"+str(iii+1)
                        # exec(training_data+","+testing_data+","+training_label+","+testing_label+"train_test_split("+shuffled_dataname+","+shuffled_labelname+",test_size=0.50)")

                        # secondary_mat_contents_0 =\
                        #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type1')\
                        #         ['CA_Gelsdata']
                        # secondary_mat_contents_1 =\
                        #     sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type2')\
                        #         ['CA_Gelsdata']


                        # #Extract the data and create labels etc.#
                        # secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
                        # secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
                        # secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
                        # secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
                        # #Extract the x points from the cells to a big array
                        # for ii in range(len(secondary_mat_contents_0)):secondary_test_data_0[ii]=secondary_mat_contents_0[ii][0]
                        #     # if ii<len(secondary_mat_contents_1):
                        # for ii in range(len(secondary_mat_contents_1)):secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]
                        # secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
                        # secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
                        # secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

                        # #Shuffle the labeled data to get a "neutral" dataset#
                        # np.random.shuffle(secondary_test_shuffled)
                        # #Separate the labels and data part of the shuffled dataset#
                        # secondary_test_label=secondary_test_shuffled[:,-1]
                        # secondary_test_data=secondary_test_shuffled[:,0:-1]
                        # secondary_test_label=secondary_test_label.astype(int)
                        # secondary_test_data=secondary_test_data.astype(float)
                
                        # secondary_test_data_after_pca = pca.transform(secondary_test_data)
                        # secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]

                        # secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
                        # secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
                        # accuracy_pca[train,idx,test]=accuracy_score(secondary_test_label,secondary_label_predicted)
                
         
                trig=trig+1

            elif test==train:
                for iii in range(len(train_sets)):
                    exec("x_label_predicted=mlp.predict(x_test_after_pca_p"+str(iii+1)+")")
                    exec("accuracy_pca[train,test,idx,"+str(iii)+"]=accuracy_score(x_label_test_p"+str(iii+1)+",x_label_predicted)")
                    print('Iteration for Train t='+train_sets[0][0]+'.'+train_sets[0][1]+'-'\
                        +train_sets[-1][0]+'.'+train_sets[-1][1]+'s, Test t='+train_sets[iii][0]\
                            +'.'+train_sets[iii][1]+'s is complete.')
                    print('* Accuracy: ',accuracy_pca[train,test,idx,iii])                    
                # x_label_predicted=mlp.predict(x_test_after_pca_p1)
                # accuracy_pca[train,test,idx,0]=accuracy_score(x_label_test_p1,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p2)
                # accuracy_pca[train,test,idx,1]=accuracy_score(x_label_test_p2,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p3)
                # accuracy_pca[train,test,idx,2]=accuracy_score(x_label_test_p3,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p4)
                # accuracy_pca[train,test,idx,3]=accuracy_score(x_label_test_p4,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p5)
                # accuracy_pca[train,test,idx,4]=accuracy_score(x_label_test_p5,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p6)
                # accuracy_pca[train,test,idx,5]=accuracy_score(x_label_test_p6,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p7)
                # accuracy_pca[train,test,idx,6]=accuracy_score(x_label_test_p7,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p8)
                # accuracy_pca[train,test,idx,7]=accuracy_score(x_label_test_p8,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p9)
                # accuracy_pca[train,test,idx,8]=accuracy_score(x_label_test_p9,x_label_predicted)
                # x_label_predicted=mlp.predict(x_test_after_pca_p10)
                # accuracy_pca[train,test,idx,9]=accuracy_score(x_label_test_p10,x_label_predicted)

#end of train for loop

print('Execution of the whole code completed. Total runtime: ',time.time()-start_time,'s')
save_name='PCA_10instance_'+str(len(hidden_layer))+'layers_'+\
    layer_info+'.pckl'
save=open(save_name,'wb')
train_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70']]
test_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70']]
# test_sets=[['71','72','73','74','75','76','77','78','79','80'],['61','62','63','64','65','66','67','68','69','70'],['141','142','143','144','145','146','147','148','149','150']]
pickle.dump([train_sets,test_sets,n_pca,hidden_layer,train_time,accuracy_pca],save)
save.close



fig,axs=plt.subplots(len(dms),len(dms),sharex=True,sharey=True)
fig.suptitle('Classification Results for T_train=1s With Different Number of PCs')

for train in range(len(dms)):
    for test in range(len(dms)):
        axs[train][test].plot(accuracy_pca[train,test,0,:],'r-o',label='PC=10')
        axs[train][test].plot(accuracy_pca[train,test,1,:],'b-o',label='PC=30')
        axs[train][test].plot(accuracy_pca[train,test,2,:],'g-o',label='PC=100')
        axs[train][test].plot(accuracy_pca[train,test,3,:],'c-o',label='PC=1000')
        axs[train][test].plot(accuracy_pca[train,test,4,:],'m-o',label='PC=3367')
        # axs[train][test].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s & '+train_sets[train+shift][0]+\
        #     '.'+train_sets[train+shift][1]+'s & '+train_sets[train+2*shift][0]+'.'+train_sets[train+2*shift][1]+'s')
        axs[train][test].set_xticklabels([])
axs[1][0].set_xlabel('3rd DM')
axs[1][1].set_xlabel('4th DM')
# axs[2][2].set_xlabel('5th DM')
axs[0][0].set_ylabel('3rd DM')
axs[1][0].set_ylabel('4th DM')
# axs[2][0].set_ylabel('5th DM')
handles,labels=axs[1][1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')
# plt.xticks([])
plt.show()
                    

'''

#second test set #
secondary_mat_contents_0 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type1')['CA_Gelsdata']
secondary_mat_contents_1 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type2')['CA_Gelsdata']


#create label vectors for the data
secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
print(secondary_test_data_0.shape)

#Extract the x points from the cells to a big array
for idx in range(len(secondary_mat_contents_0)):
    secondary_test_data_0[idx]=secondary_mat_contents_0[idx][0]
    if idx<len(secondary_mat_contents_1):
# for idx in range(len(mat_contents_t1_p1)):
        secondary_test_data_1[idx]=secondary_mat_contents_1[idx][0]

secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

#Shuffle the labeled data to get a "neutral" dataset#
np.random.shuffle(secondary_test_shuffled)
#Separate the labels and data part of the shuffled dataset#
secondary_test_label=secondary_test_shuffled[:,-1]
secondary_test_data=secondary_test_shuffled[:,0:-1]
secondary_test_label=secondary_test_label.astype(int)
secondary_test_data=secondary_test_data.astype(float)



N_p_variance=1
N_p=30 #number of principal components used

#create the hidden layer architecture#
u=30
# v=30
# w=30

hidden_layer = [u]
x_train_after_pca=x_train_pca[:,0:N_p]
x_test_after_pca=x_test_pca[:,0:N_p]
pca_score_sum=sum(pca_score[0:N_p])

scaler=StandardScaler()
scaler.fit(x_train_after_pca)
# scaler.fit(x_test_after_pca)
x_train_after_pca=scaler.transform(x_train_after_pca)
x_test_after_pca=scaler.transform(x_test_after_pca)

print('\n\nTrain t=6.1s, Test t=6.1s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                    max_iter = 1000)
mlp.fit(x_train_after_pca,x_label_train)
x_label_predicted=mlp.predict(x_test_after_pca)

# the count of true negatives is C_0,0, false negatives is C_1,0, 
# true positives is C_1,1 and false positives is C_0,1.
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))

x_train_shuffled_62_pca=pca.transform(secondary_test_data)
pca_score = pca.explained_variance_ratio_
pca_score_sum=sum(pca_score[0:N_p])
x_train_shuffled_62_pca=scaler.transform(x_train_shuffled_62_pca)
print('\n\nTrain t=6.1s, Test t=6.2s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

x_label_shuffled_pred_62=mlp.predict(x_train_shuffled_62_pca)
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(secondary_test_label,x_label_shuffled_pred_62))                
print('* Classification report\n',classification_report(secondary_test_label,x_label_shuffled_pred_62))
print('* Accuracy of the neural network: ',accuracy_score(secondary_test_label,x_label_shuffled_pred_62))
# print(precision_recall)

'''