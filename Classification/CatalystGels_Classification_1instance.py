"""
Classification trial 
v0 Created on Sat Jan 25 2020 15:23
v0.1 Created on Wed Feb 05 2020 23:10
    Changes:    Edits for Ubuntu, Timekeeping, NN model keeping, Data keeping added
v0.2 Created on Mon Feb 10 2020 09:38
    Changes:    Data cropping trial to investigate an increase in performance

Author: Doruk
"""
#This python file is created to do the type classification of the simulation
#results based on the previous classifiers (on 1st dataset) written by DK

import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.cm as cm
#import pandas as pd

from sklearn.pipeline import Pipeline
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.font_manager import FontProperties
from sklearn.decomposition import PCA
#form sklearn.decomposition import FastICA, KernelPCA, NMF
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, silhouette_samples

from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor, MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import pickle
import joblib
import time


train_sets=['61','62','63','64','65','66','67','68','69','70']
# ['61','62','63','64','65','66','67','68','69','70']
test_sets=['61','62','63','64','65','66','67','68','69','70']
# n_pca=np.array([10,30])
# n_pca=np.array([10,30,100,1000,3367])
n_pca=np.array([10,20,30,40,50])
accuracy_pca=np.zeros((len(train_sets),len(n_pca),len(test_sets)))
pca_scores=np.zeros((len(train_sets),len(n_pca)))
train_time=np.zeros((len(train_sets),len(n_pca)))


start_time=time.time()
for train in range(len(train_sets)):
    print('Iteration for train t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, has started.')


    #Load files and extract the cell structures from files

    #For Windows
    # mat_contents_t0=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type1')['CA_Gelsdata']
    # mat_contents_t1=sio.loadmat\
    #     ('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+train_sets[train]+'_type2')['CA_Gelsdata']
    
    #For Ubuntu
    mat_contents_t0=sio.loadmat\
        ('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+train_sets[train]+'_type1')['CA_Gelsdata']
    mat_contents_t1=sio.loadmat\
        ('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+train_sets[train]+'_type2')['CA_Gelsdata']
    # print(mat_contents_t0.shape,mat_contents_t0[0].shape,mat_contents_t0[0][0].shape)
    
    mat_contents_t0=mat_contents_t0[:6400]
    mat_contents_t1=mat_contents_t1[:6400]

    #create label vectors for the data
    train_label_0=np.zeros(mat_contents_t0.shape)
    train_label_1=np.ones(mat_contents_t1.shape)
    x_train_0=np.zeros((mat_contents_t0.shape[0],mat_contents_t0[0][0].shape[1]))
    x_train_1=np.zeros((mat_contents_t1.shape[0],mat_contents_t1[0][0].shape[1]))
    # print(x_train_0.shape)

    #Extract the x points from the cells to a big array
    for idx in range(len(mat_contents_t0)):
        x_train_0[idx]=mat_contents_t0[idx][0]
        # if idx<len(mat_contents_t1):
    for idx in range(len(mat_contents_t1)):
            x_train_1[idx]=mat_contents_t1[idx][0]

    #Here we have the x positions of the 8000+6400 simulations at t=6.1s (type1 and
    # type2) extracted from the .mat file. Then the next step is to reduce the dimension
    #of this huge sized matrix by applying principle component analysis (PCA)

    x_train_all=np.append(x_train_0,x_train_1,axis=0)

    label_train_all=np.append(train_label_0,train_label_1,axis=0)

    x_train_all_labeled=np.append(x_train_all,label_train_all,axis=1)

    #Shuffle the labeled data to get a "neutral" dataset#
    np.random.shuffle(x_train_all_labeled)
    #Separate the labels and data part of the shuffled dataset#
    x_label_shuffled=x_train_all_labeled[:,-1]

    x_train_shuffled=x_train_all_labeled[:,0:-1]

    x_label_shuffled=x_label_shuffled.astype(int)

    x_train_shuffled=x_train_shuffled.astype(float)

    # print('Dimension of the training data:',x_train_all.shape)

    #divide the set as training/test set#
    x_train , x_test , x_label_train ,x_label_test = \
        train_test_split(x_train_shuffled,x_label_shuffled,test_size=0.20)

    a,b = x_train.shape
#buraya pca loop'u gelecek bu sayede en dista train loopu sonra icerde pca loopu en icte de 
# test loopu olacak sekilde butun simulasyon kosacak#
    for idx in range(len(n_pca)):
        print('Iteration with N_PCA:',n_pca[idx],' has started.')
        pca=PCA(n_components = n_pca[idx])
        pca.fit(x_train)
        x_train_pca=pca.transform(x_train)
        pca_score = pca.explained_variance_ratio_
        x_test_pca = pca.transform(x_test)



        N_p_variance=1
        N_p=n_pca[idx]#number of principal components used

        #create the hidden layer architecture#
        u=30
        v=30
        w=30
        y=30
        hidden_layer = [u,v,w,y]

        x_train_after_pca=x_train_pca[:,0:N_p]
        x_test_after_pca=x_test_pca[:,0:N_p]

        pca_score_sum=sum(pca_score[0:N_p])
        pca_scores[train,idx]=pca_score_sum

        scaler=StandardScaler()
        scaler.fit(x_train_after_pca)
        # scaler.fit(x_test_after_pca)
        x_train_after_pca=scaler.transform(x_train_after_pca)
        x_test_after_pca=scaler.transform(x_test_after_pca)


        # print('\n\nTrain t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
        #     +'.'+test_sets[test][1]+'s results:')

        # print('\n [MLP Classifier Result]')
        # print('\n* The number of PCs :', N_p, \
        #         '\n* Total PCA score :',pca_score_sum,\
        #         '\n* Hidden Layer :', hidden_layer)
        print('Neural network training started...')
        inter_time=time.time()
        mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                            max_iter = 2000)
        mlp.fit(x_train_after_pca,x_label_train)
        train_time[train,idx]=time.time()-inter_time
        print('Neural network trained in ',train_time[train,idx],'s.')

        # network_name=''
        # for iii in range(len(hidden_layer)):
        #     network_name=network_name+str(hidden_layer[iii])+'_'
        # network_name=layer_info=network_name[:-1]
        # network_name='MLPClassifier_1instance_train'+train_sets[train]+'s_pc'+str(n_pca[idx])+'_'+\
        #     str(len(hidden_layer))+'layers_'+network_name+'.sav'
        # joblib.dump(mlp,network_name)


        # # the count of true negatives is C_0,0, false negatives is C_1,0, 
        # # true positives is C_1,1 and false positives is C_0,1.
        # print('\n\nResults of the trained neural network:')
        # print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
        # print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
        # print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))        




        for test in range(len(test_sets)):
            if test!=train:
                #call secondary test set#
                #For Windows
                # secondary_mat_contents_0 =\
                #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type1')\
                #         ['CA_Gelsdata']
                # secondary_mat_contents_1 =\
                #     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x'+test_sets[test]+'_type2')\
                #         ['CA_Gelsdata']
                #For Ubuntu
                secondary_mat_contents_0 =\
                    sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type1')\
                        ['CA_Gelsdata']
                secondary_mat_contents_1 =\
                    sio.loadmat('/media/dorukaks/Database/Nathan_GelsData/4th_DM/CA_4thDM_Pts_x'+test_sets[test]+'_type2')\
                        ['CA_Gelsdata']
                
                secondary_mat_contents_0=secondary_mat_contents_0[:6400]
                secondary_mat_contents_1=secondary_mat_contents_1[:6400]

                #Extract the data and create labels etc.#
                secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
                secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
                secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
                secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
                #Extract the x points from the cells to a big array
                for ii in range(len(secondary_mat_contents_0)):
                    secondary_test_data_0[ii]=secondary_mat_contents_0[ii][0]
                    # if ii<len(secondary_mat_contents_1):
                for ii in range(len(secondary_mat_contents_1)):
                        secondary_test_data_1[ii]=secondary_mat_contents_1[ii][0]
                secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
                secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
                secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

                #Shuffle the labeled data to get a "neutral" dataset#
                np.random.shuffle(secondary_test_shuffled)
                #Separate the labels and data part of the shuffled dataset#
                secondary_test_label=secondary_test_shuffled[:,-1]
                secondary_test_data=secondary_test_shuffled[:,0:-1]
                secondary_test_label=secondary_test_label.astype(int)
                secondary_test_data=secondary_test_data.astype(float)
        
                secondary_test_data_after_pca = pca.transform(secondary_test_data)
                secondary_test_data_after_pca=secondary_test_data_after_pca[:,0:N_p]

                secondary_test_data_after_pca=scaler.transform(secondary_test_data_after_pca)
                secondary_label_predicted=mlp.predict(secondary_test_data_after_pca)
                accuracy_pca[train,idx,test]=accuracy_score(secondary_test_label,secondary_label_predicted)

            else:
                
                x_label_predicted=mlp.predict(x_test_after_pca)
                accuracy_pca[train,idx,test]=accuracy_score(x_label_test,x_label_predicted)
            print('Train t='+train_sets[train][0]+'.'+train_sets[train][1]+'s, Test t='+test_sets[test][0]\
            +'.'+test_sets[test][1]+'s completed.')
            print('* Accuracy of the neural network: ',accuracy_pca[train,idx,test])
            
print('Execution of the whole code completed. Total runtime: ',time.time()-start_time,'s')

#Dumping the necessary information into a .pckl file for future post processing of the
# simulation results. #
# save_name='PCA_1instance_'+str(len(hidden_layer))+'layers_'+\
#     layer_info+'.pckl'
# save=open(save_name,'wb')
# pickle.dump([train_sets,test_sets,n_pca,hidden_layer,train_time,accuracy_pca],save)
# save.close

fig,axs=plt.subplots(1,len(train_sets),sharey=True)
fig.suptitle('Classification Results With Different Number of PCs')
for train in range(len(train_sets)):
    axs[train].plot(accuracy_pca[train,0,:],'r-o',label='PC=10')
    axs[train].plot(accuracy_pca[train,1,:],'b-o',label='PC=20')
    axs[train].plot(accuracy_pca[train,2,:],'g-o',label='PC=30')
    axs[train].plot(accuracy_pca[train,3,:],'c-o',label='PC=40')
    axs[train].plot(accuracy_pca[train,4,:],'m-o',label='PC=50')
    axs[train].set_xlabel('TR '+train_sets[train][0]+'.'+train_sets[train][1]+'s')
    axs[train].set_xticklabels([])

handles,labels=axs[len(train_sets)-1].get_legend_handles_labels()
fig.legend(handles,labels,loc='lower right')
# plt.xticks([])
plt.show()
                    

'''

#second test set #
secondary_mat_contents_0 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type1')['CA_Gelsdata']
secondary_mat_contents_1 =\
     sio.loadmat('D:/Nathan_GelsData/4th_DM/CA_4thDM_pts_x69_type2')['CA_Gelsdata']


#create label vectors for the data
secondary_test_label_0=np.zeros(secondary_mat_contents_0.shape)
secondary_test_label_1=np.ones(secondary_mat_contents_1.shape)
secondary_test_data_0=np.zeros((secondary_mat_contents_0.shape[0],secondary_mat_contents_0[0][0].shape[1]))
secondary_test_data_1=np.zeros((secondary_mat_contents_1.shape[0],secondary_mat_contents_1[0][0].shape[1]))
print(secondary_test_data_0.shape)

#Extract the x points from the cells to a big array
for idx in range(len(secondary_mat_contents_0)):
    secondary_test_data_0[idx]=secondary_mat_contents_0[idx][0]
    if idx<len(secondary_mat_contents_1):
# for idx in range(len(mat_contents_t1)):
        secondary_test_data_1[idx]=secondary_mat_contents_1[idx][0]

secondary_test_data_all=np.append(secondary_test_data_0,secondary_test_data_1,axis=0)
secondary_test_label_all=np.append(secondary_test_label_0,secondary_test_label_1,axis=0)
secondary_test_shuffled=np.append(secondary_test_data_all,secondary_test_label_all,axis=1)

#Shuffle the labeled data to get a "neutral" dataset#
np.random.shuffle(secondary_test_shuffled)
#Separate the labels and data part of the shuffled dataset#
secondary_test_label=secondary_test_shuffled[:,-1]
secondary_test_data=secondary_test_shuffled[:,0:-1]
secondary_test_label=secondary_test_label.astype(int)
secondary_test_data=secondary_test_data.astype(float)



N_p_variance=1
N_p=30 #number of principal components used

#create the hidden layer architecture#
u=30
# v=30
# w=30

hidden_layer = [u]
x_train_after_pca=x_train_pca[:,0:N_p]
x_test_after_pca=x_test_pca[:,0:N_p]
pca_score_sum=sum(pca_score[0:N_p])

scaler=StandardScaler()
scaler.fit(x_train_after_pca)
# scaler.fit(x_test_after_pca)
x_train_after_pca=scaler.transform(x_train_after_pca)
x_test_after_pca=scaler.transform(x_test_after_pca)

print('\n\nTrain t=6.1s, Test t=6.1s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

mlp = MLPClassifier(hidden_layer_sizes=hidden_layer, activation='relu',\
                    max_iter = 1000)
mlp.fit(x_train_after_pca,x_label_train)
x_label_predicted=mlp.predict(x_test_after_pca)

# the count of true negatives is C_0,0, false negatives is C_1,0, 
# true positives is C_1,1 and false positives is C_0,1.
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(x_label_test,x_label_predicted))                
print('* Classification report\n',classification_report(x_label_test,x_label_predicted))
print('* Accuracy of the neural network: ',accuracy_score(x_label_test,x_label_predicted))

x_train_shuffled_62_pca=pca.transform(secondary_test_data)
pca_score = pca.explained_variance_ratio_
pca_score_sum=sum(pca_score[0:N_p])
x_train_shuffled_62_pca=scaler.transform(x_train_shuffled_62_pca)
print('\n\nTrain t=6.1s, Test t=6.2s results:')

print('\n [MLP Classifier Result]')
print('\n* The number of PCs :', N_p, \
        '\n* Total PCA score :',pca_score_sum,\
        '\n* Hidden Layer :', hidden_layer)

x_label_shuffled_pred_62=mlp.predict(x_train_shuffled_62_pca)
print('\n\nResults of the trained neural network:')
print('* Confusion matrix \n',confusion_matrix(secondary_test_label,x_label_shuffled_pred_62))                
print('* Classification report\n',classification_report(secondary_test_label,x_label_shuffled_pred_62))
print('* Accuracy of the neural network: ',accuracy_score(secondary_test_label,x_label_shuffled_pred_62))
# print(precision_recall)

'''